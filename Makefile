# Project name
TARGET = mattapet

# directories
CURDIR   = .
SRCROOT  = $(CURDIR)
SRCDIR   = $(SRCROOT)/src
BUILDDIR = $(SRCROOT)/build
INCLUDE  = $(SRCROOT)/src
LIB      = $(SRCROOT)/lib
BINDIR   = $(SRCROOT)/bin

MAKESUBDIR = $(SRCDIR)

# Compilers
CC  = gcc
CXX = g++
CXXFLAGS = -std=c++11 -Wall -pedantic -Wno-long-long -O0 -ggdb -I$(INCLUDE)
LFLAGS = -lncurses

# Files
SRCS := $(shell find $(SRCDIR)  -name '*.cpp')
OBJS := $(subst .cpp,.o,$(SRCS))

all: clean docs compile

run: $(TARGET)
	./$(TARGET)

$(TARGET): compile

compile: $(OBJS)
	$(CXX) $(OBJS) -o $(TARGET) $(LFLAGS)

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^ >> ./.depend;

clean:
	-rm -rf doc
	$(RM) *~ .depend
	$(RM) $(OBJS)
	$(RM) $(TARGET)

include .depend

.PHONY: doc

doc:
	doxygen Doxyfile
