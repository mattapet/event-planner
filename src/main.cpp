//===- main.cpp -----------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file main.cpp
///
/// \brief This file is an entry point for the application EventPlanner.
///  It starts the initial view constroller and the event loop.
//===----------------------------------------------------------------------===//

#include "UI/UI.hpp"
#include "Planner/PlannerViewController.hpp"
#include <string>
#include <memory>

void startApplicationWithUID(const std::string &uid) {
    using namespace planner;
    auto win = initscr();
    auto initialVC = PlannerViewController_uptr(
        new PlannerViewController(win, uid)
    );
    initialVC->init();
    initialVC->load();
    initialVC->display();
    UI::NotificationCenter::listen();
    initialVC->hide();
    initialVC->deinit();
    wrefresh(win);
    delwin(win);
    endwin();
}

int main(int argc, const char * argv[]) {
    std::string uid = "default";
    if (argc > 1) {
        uid = argv[1];
    }
    startApplicationWithUID(uid);
    return 0;
}
