//===- PlanerViewController.cpp -------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file PlannerViewController.cpp
//===----------------------------------------------------------------------===//

#include "PlannerViewController.hpp"
#include <iostream>
#include <unistd.h>
#include <ncurses.h>
#include <vector>
#include <string>
#include <sstream>

using namespace planner;
    
// MARK: Lifecycle construction, desctruction.

PlannerViewController::PlannerViewController(WINDOW *win) throw()
: UI::ViewController(win),
  _state(PlannerState::Default),
  _commandStackPosition(0),
  _planner(Planner_uptr(new Planner()))
{}

PlannerViewController::
PlannerViewController(WINDOW *win, const std::string &uid) throw()
: UI::ViewController(win),
  _state(PlannerState::Default),
  _commandStackPosition(0),
  _planner(Planner_uptr(new Planner(uid)))
{}

void PlannerViewController::pushCommand(const std::string &command) noexcept {
    _commandStack.push_back(command);
    _commandStackPosition = _commandStack.size();
}

std::string PlannerViewController::getNextCommand() noexcept {
    if (_commandStackPosition == _commandStack.size()) {
        return "";
    } else {
        return _commandStack[++_commandStackPosition];
    }
}

std::string PlannerViewController::getPrevCommand() noexcept {
    if (!_commandStackPosition) {
        return "";
    } else {
        return _commandStack[--_commandStackPosition];
    }
}

// MARK: Public Implementation

void PlannerViewController::init() {
    UI::ViewController::init();
    _navigation = std::make_shared<PlannerNavigationController>(
      _win, _planner->calendars()
    );
    _navigation->initWithBounds({
        _bounds.origin.x,
        _bounds.origin.y,
        _bounds.size.width,
        _bounds.size.height - 2
    });
    _navigation->setDelegate(this);
    _calendarVC = _navigation->calendar();
    _input = std::make_shared<UI::TextField>(_win);
    _input->initWithBounds({
        _bounds.origin.x,
        _bounds.origin.y + _bounds.size.height - 2,
        _bounds.size.width,
        2
    });
    _input->setDelegate(this);
}

void PlannerViewController::deinit() {
    _calendarVC->deinit();
    _navigation->deinit();
    _input->deinit();
    UI::ViewController::deinit();
}

void PlannerViewController::viewDidLoad() {
    UI::ViewController::viewDidLoad();
    addChildViewController(_navigation);
    _view->addSubview(_input);
    _input->becomeFirstResponder();
}

void PlannerViewController::keyPressedEnterKey() {
    if (_state == PlannerState::DatePick) {
        _calendarVC->chooseSelectedEvents();
    } else if (_state == PlannerState::Form) {
        _calendarVC->chooseSelectedDate();
    }
}

void PlannerViewController::keyPressedEscapeKey() {
    _state = PlannerState::Default;
    _calendarVC->stopSelection();
    _input->becomeFirstResponder();
    _input->setText("");
}

void PlannerViewController::keyPressedArrowKeyUp() {
    _calendarVC->selectUp();
}

void PlannerViewController::keyPressedArrowKeyDown() {
    _calendarVC->selectDown();
}

void PlannerViewController::keyPressedArrowKeyRight() {
    _calendarVC->selectRight();
}

void PlannerViewController::keyPressedArrowKeyLeft() {
    _calendarVC->selectLeft();
}

// MARK: PlannerDelegate
void PlannerViewController::createEventWithData(const json &j) {
    PrivacyOption privacy = PrivacyOption::Private;
    if (j["privacy"] == "Public") {
        privacy = PrivacyOption::Public;
    } else if (j["privacy"] == "Shared") {
        privacy = PrivacyOption::Shared;
    } else {
        privacy = PrivacyOption::Private;
    }
    if (j.find("recurrence") != j.end()) {
        _planner->createEvent(j["recurrence"], {
            j["date_start"].get<Date>(),
            j["date_end"].get<Date>()
        }, j["summary"], privacy);
    } else {
        _planner->createEvent({
            j["date_start"].get<Date>(),
            j["date_end"].get<Date>()
        }, j["summary"], privacy);
    }
    
}

// MARK: UI::TextFieldDelegate
void PlannerViewController::textFieldDidReceiveReturn(UI::TextField &field) {
    auto command = field.text();
    std::istringstream iss(command);
    std::vector<std::string> tokens {
        std::istream_iterator<std::string>{iss},
        std::istream_iterator<std::string>{}
    };
    switch (_state) {
        case PlannerState::Default:
            if (tokens[0] == "next") {
                _calendarVC->next();
            } else if (tokens[0] == "prev") {
                _calendarVC->prev();
            } else if (tokens[0] == "goto") {
                try {
                    _state = PlannerState::DatePick;
                    auto date = DateFormatter::dateFromString(tokens[1], "%Y/%m/%d");
                    _calendarVC->selectDate(date);
                    becomeFirstResponder();
                } catch (...) {}
            } else if (tokens[0] == "next-available") {
                try {
                    auto now = DateFormatter::stringFromDate(Date::now(), " %Y/%m/%d");
                    auto nextInterval = _planner->findAvailable({
                        DateFormatter::dateFromString(tokens[1] + now, "%H:%M %Y/%m/%d"),
                        DateFormatter::dateFromString(tokens[2] + now, "%H:%M %Y/%m/%d")
                    });
                    _state = PlannerState::Form;
                    _calendarVC->selectDate(
                        nextInterval.dateStart().startOf(Date::TimeUnit::Day)
                    );
                    becomeFirstResponder();
                } catch (...) {}
            } else if (tokens[0] == "select") {
                _state = PlannerState::DatePick;
                _calendarVC->startSelection();
                becomeFirstResponder();
            } else if (tokens[0] == "new") {
                _state = PlannerState::Form;
                _calendarVC->startSelection();
                becomeFirstResponder();
            } else if (tokens[0] == "export-ical") {
                _planner->exportToICal();
            } else if (tokens[0] == "export-json") {
                _planner->exportCalendar();
            } else if (tokens[0] == "share") {
                _planner->shareCalendar();
            } else if (tokens[0] == "import") {
                try {
                    _planner->importCalendar(tokens[1]);
                    _calendarVC->setCalendars(_planner->calendars());
                } catch (...) {}
            } else if (tokens[0] == "exit") {
                UI::NotificationCenter::shouldStopEventLoop();
            }
            _input->setText("");
            break;
        default:break;
    }
}

