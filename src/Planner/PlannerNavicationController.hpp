//===- PlannerNavigationController.hpp - Planner navigation -----*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file PlannerNavigationController.hpp
//===----------------------------------------------------------------------===//

#ifndef PlannerNavicationController_hpp
#define PlannerNavicationController_hpp

#include "UI/UI.hpp"
#include "core/json.hpp"
#include "Calendar/Calendar.hpp"
#include "Calendar/CalendarViewController.hpp"
#include "Event/EventListViewController.hpp"
#include "Event/EventFormViewController.hpp"
#include <ncurses.h>
#include <vector>
#include <memory>

namespace planner {
    
    class PlannerDelegate;
    
    class PlannerNavigationController
    : public UI::NavigationController,
             CalendarViewControllerDelegate,
             EventListViewControllerDelegate,
             EventFormViewControllerDelegate
    {
    protected:
        CalendarViewController_ptr  _calendarVC;
        EventListViewController_ptr _eventListVC;
        EventFormViewController_ptr _eventFormVC;
        std::vector<Calendar_ptr>   _calendars;
        PlannerDelegate            *_delegate;
        
    public:
        PlannerNavigationController(WINDOW *win,
                                        std::vector<Calendar_ptr> calendars);
        
        virtual void initWithBounds(UI::Bounds bounds) override;
        
        CalendarViewController_ptr calendar() noexcept;
        
        PlannerDelegate *delegate();
        void setDelegate(PlannerDelegate *delegate);
        
        // MARK: EventFormViewControllerDelegate
        virtual void eventFormViewControllerShouldCreate(EventFormViewController &) override;
        virtual void eventFormViewControllerShouldCancel(EventFormViewController &) override;
        
        // MARK: EventListViewControllerDelegate
        virtual void eventListViewControllerShouldDismiss(EventListViewController &) override;
        
        // MARK: CalendarViewControllerDelegate
        virtual void calendarViewControllerSelectedDate(Date date) override;
        virtual void calendarViewControllerSelectedDateWithEvents(std::vector<Event_ptr> events) override;
        
    }; /* class PlannerNavigationController */
    
    typedef std::shared_ptr<PlannerNavigationController> PlannerNavigationController_ptr;
    typedef std::unique_ptr<PlannerNavigationController> PlannerNavigationController_uptr;
    typedef std::weak_ptr<PlannerNavigationController> PlannerNavigationController_wptr;

    class PlannerDelegate {
    public:
        virtual void createEventWithData(const json &j) = 0;
    };
    
    
} /* end namespace planner */

#endif /* PlannerNavicationController_hpp */
