//===- PlanerViewController.hpp - Planner view controller -------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file PlannerViewController.hpp
//===----------------------------------------------------------------------===//

#ifndef PlannerViewController_hpp
#define PlannerViewController_hpp

#include "UI/UI.hpp"
#include "Calendar/CalendarViewController.hpp"
#include "PlannerNavicationController.hpp"
#include "Planner.hpp"

#include <vector>
#include <string>
#include <memory>
#include <ncurses.h>

namespace planner {
    
    enum PlannerState {
        Default,
        DatePick,
        Form
    };
    
    class PlannerViewController
    : public UI::ViewController, UI::TextFieldDelegate, PlannerDelegate
    {
        PlannerState               _state;
        size_t                     _commandStackPosition;
        std::vector<std::string>   _commandStack;
        Planner_uptr               _planner;
        CalendarViewController_ptr _calendarVC;
        UI::TextField_ptr          _input;
        PlannerNavigationController_ptr _navigation;
        
        void pushCommand(const std::string &command) noexcept;
        std::string getNextCommand() noexcept;
        std::string getPrevCommand() noexcept;
        
    public:
        /// Designated constructor.
        explicit PlannerViewController(WINDOW *win) throw();
        PlannerViewController(WINDOW *win, const std::string &uid) throw();
        
        /// Designated destructor.
        virtual ~PlannerViewController() = default;
        
        virtual void init() override;
        virtual void deinit() override;
        
        virtual void viewDidLoad() override;
        
        virtual void keyPressedEnterKey() override;
        virtual void keyPressedEscapeKey() override;
        
        virtual void keyPressedArrowKeyUp() override;
        virtual void keyPressedArrowKeyDown() override;
        virtual void keyPressedArrowKeyRight() override;
        virtual void keyPressedArrowKeyLeft() override;
        
        // MARK: PlannerDelegate
        virtual void createEventWithData(const json &j) override;
        
        // MARK: TextFieldDelegate
        virtual void textFieldDidReceiveReturn(UI::TextField &field) override;
        
    }; /* PlannerViewController */
    
    typedef std::shared_ptr<PlannerViewController> PlannerViewController_ptr;
    typedef std::unique_ptr<PlannerViewController> PlannerViewController_uptr;
    typedef std::weak_ptr<PlannerViewController> PlannerViewController_wptr;
    
} /* end namespace planner */

#endif /* PlannerViewController_hpp */
