//===- PlannerNavigationController.cpp ------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file PlannerNavigationController.cpp
//===----------------------------------------------------------------------===//

#include "PlannerNavicationController.hpp"

using namespace planner;
    
PlannerNavigationController::
PlannerNavigationController(WINDOW *win,
                                std::vector<Calendar_ptr> calendars)
: UI::NavigationController(win),
  _calendars(calendars),
  _delegate(nullptr)
{
    _calendarVC = std::make_shared<CalendarViewController>(_win, _calendars);
    _rootVC = _calendarVC;
    _calendarVC->setDelegate(this);
}

void PlannerNavigationController::initWithBounds(UI::Bounds bounds) {
    UI::NavigationController::initWithBounds(bounds);
    _calendarVC->initWithBounds(bounds);
}

CalendarViewController_ptr
PlannerNavigationController::calendar() noexcept {
    return _calendarVC;
}

PlannerDelegate *PlannerNavigationController::delegate() {
    return _delegate;
}

void PlannerNavigationController::
setDelegate(PlannerDelegate *delegate)
{
    _delegate = delegate;
}

void PlannerNavigationController::
eventFormViewControllerShouldCreate(EventFormViewController &formVC)
{
    try {
        if (_delegate) {
            _delegate->createEventWithData(formVC.data());
        }
        popViewController();
        _eventFormVC->deinit();
        _eventFormVC = nullptr;
        _parentViewController->becomeFirstResponder();
    } catch (...) {}
}

void PlannerNavigationController::
eventFormViewControllerShouldCancel(EventFormViewController &formVC)
{
    popViewController();
    _eventFormVC->deinit();
    _eventFormVC = nullptr;
    _parentViewController->becomeFirstResponder();
}

void PlannerNavigationController::
eventListViewControllerShouldDismiss(EventListViewController &_)
{
    popViewController();
    _eventListVC->deinit();
    _eventListVC = nullptr;
    _parentViewController->becomeFirstResponder();
}

void PlannerNavigationController::
calendarViewControllerSelectedDate(Date date)
{
    _eventFormVC = std::make_shared<EventFormViewController>(_win, date);
    _eventFormVC->init();
    _eventFormVC->setDelegate(this);
    _eventFormVC->becomeFirstResponder();
    pushViewController(_eventFormVC);
}

void PlannerNavigationController::
calendarViewControllerSelectedDateWithEvents(std::vector<Event_ptr> events)
{
    _eventListVC = std::make_shared<EventListViewController>(_win, events);
    _eventListVC->init();
    _eventListVC->setDelegate(this);
    _eventListVC->becomeFirstResponder();
    pushViewController(_eventListVC);
}

