//===- Planer.hpp - The Planner model ---------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Planner.hpp
///
/// \brief This file contains declaration of the \c Planner class, which is
///  main class of the application. Also it contains a declaration of exceptions
///  used throughout the application.
//===----------------------------------------------------------------------===//

#ifndef Planner_hpp
#define Planner_hpp

#include "EventLoader/FileEventLoader.hpp"
#include "Calendar/Calendar.hpp"
#include "Event/Event.hpp"
#include "core/Recurrence.hpp"
#include "core/Date.hpp"
#include "core/json.hpp"
#include <string>
#include <vector>
#include <memory>
#include <exception>

namespace planner {
    // For the json convenience
    using json = nlohmann::json;
    
    class PlannerConfigurationNotFoundException: public std::runtime_error {
    public:
        PlannerConfigurationNotFoundException();
    };
    
    /// \brief Exception is thrown when there was an error while creating
    ///  a default configuration file.
    ///
    /// \description This exception is most commonly thrown, when the user,
    ///  under whom the application is run does not have proper access to the
    ///  directory in which the executable is stored.
    class PlannerConfigurationWriteException: public std::runtime_error {
    public:
        /// Default constructor for the \c PlannerConfigurationWriteException.
        PlannerConfigurationWriteException();
    };
    
    /// \brief Exception is thrown when there was an error in structure of
    ///  the configuration file.
    ///
    /// \description This exception is most comonly thrown, when a property
    ///  of the configuration is missing and/or types of a properties do not
    ///  match.
    class PlannerBadConfigurationException: public std::runtime_error {
    public:
        /// Deafult constructor for the \c PlannerBadConfigurationException.
        PlannerBadConfigurationException();
    };
    
    /// \brief Exception is thrown, when there was an error while saving the
    ///  Planner configuration.
    ///
    /// \description This exception is most commonly thrown, when there
    ///  already exists a file with the exact name of the configuration file,
    ///  but the user, whom doesn't have proper right for overriding the file,
    ///  and/or right for writing in current directory.
    class PlannerConfigurationSaveException: public std::runtime_error {
    public:
        /// Default constructor for the \c PlannerBadConfigurationException.
        PlannerConfigurationSaveException();
    };
    
    /// \brief Class, that contains and handles calendars and operates on them.
    ///
    /// \description Planner class is a container, that provides a unified
    ///  interface over several calendars and their events. It is configured
    ///  by a \b json configuration file named \c planer.configuration.json,
    ///  that is loaded from the current directory of the executable.
    ///  The configuration contains \c directory and \c calendars properties.
    ///
    ///  \c directory - contains the working directory of the application.
    ///   In this directory are stored all of the data files, that calendars
    ///   use. The default directory value is set to \c ./ - currect directory.
    ///
    ///  \c calendars - list of calendars, that should be used by the \c Planner
    ///   object. The default calendar value is set to an empty array.
    ///
    /// \see https://en.wikipedia.org/wiki/JSON
    class Planner {
        /// The default name of configuration file.
        static const std::string kCONFIGURATION_NAME;
        
        /// UID of the current user.
        std::string                _uid;
        /// The configuration of an object.
        json _configuration;
        /// The calendars used by an object.
        std::vector<Calendar_ptr> _calendars;
        /// Shared loader for all calendars to use.
        FileEventLoader_ptr        _loader;
        
        // MARK: Configuration
        
        /// Creates a default configuration file in the working directory.
        /// throw PlannerConfigurationWriteException
        void createDefaultConfigurationFile() const;
        
        /// Returns the default configuration as a JSON object.
        /// \return The default configuration.
        json getDefaultConfiguration() const noexcept;
        
        /// Validates loaded configuration.
        /// throw PlannerBadConfigurationException
        void validateConfiguration() const;
        
        /// Loads a configuration.
        /// throw PlannerConfigurationNotFoundException
        /// throw PlannerConfigurationWriteException
        /// throw PlannerBadConfigurationException
        void loadConfiguration();
        
        // MARK: Interval loading and saving of calendars.
        
        /// Loads all calendars.
        void loadCalendars();
        
        /// Saves current configuration.
        void saveConfiguration() const;
        
    public:
        
        // MARK: Lifecycle.
        
        /// Constructs a default planner object.
        /// throw PlannerConfigurationNotFoundException
        /// throw PlannerConfigurationWriteException
        /// throw PlannerBadConfigurationException
        explicit Planner();
        
        /// Constructs a default planner object.
        /// throw PlannerConfigurationNotFoundException
        /// throw PlannerConfigurationWriteException
        /// throw PlannerBadConfigurationException
        Planner(const std::string &uid);
        
        /// Destructs a planner object while updating the configuration file.
        ~Planner();
        
        // MARK: Property accessors.
        
        /// Returns the current main calendar.
        /// \return Reference to the unique pointer of the main calendar.
        Calendar_ptr calendar();
        
        /// Returns all calendars.
        std::vector<Calendar_ptr> calendars() const noexcept;
        
        /// Finds the closes available large enought spot for the give date
        /// interval.
        DateInterval findAvailable(const DateInterval &interval);
        
        /// \brief Returns a boolean value identifying if the current interval
        /// is available to create an event.
        /// \return \c true if the given date interval is not occupied, \c false
        ///  otherwise.
        bool isAvailable(const DateInterval &interval);
        
        // MARK: Mutating methods.
        
        /// Creates a single event with provided data for the main calendar.
        /// \param interval The interval of the event.
        /// \param summary The brief description of the event.
        /// \param privacy The privacy setting, default to \c Private.
        void createEvent(const DateInterval &interval,
                         const std::string &summary,
                         PrivacyOption privacy = PrivacyOption::Private);
        
        /// Creates a recurring event with provided data for the main calendar.
        /// \param recurrence The recurrence rule of the event.
        /// \param interval The interval of the event.
        /// \param summary The brief description of the event.
        /// \param privacy The privacy setting, default to \c Private.
        void createEvent(const Recurrence   &recurrence,
                         const DateInterval &interval,
                         const std::string  &summary,
                         PrivacyOption privacy = PrivacyOption::Private);
        
        /// Removed given event with.
        void removeEvent(const Event_ptr &event);
        
        // MARK: External loading and saving.
        
        /// Exports sharable version of calendar.
        void shareCalendar() const;
        
        /// Exports the calendar of current user.
        void exportCalendar() const;
        
        /// Exports the calendar with the provided UID.
        void exportCalendarForUID(const std::string &uid) const;
        
        /// Imports a calendar on the provided path.
        /// \param path The path to the import file.
        void importCalendar(const std::string &path);
        
        /// Exports the current calendar into a file in iCal format.
        void exportToICal() const;
        
        /// Imports an ical file to the current user's calendar.
        void importFromICal(const std::string &path);
        
    }; /* class Planner */

    typedef std::shared_ptr<Planner> Planner_ptr;
    typedef std::unique_ptr<Planner> Planner_uptr;
    typedef std::weak_ptr<Planner> Planner_wptr;
    
} /* end namespace planner */

#endif /* Planner_hpp */
