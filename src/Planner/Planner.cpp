//===- Planner.cpp - Planner implementation -------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Planner.cpp
///
/// \brief This file contains implementation of a Planner class and all
///  associated exceptions.
//===----------------------------------------------------------------------===//

#include "Planner.hpp"

#include "Event/Event.hpp"
#include "Event/RecurringEvent.hpp"
#include <fstream>
#include <algorithm>

using namespace planner;
    
const std::string Planner::kCONFIGURATION_NAME = "planner.configuration.json";

// MARK: Exceptions

PlannerConfigurationNotFoundException::
PlannerConfigurationNotFoundException()
: std::runtime_error("There was an error reading configuration file.")
{}

PlannerConfigurationWriteException::
PlannerConfigurationWriteException()
: std::runtime_error("There was an error creating a configuration file.")
{}

PlannerBadConfigurationException::
PlannerBadConfigurationException()
: std::runtime_error("The configuration file does not have a correct format.")
{}

PlannerConfigurationSaveException::
PlannerConfigurationSaveException()
: std::runtime_error("There was an error saving the configuration.")
{}

// MARK: Constructors

Planner::Planner()
: _loader(std::make_shared<FileEventLoader>())
{
    std::ifstream is(kCONFIGURATION_NAME);
    if (is.fail()) {
        is.clear();
        createDefaultConfigurationFile();
        _configuration = getDefaultConfiguration();
    }
    is.close();
    _loader->setDirectory(_configuration["directory"]);
    loadConfiguration();
    loadCalendars();
}

Planner::Planner(const std::string &uid)
: _uid(uid),
  _loader(std::make_shared<FileEventLoader>())
{
    std::ifstream is(kCONFIGURATION_NAME);
    if (is.fail()) {
        is.clear();
        createDefaultConfigurationFile();
        _configuration = getDefaultConfiguration();
    } else {
        is >> _configuration;
    }
    is.close();
    _loader->setDirectory(_configuration["directory"]);
    loadConfiguration();
    loadCalendars();
    _uid = uid;
    _configuration["uid"] = uid;
    for (auto calendar : _configuration["calendars"]) {
        if (calendar == _uid) {
            return;
        }
    }
    _configuration["calendars"].push_back(_uid);
    _calendars.push_back(Calendar_uptr(new Calendar(_uid, _loader)));
}

Planner::~Planner() {
    saveConfiguration();
}

// MARK: Private Implementation

void Planner::createDefaultConfigurationFile() const {
    std::ofstream os(kCONFIGURATION_NAME);
    os << getDefaultConfiguration();
    if (os.fail()) {
        throw PlannerConfigurationWriteException();
    }
}

json Planner::getDefaultConfiguration() const noexcept {
    return {
        {"directory", "./"},
        {"calendars", std::vector<std::string>({_uid})},
        {"uid", _uid}
    };
}

void Planner::validateConfiguration() const {
    try {
        _configuration.at("directory");
        _configuration.at("calendars");
        _configuration.at("uid");
    } catch (const nlohmann::detail::out_of_range &err) {
        throw PlannerBadConfigurationException();
    }
    
    if (!_configuration["directory"].is_string()
        || !_configuration["uid"].is_string()
        || !_configuration["calendars"].is_array()) {
        throw PlannerBadConfigurationException();
    }
}

void Planner::loadConfiguration() {
    std::ifstream is(kCONFIGURATION_NAME);
    try {
        is >> _configuration;
        validateConfiguration();
        _uid = _configuration["uid"];
    } catch (const nlohmann::detail::parse_error &err) {
        throw PlannerBadConfigurationException();
    }
}

void Planner::saveConfiguration() const {
    std::ofstream os(kCONFIGURATION_NAME);
    os << _configuration;
    if (os.fail()) {
        throw PlannerConfigurationSaveException();
    }
}

void Planner::loadCalendars() {
    std::vector<std::string> calendarUIDs = _configuration["calendars"];
    for (auto calendarUID : calendarUIDs) {
        _calendars.push_back(
            Calendar_uptr(new Calendar(calendarUID, _loader))
        );
    }
}

// MARK: Public Implementation

Calendar_ptr Planner::calendar() {
    for (auto &calendar : _calendars) {
        if (calendar->uid() == _uid) {
            return calendar;
        }
    }
    _configuration["calendars"].push_back(_uid);
    _calendars.push_back(Calendar_uptr(new Calendar(_uid, _loader)));
    return _calendars[_calendars.size() - 1];
}

std::vector<Calendar_ptr> Planner::calendars() const noexcept {
    return _calendars;
}

DateInterval Planner::findAvailable(const DateInterval &interval) {
    return calendar()->findAvailable(interval);
}

void Planner::createEvent(const DateInterval &interval,
                          const std::string  &summary,
                          PrivacyOption privacy) {
    auto event = std::make_shared<Event>(_uid, summary, interval);
    calendar()->addEvent(
        std::make_shared<Event>(
            _uid, summary, interval, privacy
        )
    );
}

void Planner::createEvent(const Recurrence   &recurrence,
                          const DateInterval &interval,
                          const std::string  &summary,
                          PrivacyOption       privacy) {
    calendar()->addEvent(std::make_shared<RecurringEvent>(
        recurrence, _uid, summary, interval, privacy
    ));
}

void Planner::removeEvent(const Event_ptr &event) {
    calendar()->removeEvent(event);
}

bool Planner::isAvailable(const DateInterval &interval) {
    return calendar()->isAvailable(interval);
}

// MARK: Export and import methods.

void Planner::shareCalendar() const {
    std::string dir = _configuration["directory"];
    std::ofstream os(dir + "event_planner_share-" + _uid + ".json");
    for (auto &calendar : _calendars) {
        if (calendar->uid() == _uid) {
            os << calendar->toJSONShared();
            return;
        }
    }
}

void Planner::exportCalendar() const {
    std::string dir = _configuration["directory"];
    std::ofstream os(dir + "event_planner_export-" + _uid + ".json");
    for (auto &calendar : _calendars) {
        if (calendar->uid() == _uid) {
            os << calendar->toJSON();
            return;
        }
    }
}

void Planner::exportCalendarForUID(const std::string &uid) const {
    std::string dir = _configuration["directory"];
    std::ofstream os(dir + "event_planner_export-" + uid + ".json");
    for (auto &calendar : _calendars) {
        if (calendar->uid() == uid) {
            os << calendar->toJSON();
            return;
        }
    }
}

void Planner::importCalendar(const std::string &path) {
    std::ifstream is(path);
    json j;
    is >> j;
    _calendars.push_back(Calendar_uptr(new Calendar(j, _loader)));
}

void Planner::exportToICal() const {
    std::string dir = _configuration["directory"];
    std::ofstream os(dir + "event_planner_export-" + _uid + ".ical.ics");
    for (auto &calendar : _calendars) {
        if (calendar->uid() == _uid) {
            os << calendar->toICalString();
            return;
        }
    }
}

void Planner::importFromICal(const std::string &path) {}




