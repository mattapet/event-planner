//===- ViewController.hpp - UI controller class -----------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file ViewController.hpp
///
/// \brief This file contains declaration of the \c ViewController class,
///  that is one of the essential classes of this module.
//===----------------------------------------------------------------------===//

#ifndef ViewController_hpp
#define ViewController_hpp

#include "Responder.hpp"
#include "View.hpp"
#include <ncurses.h>
#include <memory>
#include <vector>

namespace UI {
    
    /// \brief The \c ViewController provides an infrastructure for managing
    ///  views and/or other constrollers from this module.
    ///
    /// \description The \c ViewController is a base class for all view
    ///  controllers, that whish to controll \c View objects. It is possible
    ///  to create objects directly from the \c ViewController class, however
    ///  without subclassing and extending the \c ViewController it is not
    ///  usual. The \c ViewController class is only a 'toolset' you use and
    ///  extantiate for your special use.
    ///  The view controllers support parent-children behaviour, where you can
    ///  add a child view controller to a view controller using method
    ///  \c addChildViewController and/or remove a view controller from the
    ///  parent view controllelr using method \c removeFromParentViewController.
    ///
    /// \par View controller lifecycle
    ///  After allocation of a view controller, every view controller should be
    ///  initialized using one of the initialization methods. If the view
    ///  controller is a part of a view controller tree, then the loading of the
    ///  view starts. The appropriate \c viewWillLoad and \c viewDidLoad methods
    ///  are called right before and right after the allocation of the view
    ///  and it's initialization. Therefore, you \c must \c not access the
    ///  designated view before the \c viewDidLoad method is called. Typically
    ///  the next part is the controller's lifetime is to display it's view/s.
    ///  Rigth before and aftert the view/s is/are displayer methods
    ///  \c viewWillAppear and \c viewDidAppear are called. Now the view
    ///  controller's view/s is/are displayed and they stay display until the
    ///  view controller itself and/or the parent view controller will live.
    ///  During this period the view controller manages it's views and upon any
    ///  update it calls the \c realod method. The \c reload method causes
    ///  \c viewWillUpdate and \c viewDidUpdate to be called analogically to the
    ///  loading and appearing methods.
    ///
    ///  When the view controller's view should be hidden, the
    ///  \c viewWillDissappear and \c viewDidDissappear methds are called.
    ///  At the end of the view controller's lifecycle, the \c viewWillUnload
    ///  and \c viewDidUnload methods are called.
    ///  After this point you \c must \c not access the designated view
    ///  of the controller, since by now it'd been properly deinitialized
    ///  and destructed. The next step is the deinitialization of the view
    ///  controller itself, and the destruction.
    class ViewController: public Responder {
    protected:
        /// The ncurses \c WINDOW \c * object.
        WINDOW *_win;
        /// The bounds of the view controller.
        Bounds  _bounds;
        /// Boolean value indicating \c hidden state of the view controller.
        bool    _hidden;
        
        /// The designated view of the view controller.
        std::unique_ptr<View>                        _view;
        /// The parent view controller.
        ViewController                              *_parentViewController;
        /// The child view controllers.
        std::vector<std::shared_ptr<ViewController>> _childViewControllers;
        
        // MARK: Protected methods.
        
        /// Fills the bounds of the given \c WINDOW \c * object.
        virtual void fillBounds();
        
    public:
        
        // MARK: Construction and destruction of objects.
        
        /// The designated contructor of a view controller.
        /// \param win The ncurses \c WINDOW \c * object.
        ViewController(WINDOW *win);
        
        /// The default destructor.
        virtual ~ViewController() = default;
        
        // MARK: Intitialization and deinitialization of objects.
        
        /// \brief The designated initialization method.
        /// \description During the initialization, the ncurses \c WINDOW \c *
        ///  object is subviewed, and all of the child view controllers are
        ///  properly initialized.
        ///
        /// \node The initialization method should be called only \b once during
        ///  the lifetime of an object.
        /// \throw UISubwindowCreationException
        virtual void init() override;
        
        /// \brief The convenience initialization method.
        /// \description During the initialization, the ncurses \c WINDOW \c *
        ///  object is subviewed, and all of the child view controllers are
        ///  properly initialized.
        /// \param bounds The bounds, which to fill.
        ///
        /// \node The initialization method should be called only \b once during
        ///  the lifetime of an object.
        /// \throw UISubwindowCreationException
        virtual void initWithBounds(Bounds bounds);
        
        /// \brief Designated deinitialization method of the \c ViewController
        ///  objects.
        /// \description This method should be used in pair with the \c init
        ///  and/or \c initWithBounds methods to assure, that all of the memory
        ///  is freed object's destruction.
        ///
        /// \node The deinitialization method should be called only \b once
        ///  during the lifetime of an object.
        virtual void deinit() override;
        
        // MARK: Accessor methods.
        
        /// Returns a boolean value identifying designated view's hidden state.
        /// \return \c true, if the view is hidden, \c false otherwise.
        bool hidden() const;
        
        /// Hides and/or displays the designated view depending on the given
        ///  value.
        /// \pram hidden The value to set the view to be.
        void setHidden(bool hidden);
        
        /// Returns a parent view controllers
        /// \return \c nullptr, if the view controller is the root of it's
        ///  view controlelr tree, otherwise a pointer to the parent view
        ///  controller.
        ViewController *parentViewController();
        
        // MARK: Internal methods.
        
        /// Loads the designated view/s and view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void load();
        
        /// Unloads the designated view/s and view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void unload();
        
        /// Displays the designated view/s and view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void display();
        
        /// Hides the designated view/s and view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void hide();
        
        /// Refreshed the designated view/s and view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void reload();
        
        /// Loads the subviews of the view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void loadSubviews();
        
        /// Unloads the subviews of the view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void unloadSubviews();
        
        /// Disaplays the subviews of the view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void displaySubviews();
        
        /// Hides the subviews of the view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void hideSubviews();
        
        /// Refreshes the subviews of the view controllers.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for the view controller
        ///  to manage it's descendent view controllers.
        void reloadSubviews();
        
        // MARK: Parent tree managing.
        
        /// Adds a view controller as a child view controller.
        /// \param childVC A view controller, to be added as a child.
        void addChildViewController(std::shared_ptr<ViewController> childVC);
        
        /// Removes a view controller, from it parent view controller child
        /// view controllers.
        void removeFromParentViewController();
        
        // MARK: Lifecycle methods.
        
        /// Method, that is called directly before the designated view is loaded.
        /// At this point, the designated view is still not allocated, therefore
        /// you \b must \b not access it.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewWillLoad();
        /// Method, that is called directly after the designated view is loaded
        /// and properly initialized. At this pointer you are free to access
        /// the desginated view and modify it.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewDidLoad();
        /// Method, that is called directly before the designated view is
        /// unloaded. At this point the view is still allocated and fully
        /// initialized.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewWillUnload();
        /// Method, that is called directly after the designated view is
        /// unloaded. At this point you \c must \c not access the designated
        /// view, since by now it'd been properly deinitialized and destructed.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewDidUnload();
        
        /// Method, that is called directly before the designated view is
        /// displayed.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewWillAppear();
        /// Method, that is called directly after the designated view had been
        /// displayed.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewDidAppear();
        /// Method, that is called directly before the designated view is hidden.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewWillDisappear();
        /// Method, that is called directly after the designated view had been
        /// hidden.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewDidDisappear();
        
        /// Method, that is called directly before the designated view will be
        /// refreshed.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewWillUpdate();
        /// Method, that is called directly after the designated view had been
        /// refreshed.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. Call of this method is managed by the parent
        ///  view controllers.
        virtual void viewDidUpdate();
        
    }; /* class ViewController */
    
    typedef std::shared_ptr<ViewController> ViewController_ptr;
    typedef std::unique_ptr<ViewController> ViewController_uptr;
    typedef std::weak_ptr<ViewController> ViewController_wptr;
    
} /* end namespace UI */


#endif /* ViewController_hpp */
