//===- TextField.cpp ------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file TextField.cpp
//===----------------------------------------------------------------------===//

#include "TextField.hpp"

using namespace UI;
    
// MARK: Construction

TextField::TextField(WINDOW *win)
: View(win),
  _underlined(false),
  _delegate(nullptr)
{}

TextField::TextField(WINDOW *win, bool underlined)
: View(win),
  _underlined(underlined),
  _delegate(nullptr)
{}

// MARK: Property accessing methods.

const std::string &TextField::text() const {
    return _text;
}

void TextField::setText(const std::string &text) {
    _text = text;
    reload();
}

bool TextField::underlined() const {
    return _underlined;
}

void TextField::setUnderlined(bool underlined) {
    _underlined = underlined;
}

TextFieldDelegate *TextField::delegate() {
    return _delegate;
}

void TextField::setDelegate(TextFieldDelegate *delegate) {
    _delegate = delegate;
}

void TextField::draw() {
    if (_underlined) {
        wattron(_win, A_UNDERLINE);
    }
    wprintw(_win, _text.c_str());
    wattroff(_win, A_UNDERLINE);
}

void TextField::keyPressedEnterKey() {
    if (_delegate) {
        _delegate->textFieldDidReceiveReturn(*this);
    }
    reload();
}

void TextField::keyPressedBackspace() {
    if (_text.size()) {
        _text.pop_back();
    }
    reload();
}

void TextField::keyPressedTabKey() {
    if (_delegate) {
        _delegate->nextResponderShouldBecomeFirstResponder(*this);
    }
}

void TextField::keyPressedShiftTabKey() {
    if (_delegate) {
        _delegate->prevResponderShouldBecomeFirstResponder(*this);
    }
}

void TextField::keyPressedAlphanumText(char value) {
    _text += value;
    reload();
}
