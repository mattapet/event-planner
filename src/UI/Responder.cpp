//===- Responder.cpp ------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Button.cpp
//===----------------------------------------------------------------------===//

#include "Responder.hpp"

using namespace UI;

Responder::Responder() {
    NotificationCenter::registerResponder(this);
}

Responder::~Responder() {
    NotificationCenter::deregisterResponder(this);
}

void Responder::init() {}
void Responder::deinit() {}

bool Responder::isFirstResponder() const noexcept {
    return NotificationCenter::firstResponder() == this;
}

void Responder::becomeFirstResponder() noexcept {
    NotificationCenter::becomeFirstResponder(this);
}

void Responder::resignFirstResponder() noexcept {
    NotificationCenter::resignFirstResponder(this);
}

Responder *Responder::next() const noexcept {
    return NotificationCenter::next();
}

void Responder::keyPressedAlphanumText(char value) {}

void Responder::keyPressedEnterKey()      {}
void Responder::keyPressedBackspace()     {}
void Responder::keyPressedEscapeKey()     {}
void Responder::keyPressedTabKey()        {}
void Responder::keyPressedShiftTabKey()   {}

void Responder::keyPressedArrowKeyUp()    {}
void Responder::keyPressedArrowKeyDown()  {}
void Responder::keyPressedArrowKeyRight() {}
void Responder::keyPressedArrowKeyLeft()  {}

void ResponderDelegate::nextResponderShouldBecomeFirstResponder(Responder &) {}
void ResponderDelegate::prevResponderShouldBecomeFirstResponder(Responder &) {}

