//===- ViewController.cpp -----------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file ViewController.cpp
///
/// \brief This file contains implementaion of the \c ViewController class,
///  that is one of the essential classes of this module.
//===----------------------------------------------------------------------===//

#include "ViewController.hpp"

using namespace UI;
    
// MARK: Construction and destruction of objects.

ViewController::ViewController(WINDOW *win)
: _win(win),
  _hidden(false),
  _view(nullptr),
  _parentViewController(nullptr)
{}

// MARK: Protected implementation

void ViewController::fillBounds() {
    getyx(_win, _bounds.origin.y, _bounds.origin.x);
    getmaxyx(_win, _bounds.size.height, _bounds.size.width);
}

// MARK: Intitialization and deinitialization of objects.

void ViewController::init() {
    Responder::init();
    fillBounds();
    _win = subwin(
        _win,
        _bounds.size.height,
        _bounds.size.width,
        _bounds.origin.y,
        _bounds.origin.x
    );
}

void ViewController::initWithBounds(Bounds bounds) {
    Responder::init();
    _bounds = bounds;
    _win = subwin(
        _win,
        _bounds.size.height,
        _bounds.size.width,
        _bounds.origin.y,
        _bounds.origin.x
    );
}

void ViewController::deinit() {
    delwin(_win);
}

// MARK: Accessor methods.

bool ViewController::hidden() const {
    return _hidden;
}

void ViewController::setHidden(bool hidden) {
    if (_hidden != hidden) {
        if (hidden) {
            hide();
        }
        _hidden = hidden;
    }
}

ViewController *ViewController::parentViewController() {
    return _parentViewController;
}

// MARK: Internal methods.

void ViewController::load() {
    viewWillLoad();
    _view = std::unique_ptr<View>(new View(_win));
    _view->initWithBounds(_bounds);
    loadSubviews();
    viewDidLoad();
}

void ViewController::unload() {
    viewWillUnload();
    unloadSubviews();
    _view->deinit();
    viewDidUnload();
}

void ViewController::display() {
    viewWillAppear();
    _view->display();
    displaySubviews();
    viewDidAppear();
}

void ViewController::hide() {
    viewWillDisappear();
    _view->hide();
    hideSubviews();
    viewDidDisappear();
}

void ViewController::reload() {
    viewWillUpdate();
    _view->reload();
    reloadSubviews();
    viewDidUpdate();
}

void ViewController::loadSubviews() {
    for (auto childViewController : _childViewControllers) {
        childViewController->load();
    }
}

void ViewController::unloadSubviews() {
    for (auto &childViewController : _childViewControllers) {
        childViewController->unload();
    }
}

void ViewController::displaySubviews() {
    for (auto &childViewController : _childViewControllers) {
        childViewController->display();
    }
}

void ViewController::hideSubviews() {
    for (auto &childViewController : _childViewControllers) {
        childViewController->hide();
    }
}

void ViewController::reloadSubviews() {
    for (auto &childViewController : _childViewControllers) {
        childViewController->reload();
    }
}

void ViewController::
addChildViewController(std::shared_ptr<ViewController> childVC)
{
    childVC->_parentViewController = this;
    _childViewControllers.push_back(childVC);
    childVC->load();
}

void ViewController::removeFromParentViewController() {
    if (_parentViewController) {
        auto it = _parentViewController->_childViewControllers.begin();
        for (;;) {
            if (it == _parentViewController->_childViewControllers.end()) {
                break;
            }
            if ((*it).get() == this) {
                _parentViewController->_childViewControllers.erase(it);
                _parentViewController = nullptr;
                unload();
                break;
            }
            ++it;
        }
    }
}

// MARK: Lifecycle methods.

void ViewController::viewWillLoad()      {}
void ViewController::viewDidLoad()       {}
void ViewController::viewWillUnload()    {}
void ViewController::viewDidUnload()     {}

void ViewController::viewWillAppear()    {}
void ViewController::viewDidAppear()     {}
void ViewController::viewWillDisappear() {}
void ViewController::viewDidDisappear()  {}

void ViewController::viewWillUpdate()    {}
void ViewController::viewDidUpdate()     {}


