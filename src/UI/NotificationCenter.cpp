//===- NotificationCenter.cpp ---------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file NotificationCenter.cpp
//===----------------------------------------------------------------------===//

#include "NotificationCenter.hpp"
#include <algorithm>

using namespace UI;


NotificationCenter NotificationCenter::center = {};

void NotificationCenter::init() {
    _setup = true;
    noecho();
    keypad(stdscr, TRUE);
//        nodelay(stdscr, TRUE);
    cbreak();
}

void NotificationCenter::deinit() {}

bool NotificationCenter::isSetup() const noexcept {
    return _setup;
}

void NotificationCenter::_registerResponder(Responder *responder) noexcept {
    if (!_setup) {
        init();
    }
    _responders.push_back(responder);
}

void NotificationCenter::_deregisterResponder(Responder *responder) noexcept {
    if (firstResponder() == responder) {
        firstResponder()->next()->becomeFirstResponder();
    }
    for (auto it = _responders.begin(); it != _responders.end(); ++it) {
        if (*it == responder) {
            _responders.erase(it);
            break;
        }
    }
}

void NotificationCenter::_becomeFirstResponder(Responder *responder) noexcept {
    if (responder != _firstResponder) {
        auto it = std::find(_responders.begin(), _responders.end(), responder);
        if (it != _responders.end()) {
            _firstResponder = *it;
        }
    }
}

void NotificationCenter::_resignFirstResponder(Responder *responder) noexcept {
    if (responder == _firstResponder) {
        auto it = std::find(_responders.begin(), _responders.end(), responder->next());
        _firstResponder = *it;
    }
}

const Responder *NotificationCenter::_getFirstResponder() const noexcept {
    return _firstResponder;
}

Responder *NotificationCenter::_getFirstResponder() noexcept {
    return _firstResponder;
}

Responder *NotificationCenter::_next() noexcept {
    if (_firstResponder) {
        auto it = std::find(_responders.begin(), _responders.end(), _firstResponder);
        _firstResponder = it != _responders.end() ? *it : nullptr;
    }
    return _firstResponder;
}

// MARK: Static methods

void NotificationCenter::registerResponder(Responder *responder) noexcept {
    NotificationCenter::center._registerResponder(responder);
}

void NotificationCenter::deregisterResponder(Responder *responder) noexcept {
    NotificationCenter::center._deregisterResponder(responder);
}

void NotificationCenter::becomeFirstResponder(Responder *responder) noexcept {
    NotificationCenter::center._becomeFirstResponder(responder);
}

void NotificationCenter::resignFirstResponder(Responder *responder) noexcept {
    NotificationCenter::center._resignFirstResponder(responder);
}

Responder *NotificationCenter::firstResponder() noexcept {
    return NotificationCenter::center._getFirstResponder();
}

Responder *NotificationCenter::next() noexcept {
    return NotificationCenter::center._next();
}

void NotificationCenter::shouldStopEventLoop() noexcept {
    NotificationCenter::center._setup = false;
}

void NotificationCenter::listen() {
    int c;
    while (NotificationCenter::center.isSetup() && (c = getch())) {
        if (auto responder = center.firstResponder()) {
            if (c == KEY_ENTER || c == '\n') {
                responder->keyPressedEnterKey();
            } else if (c == 27) {
                responder->keyPressedEscapeKey();
            } else if (c == KEY_BACKSPACE || c == KEY_DC || c == 127) {
                responder->keyPressedBackspace();
            } else if (c == '\t') {
                responder->keyPressedTabKey();
            } else if (c == KEY_BTAB) {
                responder->keyPressedShiftTabKey();
            } else if (c == KEY_UP) {
                responder->keyPressedArrowKeyUp();
            } else if (c == KEY_DOWN) {
                responder->keyPressedArrowKeyDown();
            } else if (c == KEY_RIGHT) {
                responder->keyPressedArrowKeyRight();
            } else if (c == KEY_LEFT) {
                responder->keyPressedArrowKeyLeft();
            } else if (c < 128) {
                responder->keyPressedAlphanumText((char)c);
            }
        }
    }
}
