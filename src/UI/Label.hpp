//===- Label.hpp - Label UI object ------------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Label.hpp
//===----------------------------------------------------------------------===//

#ifndef Label_hpp
#define Label_hpp

#include "View.hpp"
#include <ncurses.h>
#include <string>
#include <memory>

namespace UI {
    
    class Label: public View {
    protected:
        std::string _text;
        
    public:
        Label(WINDOW *win);
        Label(WINDOW *win, const std::string &text);
        
        virtual const std::string &text() const;
        
        virtual void setText(const std::string &text);
        
        virtual void draw() override;
    
    }; /* class Label */
    
    typedef std::shared_ptr<Label> Label_ptr;
    typedef std::unique_ptr<Label> Label_uptr;
    typedef std::weak_ptr<Label> Label_wptr;
    
} /* end namespace planner */

#endif /* Label_hpp */
