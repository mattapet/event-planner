//===- TextField.hpp - Input UI object --------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file TextField.hpp
//===----------------------------------------------------------------------===//

#ifndef TextField_hpp
#define TextField_hpp

#include "View.hpp"
#include <ncurses.h>
#include <string>

namespace UI {
    
    class TextFieldDelegate;
    
    class TextField: public View {
    protected:
        bool               _underlined;
        std::string        _text;
        TextFieldDelegate *_delegate;
        
    public:
        TextField(WINDOW *win);
        TextField(WINDOW *win, bool underlined);
        
        virtual const std::string &text() const;
        
        virtual void setText(const std::string &text);
        
        virtual bool underlined() const;
        virtual void setUnderlined(bool underlined);
        
        virtual void draw() override;
        
        virtual TextFieldDelegate *delegate();
        
        virtual void setDelegate(TextFieldDelegate *delegate);
        
        virtual void keyPressedEnterKey() override;
        virtual void keyPressedBackspace() override;
        virtual void keyPressedTabKey() override;
        virtual void keyPressedShiftTabKey() override;
        
        virtual void keyPressedAlphanumText(char value) override;
        
    }; /* class TextField */
    
    
    class TextFieldDelegate: public UI::ResponderDelegate {
    public:
        // Optional method of the delegate to implement.
        virtual void textFieldDidReceiveReturn(TextField &textfiedl) {}
        virtual void textFieldDidUpdate(TextField &TextFieldd)        {}
        
    }; /* class TextFieldDelegate */
    
    typedef std::shared_ptr<TextField> TextField_ptr;
    typedef std::unique_ptr<TextField> TextField_uptr;
    typedef std::weak_ptr<TextField> TextField_wptr;
    
    
} /* end namesapce UI */


#endif /* TextField_hpp */
