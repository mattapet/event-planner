//===- Select.cpp ---------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Select.cpp
//===----------------------------------------------------------------------===//

#include "Select.hpp"
    
// MARK: Construction

using namespace UI;

Select::Select(WINDOW *win)
: View(win), _position(0), _delegate(nullptr)
{}

Select::Select(WINDOW *win, const std::vector<std::string> &options)
: View(win), _position(0), _options(options), _delegate(nullptr)
{}

// MARK: Protected implementation

void Select::getNextOption() noexcept {
    if (_options.size() && _position < _options.size() - 1) {
        _position += 1;
        reload();
    }
}

void Select::getPrevOption() noexcept {
    if (_position) {
        _position -= 1;
        reload();
    }
}

// MARK: Public implementation

std::string Select::option() const {
    return _options[_position];
}

SelectDelegate *Select::delegate() {
    return _delegate;
}

void Select::setDelegate(SelectDelegate *delegate) {
    _delegate = delegate;
}

void Select::draw() {
    if (_options.size()) {
        wprintw(_win, _options[_position].c_str());
    } else {
        wprintw(_win, "No available options.");
    }
}

void Select::keyPressedArrowKeyUp() {
    getNextOption();
}

void Select::keyPressedArrowKeyDown() {
    getPrevOption();
}

void Select::keyPressedTabKey() {
    if (_delegate) {
        _delegate->nextResponderShouldBecomeFirstResponder(*this);
    }
}

void Select::keyPressedShiftTabKey() {
    if (_delegate) {
        _delegate->prevResponderShouldBecomeFirstResponder(*this);
    }
}

// MARK: SelectDelegate

void SelectDelegate::selectDidChangeSelection(Select &) {}


