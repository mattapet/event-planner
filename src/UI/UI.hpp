//===- UI.hpp - UI module ---------------------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file UI.hpp
///
/// \brief This file contains only includes of all of the UI files. It is purely
///  a convenience header, so that the user does not have to include all files
///  separately.
///
/// \description The object structure and flow took inspiration from the
///  Apple's \c UIKit framework and the MVC architecture.
///  All of elements, that cooperates on handeling the UI, inherit from the
///  same base class called \c Responder. That gives all of the objects
///  ability to respond to events harvested and distributed by the
///  \c NotificationCenter object.
///
///  The base class for all of the views is the \c View. All of the views
///  should be constrolled by a class deriving from the \c VIewController
///  class, which is the base class for all of the controllers. The model part
///  of the MVC concept is not provided by the UI module, nor the module
///  has any requirements for them. The choice if purely on the developer.
///
/// \b Lifetime:
///  The main entry point for all programmes is the \c UIApplication object.
///  This object handles all of the initializations of the enviroment, prepares
///  the global \c NotificationCenter object and presents the initial view
///  controller.
///  All view controllers have their own deisgnated view, that they manages.
///  Upon initialization of view controllers, the view will be loaded and
///  displayed. All view controlles can override methods specifying the
///  lifecycle of the view e.g. \c viewDidLoad.
///  Both view controllers and views have avaialbe methods for displaying,
///  hiding and refreshing. Those methods are used mostly by the internal
///  implementation of \c View, \c ViewController, and other objects, that
///  manages the lower level interaction with the UI. The only method you
///  will be needing will be \c reload, to refresh the views after an update.
///
/// This module also provides some already created, not base classes, to start
///  the development of the application faster and/or to get a closer look on
///  how to properly use the base classes.
///  E.g. \c TextField, \c Button, \c Select
///
/// \see https://developer.apple.com/reference/uikit
/// \see https://en.wikipedia.org/wiki/Model–view–controller
//===----------------------------------------------------------------------===//

#ifndef UI_hpp
#define UI_hpp

#include "ViewController.hpp"
#include "View.hpp"
#include "Responder.hpp"

#include "Label.hpp"
#include "Select.hpp"
#include "Button.hpp"
#include "TextField.hpp"
#include "NavigationController.hpp"

#include "NotificationCenter.hpp"

#endif /* UI_hpp */
