//===- Select.hpp - Select UI object ----------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Select.hpp
//===----------------------------------------------------------------------===//

#ifndef Select_hpp
#define Select_hpp

#include "View.hpp"
#include <ncurses.h>
#include <string>
#include <vector>
#include <memory>

namespace UI {
    
    class SelectDelegate;
    
    class Select: public View {
    protected:
        size_t                    _position;
        std::vector<std::string>  _options;
        SelectDelegate         *_delegate;
        
        void getNextOption() noexcept;
        void getPrevOption() noexcept;
        
    public:
        Select(WINDOW *win);
        Select(WINDOW *win, const std::vector<std::string> &options);
        
        std::string option() const;
        
        SelectDelegate *delegate();
        void setDelegate(SelectDelegate *delegate);
        
        virtual void draw() override;
        
        virtual void keyPressedArrowKeyUp() override;
        virtual void keyPressedArrowKeyDown() override;
        virtual void keyPressedTabKey() override;
        virtual void keyPressedShiftTabKey() override;
        
    }; /* class Select */
    
    class SelectDelegate: public UI::ResponderDelegate {
    public:
        virtual void selectDidChangeSelection(Select &);
    }; /* class SelectDelegate */
    
    typedef std::shared_ptr<Select> Select_ptr;
    typedef std::unique_ptr<Select> Select_uptr;
    typedef std::weak_ptr<Select> Select_wptr;
    
    
} /* end namespace UI */

#endif /* Select_hpp */
