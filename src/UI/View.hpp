//===- View.hpp - UI view class ---------------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file View.hpp
///
/// \brief This file contains declaration of the \c View class, that is
///  one of the essential classes of this module. It also has declaration of
///  some other structures used throughout the module like \c Origin, \c Size
///  and \c Bounds.
//===----------------------------------------------------------------------===//

#ifndef View_hpp
#define View_hpp

#include "Responder.hpp"
#include <ncurses.h>
#include <iostream>
#include <vector>
#include <memory>
#include <exception>

namespace UI {
    
    /// Encapsulates origin of bounds in a single \c struct.
    struct Origin {
        /// The \i x position of the origin.
        int x;
        /// The \i y position of the origin.
        int y;
        
        /// Default constructor.
        Origin() = default;
        /// Convenience constructor.
        Origin(int x, int y);
        
        friend std::ostream &operator<<(std::ostream &os, const Origin &origin) {
            return os << "Origin ( x: " << origin.x
            << ", y: " << origin.y << " )";
        }
    }; /* struct Origin */
    
    /// Encapsualtes size of bounds in a single \c struct.
    struct Size {
        /// The width of the size.
        int width;
        /// The height of the size.
        int height;
        
        /// Defaul constructor.
        Size() = default;
        /// Convenience constructor.
        Size(int width, int height);
        
        friend std::ostream &operator<<(std::ostream &os, const Size &bounds) {
            return os << "Size ( width: " << bounds.width
            << ", height: " << bounds.height << " )";
        }
    }; /* struct Size */
    
    /// Encapsulates bounds of a view in a single \c struct.
    struct Bounds {
        /// The origin of the bounds.
        Origin origin;
        /// The size of the bounds.
        Size   size;
        
        /// Default cosntructor
        Bounds() = default;
        /// Convenience cosntructor
        Bounds(Origin origin, Size size);
        /// Convenience cosntructor
        Bounds(int x, int y, int width, int height);
        
        friend std::ostream &operator<<(std::ostream &os, const Bounds &bounds) {
            return os << "Bounds ( " << bounds.origin << ", " << bounds.size << " )\n";
        }
    }; /* struct Bounds */
    
    /// An exception, that is thrown, when there was a problem creating
    /// a subview of the view's \c WINDOW \c * object.
    /// The most common reason for this, are directly overlapping views and/or
    /// the wrong provided bounds for a view.
    class UISubwindowCreationException: public std::runtime_error {
    public:
        UISubwindowCreationException();
    };
    
    /// \brief The \c View is a prototype base class that handles a
    ///  rectangualr area of screen during the runtime.
    /// \description The \c View provides interface for shaping into different
    ///  sized, displaying, hiding, framing an area... For any other more
    ///  sophisticated behaviour, you need to subclass the \c View and
    ///  override the \c draw method, which modifies it's screen area. The
    ///  \c View has also interface to add other subviews, into it's own view
    ///  and also a subview can remove itself from the superview.
    ///
    /// \par
    ///  The \c View encapsulates a single \c ncurses \c WINDOW \c * object.
    ///  That is why, there is no default constructor, and \c never should be.
    ///  Upon initialization (calling the \c init method), the \c subview
    ///  function is used on the \c WINDOW object, provided to the constructor.
    ///  The resulting new \c WINDOW object is stored as the default object
    ///  and canvas for the view.
    ///
    /// \node On every subview, added to the view \c must be called the \c deinit
    ///  method before it leaves the scope, to prevent any memory leaks.
    ///
    /// \see http://invisible-island.net/ncurses/man/ncurses.3x.html
    class View: public Responder {
    protected:
        /// The \c ncurses window object.
        WINDOW *_win;
        /// The bounds of the view relative to the whole screen
        Bounds  _bounds;
        
        /// Boolean indicating, wheather the view should render framed or not.
        bool    _framed;
        /// Boolean indicating if the current view, and all of it's subviews
        /// are hideden.
        bool    _hidden;
        /// Pointer to the superview of the view.
        /// If the view is the top lever view, the value is \c nullptr.
        View *_superview;
        /// List of all subviews.
        std::vector<std::shared_ptr<View>> _subviews;
        
        /// MARK: Protected methods
        
        /// Fills the bounds of the given \c WINDOW \c * object.
        void fillBounds();

    public:
        /// \brief The default constructor for all of the \c View objects.
        /// \param win An \c ncurses \c WINDOW \c * object.
        View(WINDOW *win);
        
        /// \brief The convenience constructor for the \c View objects.
        /// \param win An \c ncurses \c WINDOW \c * object.
        /// \param superview The superview of the view.
        View(WINDOW *win, View *superview);
        
        /// The default constructor.
        virtual ~View() = default;
        
        /// \brief Designated initialization method of the \c View objects.
        /// \description The \c WINDOW \c * object given upon construction
        ///  is sbuviewed and bounds are properly set.
        ///
        /// \node The initialization method should be called only \b once during
        ///  the lifetime of an object.
        /// \throw UISubwindowCreationException
        virtual void init() override;
        
        /// \brief Convenience initialization method of the \c View objects.
        /// \description The \c WINDOW \c * object given upon construction
        ///  is sbuviewed and sets given bounds.
        ///
        /// \param bounds The bounds of the view.
        /// \node The initialization method should be called only \b once during
        ///  the lifetime of an object.
        /// \throw UISubwindowCreationException
        virtual void initWithBounds(Bounds bounds);
        
        /// \brief Designated deinitialization method of the \c View objects.
        /// \description This method should be used in pair with the \c init
        ///  and/or \c initWithBounds methods to assure, that all of the memory
        ///  is freed object's destruction.
        ///
        /// \node The deinitialization method should be called only \b once
        ///  during the lifetime of an object.
        virtual void deinit() override;
        
        /// Returns a boolean value identifying, if the current view is framed.
        /// \return \c true, if the current view is framed, \c false otherwise.
        bool framed() const;
        
        /// Sets the framed state of the view to the given value and refreshes
        /// the view.
        /// \param framed The value to set the view.
        void setFramed(bool framed);
        
        /// Returns a boolean value identifying view's hidden state.
        /// \return \c true, if the object is hidden, \c false otherwise.
        bool hidden() const;
        
        /// Hides and/or displays the view depending on the given value.
        /// \pram hidden The value to set the view to be.
        void setHidden(bool hidden);
        
        /// Dispalys the current view.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for a view controller
        ///  to manage the view.
        void display();
        
        /// Hides the current view.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for a view controller
        ///  to manage the view.
        void hide();
        
        /// Refreshes the current view and all of it's subviews.
        void reload();
        
        /// Displays subviews of the current view.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for a view controller
        ///  to manage the view.
        void displaySubviews();
        
        /// Hides subviews of the current view.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for a view controller
        ///  to manage the view.
        void hideSubviews();
        
        /// Reloads subviews of the current view.
        /// \note It is not recommended for the developer, to call this method
        ///  on an object itself. This method is intended for a view controller
        ///  to manage the view.
        void reloadSubviews();
        
        /// Adds a subview to the given view.
        /// \param subview A subview to be added to the current view.
        void addSubview(std::shared_ptr<View> subview);
        
        /// Removes the current view from it's superview.
        void removeFromSuperview();
        
        /// Method, that renders the current view onto the screen.
        /// It is not recommended to call \c reload or other methods handling
        /// the view from the \c draw method. The refresh is typically the next
        /// operation that happens after a view is drawn.
        virtual void draw();
        
    }; /* class View */
    
    typedef std::shared_ptr<View> View_ptr;
    typedef std::unique_ptr<View> View_uptr;
    typedef std::weak_ptr<View> View_wptr;
    
} /* end namespace UI */

#endif /* View_hpp */
