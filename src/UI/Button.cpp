//===- Button.cpp -------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Button.cpp
//===----------------------------------------------------------------------===//

#include "Button.hpp"

using namespace UI;
    
Button::Button(WINDOW *win, const std::string &text)
: View(win), _text(text), _delegate(nullptr)
{}

ButtonDelegate *Button::delegate() {
    return _delegate;
}

void Button::setDelegate(ButtonDelegate *delegate) {
    _delegate = delegate;
}

void Button::draw() {
    if (isFirstResponder()) {
        wattron(_win, A_STANDOUT);
    }
    wprintw(_win, _text.c_str());
    wattroff(_win, A_STANDOUT);
}

void Button::keyPressedEnterKey() {
    if (_delegate) {
        _delegate->buttonWasSelected(*this);
    }
}

void Button::keyPressedTabKey() {
    if (_delegate) {
        _delegate->nextResponderShouldBecomeFirstResponder(*this);
    }
}

void Button::keyPressedShiftTabKey() {
    if (_delegate) {
        _delegate->prevResponderShouldBecomeFirstResponder(*this);
    }
}
