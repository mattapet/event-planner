//===- NotificationCenter.hpp - Global event handling ----------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file NotificationCenter.hpp
//===----------------------------------------------------------------------===//

#ifndef NotificationCenter_hpp
#define NotificationCenter_hpp

#include "Responder.hpp"
#include <ncurses.h>
#include <vector>
#include <memory>

namespace UI {
    
    class Responder;
    
    class NotificationCenter {
        bool _setup;
        Responder *              _firstResponder;
        std::vector<Responder *> _responders;
        
        static NotificationCenter center;
        
    protected:
        NotificationCenter() = default;
        ~NotificationCenter() = default;
        
        void init();
        void deinit();
        bool isSetup() const noexcept;
        
        const Responder *_getFirstResponder() const noexcept;
        Responder *_getFirstResponder() noexcept;
        
        void _registerResponder(Responder *responder) noexcept;
        void _deregisterResponder(Responder *responder) noexcept;
        
        void _becomeFirstResponder(Responder *responder) noexcept;
        void _resignFirstResponder(Responder *responder) noexcept;
        
        Responder *_next() noexcept;
        
    public:
        // MARK: Static methods
        
        static void registerResponder(Responder *responder) noexcept;
        static void deregisterResponder(Responder *responder) noexcept;
        
        static Responder *firstResponder() noexcept;
        static void becomeFirstResponder(Responder *responder) noexcept;
        static void resignFirstResponder(Responder *responder) noexcept;
        
        static Responder *next() noexcept;
        
        static void shouldStopEventLoop() noexcept;
        
        static void listen();
        
    }; /* class Notification center */
    
} /* end namespace planner */

#endif /* NotificationCenter_hpp */
