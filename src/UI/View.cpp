//===- View.cpp -----------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file View.cpp
///
/// \brief This file contains implementaion of the \c View class, also has
///  implementations of some other structures used throughout the module
///  like \c Origin, \c Size and \c Bounds.
//===----------------------------------------------------------------------===//

#include "View.hpp"

using namespace UI;

// MARK: Construction and destruction

View::View(WINDOW *win)
: _win(win),
  _framed(false),
  _hidden(false),
  _superview(nullptr)
{}

View::View(WINDOW *win, View *superview)
: _win(win),
  _framed(false),
  _hidden(false),
  _superview(superview)
{}

// MARK: Protected implementation

void View::fillBounds() {
    getyx(_win, _bounds.origin.y, _bounds.origin.x);
    getmaxyx(_win, _bounds.size.height, _bounds.size.width);
}

// MARK: Public implementation

// MARK: Lifecycle methods

void View::init() {
    Responder::init();
    fillBounds();
    _win = subwin(
        _win,
        _bounds.size.height,
        _bounds.size.width,
        _bounds.origin.y,
        _bounds.origin.x
    );
}

void View::initWithBounds(Bounds bounds) {
    Responder::init();
    _bounds = bounds;
    _win = subwin(
        _win,
        _bounds.size.height,
        _bounds.size.width,
        _bounds.origin.y,
        _bounds.origin.x
    );
    if (!_win) {
        throw UISubwindowCreationException();
    }
}

void View::deinit() {
    delwin(_win);
}

bool View::framed() const {
    return _framed;
}

void View::setFramed(bool framed) {
    if (framed != _framed) {
        _framed = framed;
        reload();
    }
}

bool View::hidden() const {
    return _hidden;
}

void View::setHidden(bool hidden) {
    if (_hidden != hidden) {
        if (hidden) {
            hide();
        } else {
            display();
        }
    }
}

// MARK: View managins methods

void View::display() {
    draw();
    displaySubviews();
    touchwin(_win);
    wrefresh(_win);
    _hidden = false;
}

void View::hide() {
    hideSubviews();
    werase(_win);
    touchwin(_win);
    wrefresh(_win);
    _hidden = true;
}

void View::reload() {
    werase(_win);
    if (_framed) {
        box(_win, 0, 0);
    }
    draw();
    reloadSubviews();
    touchwin(_win);
    wrefresh(_win);
}

void View::displaySubviews() {
    for (auto &subview : _subviews) {
        subview->display();
    }
}

void View::hideSubviews() {
    for (auto &subview : _subviews) {
        subview->hide();
    }
}

void View::reloadSubviews() {
    for (auto &subview : _subviews) {
        subview->reload();
    }
}

// MARK: Subview managin methods.

void View::addSubview(std::shared_ptr<View> subview) {
    subview->_superview = this;
    _subviews.push_back(subview);
}

void View::removeFromSuperview() {
    if (_superview) {
        auto it = _superview->_subviews.begin();
        for (;;) {
            if (it == _superview->_subviews.end()) {
                break;
            }
            if ((*it).get() == this) {
                _superview->_subviews.erase(it);
                (*it)->_superview = nullptr;
                break;
            }
            ++it;
        }
    }
}

// MARK: Draw method.

void View::draw() {}    

// MARK: UISubwindowCreationException
UISubwindowCreationException::UISubwindowCreationException()
: std::runtime_error("There was an error while creating a new subview.")
{}

// MARK: Origin

Origin::Origin(int x, int y): x(x), y(y) {}

//std::ostream &operator<<(std::ostream &os, const Origin &origin) {
//    return os << "Origin ( x: " << origin.x
//    << ", y: " << origin.y << " )";
//}


// MARK: Size

Size::Size(int width, int height): width(width), height(height) {}

//std::ostream &operator<<(std::ostream &os, const Size &bounds) {
//    return os << "Size ( width: " << bounds.width
//    << ", height: " << bounds.height << " )";
//}

// MARK: Bounds

Bounds::Bounds(Origin origin, Size size): origin(origin), size(size) {}
Bounds::Bounds(int x, int y, int width, int height)
: origin({x, y}), size({width, height}) {}

//std::ostream &operator<<(std::ostream &os, const Bounds &bounds) {
//    return os << "Bounds ( " << bounds.origin << ", " << bounds.size << " )\n";
//}
