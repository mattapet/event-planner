//===- NavigationController.cpp -------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file NavigationController.cpp
//===----------------------------------------------------------------------===//

#include "NavigationController.hpp"

using namespace UI;
    
NavigationController::NavigationController(WINDOW *win)
: ViewController(win), _rootVC(nullptr)
{}

void NavigationController::viewDidLoad() {
    ViewController::viewDidLoad();
    _VCStack.push_back(_rootVC);
    addChildViewController(_rootVC);
}

ViewController_ptr NavigationController::topViewController() {
    return _VCStack.back();
}

void NavigationController::
pushViewController(ViewController_ptr VC)
{
    topViewController()->hide();
    topViewController()->removeFromParentViewController();
    _VCStack.push_back(VC);
    addChildViewController(topViewController());
    topViewController()->display();
}

void NavigationController::popViewController() {
    if (topViewController() != _rootVC) {
        topViewController()->hide();
        topViewController()->removeFromParentViewController();
        _VCStack.pop_back();
        addChildViewController(topViewController());
        topViewController()->display();
    }
}

void NavigationController::popToRootViewController() {
    for (;;) {
        if (topViewController() == _rootVC) {
            break;
        }
        popToRootViewController();
    }
}
