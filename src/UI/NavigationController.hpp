//===- NavigationController.hpp - Navigation controller ---------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file NavigationController.hpp
//===----------------------------------------------------------------------===//

#ifndef NavigationController_hpp
#define NavigationController_hpp

#include "ViewController.hpp"
#include <ncurses.h>
#include <vector>

namespace UI {
    
    class NavigationController: public ViewController {
    protected:
        ViewController_ptr _rootVC;
        std::vector<ViewController_ptr> _VCStack;
        
    public:
        NavigationController(WINDOW *win);
        
        ViewController_ptr topViewController();
        
        virtual void viewDidLoad() override;
        
        void pushViewController(ViewController_ptr VC);
        void popViewController();
        void popToRootViewController();
        
    }; /* class NavigationController */
    
} /* end namespace planner */

#endif /* NavigationController_hpp */
