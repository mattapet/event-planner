//===- Button.hpp - Button UI object ----------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Button.hpp
//===----------------------------------------------------------------------===//

#ifndef Button_hpp
#define Button_hpp

#include "View.hpp"
#include <ncurses.h>
#include <string>
#include <memory>

namespace UI {
    
    class ButtonDelegate;
    
    class Button: public View {
    protected:
        std::string _text;
        ButtonDelegate *_delegate;
        
    public:
        Button(WINDOW *win, const std::string &text);
        
        ButtonDelegate *delegate();
        void setDelegate(ButtonDelegate *delegate);
        
        virtual void draw() override;

        virtual void keyPressedEnterKey() override;
        virtual void keyPressedTabKey() override;
        virtual void keyPressedShiftTabKey() override;
        
    }; /* class Button */

    typedef std::shared_ptr<Button> Button_ptr;
    typedef std::unique_ptr<Button> Button_uptr;
    typedef std::weak_ptr<Button> Button_wptr;
    
    class ButtonDelegate: public ResponderDelegate {
    public:
        virtual void buttonWasSelected(Button &button) = 0;
    }; /* class ButtonDelegate */
    
} /* end namespace planner */

#endif /* Button_hpp */
