//===- Responder.hpp - Base responder ---------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Responder.hpp
//===----------------------------------------------------------------------===//

#ifndef Responder_hpp
#define Responder_hpp

#include "NotificationCenter.hpp"
#include <ncurses.h>

namespace UI {
    
    class NotificationCenter;
    
    class ResponderDelegate;
    
    class Responder {
    public:
        Responder();
        Responder(const Responder &other) = delete;
        virtual ~Responder();
        
        virtual void init();
        virtual void deinit();
        
        bool isFirstResponder() const noexcept;
        void becomeFirstResponder() noexcept;
        void resignFirstResponder() noexcept;
        
        Responder *next() const noexcept;
        
        // MARK: Responding methods.
        virtual void keyPressedEnterKey();
        virtual void keyPressedBackspace();
        virtual void keyPressedEscapeKey();
        virtual void keyPressedTabKey();
        virtual void keyPressedShiftTabKey();
        
        virtual void keyPressedAlphanumText(char value);
        
        virtual void keyPressedArrowKeyUp();
        virtual void keyPressedArrowKeyDown();
        virtual void keyPressedArrowKeyRight();
        virtual void keyPressedArrowKeyLeft();
        
    }; /* class Responder */
    
    class ResponderDelegate {
    public:
        virtual void nextResponderShouldBecomeFirstResponder(Responder &);
        virtual void prevResponderShouldBecomeFirstResponder(Responder &);
        
    }; /* class ResponderDelegate */
    
} /* end namespace UI */

#endif /* Responder_hpp */
