//===- Label.cpp ----------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Label.cpp
//===----------------------------------------------------------------------===//

#include "Label.hpp"

using namespace UI;
    
Label::Label(WINDOW *win)
: View(win)
{}

Label::Label(WINDOW *win, const std::string &text)
: View(win), _text(text)
{}

const std::string &Label::text() const {
    return _text;
}

void Label::setText(const std::string &text) {
    _text = text;
    reload();
}

void Label::draw() {
    wprintw(_win, _text.c_str());
}
