//===- CalendarViewController.cpp -----------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file CalendarViewController.cpp
//===----------------------------------------------------------------------===//

#include "CalendarViewController.hpp"

#include <unistd.h>

using namespace planner;
    
// MARK: Constructors

CalendarViewController::CalendarViewController(WINDOW *win) noexcept
: UI::ViewController(win),
  _selectedRow(0),
  _selectedCol(0),
  _monthDate(Date::now().startOf(Date::TimeUnit::Month)),
  _interval(monthIntervalForDate(Date::now())),
  _calendarItems({})
{}

CalendarViewController::
CalendarViewController(WINDOW *win, std::vector<Calendar_ptr> calendars) noexcept
: UI::ViewController(win),
  _selectedRow(0),
  _selectedCol(0),
  _interval(monthIntervalForDate(Date::now())),
  _calendars(calendars),
  _calendarItems({})
{}

CalendarViewController::~CalendarViewController() throw()
{}

std::vector<Event_ptr> CalendarViewController::eventsForCalendars() const {
    auto events = std::vector<Event_ptr>();
    for (auto calendar : _calendars) {
        for (auto event : calendar->eventsForDateInterval(_interval)) {
            events.push_back(event);
        }
    }
    return events;
}

// MARK: Delegate management.

CalendarViewControllerDelegate *CalendarViewController::delegate() noexcept {
    return _delegate;
}

void CalendarViewController::
setDelegate(CalendarViewControllerDelegate *delegate) noexcept
{
    _delegate = delegate;
}

// MARK: Protected Implementation

void CalendarViewController::addCalendarItems() {
    int row = 0;
    int col = 0;
    auto events = eventsForCalendars();
    for (auto date : _interval) {
        auto day_ = DateInterval{date, (time_t)date.add(24, Date::TimeUnit::Hour) - 1};
        auto eventsForDay = std::vector<Event_ptr>();
        for (auto event : events) {
            if (event->interval().intersects(day_)) {
                eventsForDay.push_back(event);
            }
        }
        
        _calendarItems.push_back(
            std::make_shared<CalendarItem>(
                _win, date, eventsForDay
            )
        );
        _calendarItems.back()->initWithBounds({
            _bounds.origin.x + row * _itemSize.width + 2,  // for margin
            _bounds.origin.y + col * _itemSize.height + 2, // for margin
            _itemSize.width,
            _itemSize.height
        });
        _view->addSubview(_calendarItems.back());
        col += ++row / 7;
        row %= 7;
    }
}

void CalendarViewController::removeCalendarItems() {
    for (auto item : _calendarItems) {
        item->removeFromSuperview();
    }
    _calendarItems.clear();
}

// MARK: Public Implementation

void CalendarViewController::initWithBounds(UI::Bounds bounds) {
    UI::ViewController::initWithBounds(bounds);
    _itemSize.height = (_bounds.size.height - 1) / kROW_COUNT;
    _itemSize.width = _bounds.size.width / kCOL_COUNT;
    std::vector<std::string> weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    for (size_t i = 0; i < kCOL_COUNT; i++) {
        _weekdayLabels.push_back(std::make_shared<UI::Label>(_win, weekDays[i]));
    }
}

void CalendarViewController::deinit() {
    removeCalendarItems();
    for (auto weekdayLabel : _weekdayLabels) {
        weekdayLabel->deinit();
    }
    UI::ViewController::deinit();
}

void CalendarViewController::viewDidLoad() {
    UI::ViewController::viewDidLoad();
    _view->setFramed(true);
    for (size_t i = 0; i < kCOL_COUNT; i++) {
        _weekdayLabels[i]->initWithBounds({
            _bounds.origin.x + 3 + (int)i * _itemSize.width,
            _bounds.origin.y + 1,
            _itemSize.width,
            1
        });
        _view->addSubview(_weekdayLabels[i]);
    }
    addCalendarItems();
}

void CalendarViewController::viewWillUnload() {
    removeCalendarItems();
    UI::ViewController::viewWillUnload();
}

void CalendarViewController::
setCalendars(std::vector<Calendar_ptr> calendars) noexcept
{
    _calendars = calendars;
    removeCalendarItems();
    addCalendarItems();
    _view->reload();
}

// MARK: Calendar paging methods.

void CalendarViewController::next() {
    removeCalendarItems();
    _monthDate += Date::TimeUnit::Month;
    _interval = monthIntervalForDate(_monthDate);
    addCalendarItems();
    _view->reload();
}

void CalendarViewController::prev() {
    removeCalendarItems();
    _monthDate -= Date::TimeUnit::Month;
    _interval = monthIntervalForDate(_monthDate);
    addCalendarItems();
    _view->reload();
}

// MARK: Date selection methods.

void CalendarViewController::startSelection() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    item->setSelected(true);
}

void CalendarViewController::selectUp() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    if (_selectedRow) {
        item->setSelected(false);
        item = _calendarItems[--_selectedRow * kCOL_COUNT + _selectedCol];
        item->setSelected(true);
    }
}

void CalendarViewController::selectDown() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    if (_selectedRow < kROW_COUNT - 1) {
        item->setSelected(false);
        item = _calendarItems[++_selectedRow * kCOL_COUNT + _selectedCol];
        item->setSelected(true);
    }
}

void CalendarViewController::selectLeft() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    if (_selectedCol) {
        item->setSelected(false);
        item = _calendarItems[_selectedRow * kCOL_COUNT + --_selectedCol];
        item->setSelected(true);
    }
}

void CalendarViewController::selectRight() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    if (_selectedCol < kCOL_COUNT - 1) {
        item->setSelected(false);
        item = _calendarItems[_selectedRow * kCOL_COUNT + ++_selectedCol];
        item->setSelected(true);
    }
}

void CalendarViewController::chooseSelectedEvents() noexcept {
    if (_delegate) {
        auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
        _delegate->calendarViewControllerSelectedDateWithEvents(item->events());
    }
}

void CalendarViewController::chooseSelectedDate() noexcept {
    if (_delegate) {
        auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
        _delegate->calendarViewControllerSelectedDate(item->date());
    }
}

void CalendarViewController::selectDate(Date date) {
    _monthDate = date.startOf(Date::TimeUnit::Month);
    _interval = monthIntervalForDate(_monthDate);
    removeCalendarItems();
    addCalendarItems();
    for (size_t i = 0; i < kROW_COUNT * kCOL_COUNT; i++) {
        if (_calendarItems[i]->date() == date) {
            _selectedRow = i / kCOL_COUNT;
            _selectedCol = i % kCOL_COUNT;
            _calendarItems[i]->setSelected(true);
            return;
        }
    }
}

void CalendarViewController::stopSelection() noexcept {
    auto item = _calendarItems[_selectedRow * kCOL_COUNT + _selectedCol];
    item->setSelected(false);
}

// MARK: Static methods.

DateInterval
CalendarViewController::monthIntervalForDate(Date date) noexcept {
    auto dateStart = date.startOf(Date::TimeUnit::Month);
    auto dateEnd   = date.startOf(Date::TimeUnit::Month);
    dateStart = dateStart.add(- DateFormatter::weekDay(dateStart),
                              Date::TimeUnit::Day);
    int dayDiff = (42 - (int)(dateEnd - dateStart) / Date::kSECONDS_PER_DAY);
    dateEnd = dateEnd.add(dayDiff, Date::TimeUnit::Day);
    return {
        dateStart,
        dateEnd
    };
}


// MARK: Delegate

void CalendarViewControllerDelegate::calendarViewControllerSelectedDate(Date date) {}
void CalendarViewControllerDelegate::calendarViewControllerSelectedDateWithEvents(std::vector<Event_ptr> events) {}


