//===- CalendarItem.cpp ---------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file CalendarItem.cpp
//===----------------------------------------------------------------------===//

#include "CalendarItem.hpp"
#include <ncurses.h>

using namespace planner;

CalendarItem::CalendarItem(WINDOW *win, Date date,
                           std::vector<Event_ptr> events) noexcept
: UI::View(win),
  _selected(false),
  _date(date),
  _events(events)
{}

std::vector<Event_ptr> CalendarItem::events() const noexcept {
    return _events;
}

Date CalendarItem::date() const noexcept {
    return _date;
}

bool CalendarItem::selected() const noexcept {
    return _selected;
}

void CalendarItem::setSelected(bool selected) noexcept {
    if (selected != _selected) {
        _selected = selected;
        reload();
    }
}

void CalendarItem::draw() {
    if (_selected) {
        wattron(_win, A_STANDOUT);
    }
    wprintw(_win, "%s - %d",
            DateFormatter::stringFromDate(_date, "%m/%d").c_str(),
            _events.size());
    wattroff(_win, A_STANDOUT);
}
