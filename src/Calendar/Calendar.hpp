//===- Calendar.hpp - Calendar model ----------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Calendar.hpp
///
/// \brief This file contains declaration of the \c Calendar class, which is
///  supposed to encapsulate events of a single user.
//===----------------------------------------------------------------------===//

#ifndef Calendar_hpp
#define Calendar_hpp

#include "core/protocols/JSONable.hpp"
#include "core/protocols/ICalable.hpp"
#include "core/Date.hpp"
#include "EventLoader/EventLoader.hpp"
#include "Event/Event.hpp"
#include "Event/RecurringEvent.hpp"

#include <string>
#include <memory>
#include <vector>
#include <exception>

namespace planner {
    
    /// The exception is thrown, when there is an error while creating an event.
    class EventCreationException: public std::runtime_error {
    public:
        EventCreationException(const char *desc);
    };
    
    /// \brief Calendar object encapsulates all events of a single user.
    ///
    /// \description The \c Calendar object requires an \c EventLoader object,
    ///  which it uses to load, save and in any way manage the events of the
    ///  user. The user is uniquely specified by a single UID string. Most
    ///  common choice for the UID is an email. However any string identifier
    ///  suits the role fine, if it is guaranteed, to be unique for a user.
    class Calendar: public protocol::ICalable, protocol::JSONable {
    public:
        
        /// Available calendar types. Right now there is avaialable only
        /// Gregorian version of calendar.
        enum CalendarType {
            Gregorian
        };
        
    protected:
        // Calendar meta.
        std::string         _uid;
        CalendarType        _type;
        EventLoader_ptr     _loader;
        Date::TimeUnit      _calendarUnit;
        // Calendar own data.
        DateInterval        _interval;
        std::set<Event_ptr> _recurringEvents;
        std::set<Event_ptr> _events;
        
        /// \brief Ceils the provided interval in a way, that the newly created
        /// interval will encontain it fully.
        ///
        /// \decription The start will be the closest start of a month to the
        ///  \c dateStart property of interval and end will be the start of the
        ///  month, closest to the \c dateEnd property of interval.
        /// \param interval Interval to ceil.
        /// \return An interval, that encontains the given interval.
        DateInterval ceilInterval(const DateInterval &interval) const noexcept;
        
        /// Loads events for the current date interval.
        virtual void load();
        virtual void reload();
        virtual void save();
        
        /// Instantiates recurring events for the provided date interval.
        void instantiateRecurringEvents(DateInterval &interval,
                                        std::set<Event_ptr> &events) const;
        
        /// Adds a recurring event.
        virtual void addRecurringEvent(Event_ptr event);
        /// Adds a regular event.
        virtual void addSingleEvent(Event_ptr event);
        
        /// Removes a recurring event.
        virtual void removeRecurringEvent(Event_ptr event);
        /// Removes a regular event.
        virtual void removeSingleEvent(Event_ptr event);
        
    public:
        // MARK: Lifetime of instances.
        
        /// Designated constructor of a \c Calendar.
        /// \param uid A UID of the user.
        /// \param loader A loader that will be used to manage the events.
        explicit Calendar(const std::string &uid, EventLoader_ptr loader);
        
        /// Convenience constructor of a \c Calendar.
        /// \param uid A UID of the user.
        /// \param loader A loader that will be used to manage the events.
        /// \param interval An initial interval to load.
        explicit Calendar(const std::string &uid,
                          EventLoader_ptr loader,
                          const DateInterval &interval);
        
        /// Movable constructor of a \c Calendar.
        /// \param uid A UID of the user.
        /// \param loader A loader that will be used to manage the events.
        /// \param interval An initial interval to load.
        Calendar(std::string &&uid,
                 EventLoader_ptr &&loader,
                 DateInterval &&interval);
        
        /// Convenience constructor of a \Calendar using a JSON object.
        /// \param j A JSON object used to create the instance.
        explicit Calendar(const json &j, EventLoader_ptr loader);
        
        /// Default descructor of a \c Destructor.
        virtual ~Calendar() throw();
        
        // MARK: Accessing parameters
        
        /// Returns UID of the calendar.
        virtual const std::string &uid() const noexcept;
        /// Returns current date interval.
        virtual const DateInterval &interval() const noexcept;
        /// Returns currently loaded events.
        virtual const std::set<Event_ptr> &events() const noexcept;
        /// Returns currently loaded events.
        virtual std::set<Event_ptr> &events() noexcept;
        
        /// Returns events in given dateRange.
        virtual std::vector<Event_ptr>
        eventsForDateInterval(const DateInterval &interval) noexcept;
        
        // MAKR: Mutating calendar state
        
        /// Returns a JSON format of a calendar.
        /// \return JSON format of a calendar.
        virtual json toJSON() const override;
        
        /// Returns a JSON format of a calendar with sharable events.
        /// \return JSON format of a calendar.
        virtual json toJSONShared() const;
        
        /// \brief Converts the calendar into an iCal string format.
        ///
        /// \description Loads all events of the calendar and formats each one
        ///  of them into iCal format string, that it returns.
        /// \return A single string containing all events of the calendar
        ///  in the iCal format.
        virtual std::string toICalString() const override;
        
        /// Returns a date interval, closest to the given date interval,
        /// that encapsulates the whole interval and resolves any colissions.
        virtual DateInterval findAvailable(DateInterval interval) const;
        
        /// \brief Checks fi there is available interval.
        /// \returns \c true if there is a avaialable spot for given interval,
        ///  \c false otherwise.
        virtual bool isAvailable(const DateInterval &interval) const;
        
        /// \brief Checks if there is avaialable time spot for given event.
        /// \return \c true if there is available spot for the event, \c false
        ///  otherwise.
        virtual bool isAvailable(Event_ptr event) const;
        
        /// \brief Checks if there is avaialable time spot for given recurring
        ///  event.
        /// \return \c true if there is available spot for the recurrgin event,
        ///  \c false otherwise.
        virtual bool isAvailable(RecurringEvent_ptr event) const;
        
        /// Adds a new event.
        virtual void addEvent(Event_ptr event);
        /// Removes an event.
        virtual void removeEvent(Event_ptr event);
        
        /// Returns the maximum date interval, which can be operate on.
        static DateInterval maxDateInterval() noexcept;
        
        // MARK: JSON compatibility implementation.
        
//        /// JSON library compatability function.
//        /// Converts \c DateIterval to appropriate JSON format.
//        void to_json(nlohmann::json &j, const Calendar &interval) noexcept;
//        
//        /// JSON library compatability function.
//        /// Converts appropriate JSON format to \c DateInterval.
//        void from_json(const nlohmann::json &j, Calendar &interval);
        
    }; /* class Calendar */
    
    /* Convenience typedefs. */
    typedef std::shared_ptr<Calendar> Calendar_ptr;
    typedef std::unique_ptr<Calendar> Calendar_uptr;
    typedef std::weak_ptr<Calendar> Calendar_wptr;
    
} /* end namesapce planner */

#endif /* Calendar_hpp */
