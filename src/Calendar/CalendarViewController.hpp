//===- CalendarViewController.hpp - Calendar view controller ----*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file CalendarViewController.hpp
//===----------------------------------------------------------------------===//

#ifndef CalendarViewController_hpp
#define CalendarViewController_hpp

#include "UI/UI.hpp"
#include "EventLoader/EventLoader.hpp"
#include "Calendar.hpp"
#include "CalendarItem.hpp"

#include <string>
#include <vector>
#include <memory>
#include <ncurses.h>

namespace planner {
    
    class CalendarViewControllerDelegate;
    
    class CalendarViewController: public UI::ViewController {
    protected:
        const static size_t kROW_COUNT = 6;
        const static size_t kCOL_COUNT = 7;
        
        size_t                          _selectedRow;
        size_t                          _selectedCol;
        UI::Size                        _itemSize;
        Date                            _monthDate;
        DateInterval                    _interval;
        std::vector<Calendar_ptr>       _calendars;
        std::vector<CalendarItem_ptr>   _calendarItems;
        std::vector<UI::Label_ptr>      _weekdayLabels;
        CalendarViewControllerDelegate *_delegate;
        
        void addCalendarItems();
        void removeCalendarItems();
        
    public:
        explicit CalendarViewController(WINDOW *win) noexcept;
        explicit CalendarViewController(WINDOW *win,
                                        std::vector<Calendar_ptr> calendars) noexcept;
        virtual ~CalendarViewController() throw();
        
        virtual void initWithBounds(UI::Bounds bounds) override;
        virtual void deinit() override;
        
        virtual std::vector<Event_ptr> eventsForCalendars() const;
        virtual void viewDidLoad() override;
        virtual void viewWillUnload() override;
        
        // MARK: Delegate managing.
        CalendarViewControllerDelegate *delegate() noexcept;
        void setDelegate(CalendarViewControllerDelegate *delegate) noexcept;
        
        virtual void setCalendars(std::vector<Calendar_ptr> calendars) noexcept;
        
        // MARK: Calendar paging
        virtual void next();
        virtual void prev();
        
        // MARK: Date selection
        virtual void startSelection() noexcept;
        virtual void selectUp() noexcept;
        virtual void selectDown() noexcept;
        virtual void selectRight() noexcept;
        virtual void selectLeft() noexcept;
        virtual void chooseSelectedEvents() noexcept;
        virtual void chooseSelectedDate() noexcept;
        virtual void selectDate(Date date);
        virtual void stopSelection() noexcept;
        
        // MARK: Static methods
        static DateInterval monthIntervalForDate(Date date) noexcept;
        
    }; /* class CalendarViewController */
    
    typedef std::shared_ptr<CalendarViewController> CalendarViewController_ptr;
    typedef std::unique_ptr<CalendarViewController> CalendarViewController_uptr;
    typedef std::weak_ptr<CalendarViewController> CalendarViewController_wptr;

    
    class CalendarViewControllerDelegate {
    public:
        /// Optional
        virtual void calendarViewControllerSelectedDate(Date date);
        /// Optional
        virtual void calendarViewControllerSelectedDateWithEvents(std::vector<Event_ptr> events);
    }; /* CalendarViewControllerDelegate */
    
} /* end namespace planner */


#endif /* CalendarViewController_hpp */
