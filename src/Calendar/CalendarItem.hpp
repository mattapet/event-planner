//===- CalendarItem.hpp - Calendar item view --------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file CalendarItem.hpp
//===----------------------------------------------------------------------===//

#ifndef CalendarItem_hpp
#define CalendarItem_hpp

#include "UI/UI.hpp"
#include "core/Date.hpp"
#include "Event/Event.hpp"

#include <vector>
#include <memory>
#include <ncurses.h>

namespace planner {
    
    class CalendarItem: public UI::View {
    protected:
        bool                   _selected;
        Date                   _date;
        std::vector<Event_ptr> _events;
        
    public:
        /// Designated constructor for \c CalendarItem.
        explicit CalendarItem(WINDOW *win,
                              Date date,
                              std::vector<Event_ptr> events = {}) noexcept;
        
        /// Designated destructor for \c CalendarItem.
        virtual ~CalendarItem() = default;
        
        std::vector<Event_ptr> events() const noexcept;
        
        Date date() const noexcept;
        
        bool selected() const noexcept;
        
        void setSelected(bool selected) noexcept;
        
        virtual void draw() override;
        
    }; /* class CalenderItem */
    
    typedef std::shared_ptr<CalendarItem> CalendarItem_ptr;
    typedef std::unique_ptr<CalendarItem> CalendarItem_uptr;
    typedef std::weak_ptr<CalendarItem> CalendarItem_wptr;
    
} /* end namespace planner */

#endif /* CalendarItem_hpp */
