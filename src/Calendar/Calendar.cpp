//===- Calendar.cpp - Calendar implementation -----------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Calendar.cpp
///
/// \brief This file contains implementation of a Calendar class and all
///  associated exceptions.
//===----------------------------------------------------------------------===//

#include "Calendar.hpp"
#include <algorithm>

using namespace planner;

// MARK: EventCreationException

EventCreationException::EventCreationException(const char *desc)
: std::runtime_error(desc) {}

// MARK: Constructors

Calendar::Calendar(const std::string &uid, EventLoader_ptr loader)
: _uid(uid),
  _type(CalendarType::Gregorian),
  _loader(loader),
  _calendarUnit(Date::TimeUnit::Month),
  _interval({
      Date::now().startOf(Date::TimeUnit::Month),
      Date::now().add(1, Date::TimeUnit::Month)
      .startOf(Date::TimeUnit::Month),
      Date::TimeUnit::Month
  })
{
    load();
}

Calendar::Calendar(const std::string &uid,
                   EventLoader_ptr loader,
                   const DateInterval &interval)
: _uid(uid),
  _type(CalendarType::Gregorian),
  _loader(loader),
  _calendarUnit(Date::TimeUnit::Month),
  _interval(interval)
{
    load();
}

Calendar::Calendar(std::string &&uid,
                   EventLoader_ptr &&loader,
                   DateInterval &&interval)
: _uid(uid),
  _type(CalendarType::Gregorian),
  _loader(std::move(loader)),
  _calendarUnit(Date::TimeUnit::Month),
  _interval(std::move(interval))
{
    load();
}

Calendar::Calendar(const json &j, EventLoader_ptr loader)
: _loader(loader),
  _interval(maxDateInterval())
{
    _uid = j["uid"];
    for (auto event : j["recurring_events"]) {
        _recurringEvents.insert(std::make_shared<RecurringEvent>(event));
    }
    for (auto event : j["single_events"]) {
        _events.insert(std::make_shared<Event>(event));
    }
    instantiateRecurringEvents(_interval, _events);
}

Calendar::~Calendar() throw() {
    save();
}

// MARK: Protected Implemenation


DateInterval Calendar::
ceilInterval(const DateInterval &interval) const noexcept
{
    return {
        interval.dateStart().startOf(Date::TimeUnit::Month),
        interval.dateEnd().add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month)
    };
}

void Calendar::load() {
    auto interval = maxDateInterval();
    _loader->loadRecurringEventsForUID(_uid);
    _recurringEvents = _loader->events();
    _loader->loadDateIntervalForUID(_uid, interval);
    _events = _loader->events();
    instantiateRecurringEvents(interval, _events);
}

void Calendar::reload() {
    _loader->loadDateIntervalForUID(_uid, _interval);
    _events = _loader->events();
    instantiateRecurringEvents(_interval, _events);
}

void Calendar::save() {
    _loader->saveEventsForUID(_uid, _events);
    _loader->saveRecurringEventsForUID(_uid, _recurringEvents);
}

void Calendar::instantiateRecurringEvents(DateInterval &interval,
                                          std::set<Event_ptr> &events) const {
    for (auto event : _recurringEvents) {
        auto event_ = std::dynamic_pointer_cast<RecurringEvent>(event);
        auto eventInstances = event_->instantiate();
        events.insert(eventInstances.begin(), eventInstances.end());
    }
}

void Calendar::addRecurringEvent(Event_ptr event) {
    _recurringEvents.insert(event);
}

void Calendar::addSingleEvent(Event_ptr event) {
    auto interval_ = DateInterval(
        event->interval().dateStart().startOf(Date::TimeUnit::Month),
        event->interval().dateEnd().add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month),
        Date::TimeUnit::Month
    );
    _loader->loadDateIntervalForUID(_uid, interval_);
    auto events = _loader->events();
    events.insert(event);
    _loader->saveEventsForUID(_uid, events);
}


void Calendar::removeRecurringEvent(Event_ptr event) {
    for (auto event_ : _recurringEvents) {
        if (event_ == event) {
            _recurringEvents.erase(event_);
            break;
        }
    }
    for (auto it = _events.begin(); it != _events.end(); ++it) {
        if ((*it) == event) {
            _events.erase(it);
        }
    }
}

void Calendar::removeSingleEvent(Event_ptr event) {
    auto interval_ = DateInterval(
        event->interval().dateStart().startOf(Date::TimeUnit::Month),
        event->interval().dateEnd().add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month),
        Date::TimeUnit::Month
    );
    _loader->loadDateIntervalForUID(_uid, interval_);
    auto events = _loader->events();
    events.erase(event);
    _loader->saveEventsForUID(_uid, events);
}


// MARK: Public implementation

const std::string &Calendar::uid() const noexcept {
    return _uid;
}

const DateInterval &Calendar::interval() const noexcept {
    return _interval;
}

const std::set<Event_ptr> &Calendar::events() const noexcept {
    return _events;
}

std::set<Event_ptr> &Calendar::events() noexcept {
    return _events;
}

std::vector<Event_ptr>
Calendar::eventsForDateInterval(const DateInterval &interval) noexcept {
    auto events = std::vector<Event_ptr>();
    if (!_events.size()) {
        return events;
    }
    auto it = std::upper_bound(_events.begin(), _events.end(),
                               interval, std::less<DateInterval>());
    for (;;) {
        if (it == _events.end()
            || (*it)->interval().dateStart() > interval.dateEnd()) {
            break;
        }
        events.push_back(*it);
        ++it;
    }
    return events;
}

json Calendar::toJSON() const {
    auto single = std::vector<json>();
    for (auto event : _events) {
        if (!event->isRecurring()) {
            single.push_back(event->toJSON());
        }
    }
    auto recurring = std::vector<json>();
    for (auto event : _recurringEvents) {
        single.push_back(event->toJSON());
    }
    return {
        {"uid", _uid},
        {"type", "Gregorian"},
        {"single_events", single},
        {"recurring_events", recurring}
    };
}

json Calendar::toJSONShared() const {
    auto single = std::vector<json>();
    for (auto event : _events) {
        if (event->privacy() == PrivacyOption::Private
            || event->isRecurring()) {
            continue;
        }
        single.push_back(event->toJSON());
    }
    auto recurring = std::vector<json>();
    for (auto event : _recurringEvents) {
        if (event->privacy() == PrivacyOption::Private) {
            continue;
        }
        single.push_back(event->toJSON());
    }
    return {
        {"uid", _uid},
        {"type", "Gregorian"},
        {"single_events", single},
        {"recurring_events", recurring}
    };
}

std::string Calendar::toICalString() const {
    std::stringstream ss;
    ss << "BEGIN:VCALENDAR\nVERSION:2.0\n";
    _loader->loadAll(_uid);
    for (auto event : _loader->events()) {
        ss << event->toICalString();
    }
    for (auto event : _recurringEvents) {
        ss << event->toICalString();
    }
    ss << "END:VCALENDAR\n";
    return ss.str();
}

DateInterval Calendar::findAvailable(DateInterval interval) const {
    for (;;) {
        auto start = std::lower_bound(_events.begin(), _events.end(),
            interval.dateStart(), [&](const Event_ptr &event, const Date &date) {
                return event->interval().dateEnd() < date;
        });
        auto end = std::lower_bound(
            _events.begin(), _events.end(),
            interval.dateEnd(), [&](const Event_ptr &event, const Date &date) {
                return event->interval().dateStart() > date;
        });
        if ((start != _events.end() && interval.intersects((*start)->interval()))
            || (end != _events.end() && interval.intersects((*end)->interval()))) {
            interval += Date::TimeUnit::Day;
        } else {
            break;
        }
    }
    return interval;
}

bool Calendar::isAvailable(RecurringEvent_ptr event) const {
    auto interval = maxDateInterval();
    
    interval = {
        event->interval().dateStart(),
        DateFormatter::dateFromFormat(2500, 1, 1),
        Date::TimeUnit::Day
    };
    
    auto instances = event->instantiate();
    for (auto instance : instances) {
        for (auto event_ : _events) {
            if (instance->interval().intersects(event_->interval())) {
                return false;
            }
        }
    }
    return true;
}

bool Calendar::isAvailable(Event_ptr event) const {
    // If event is recurring, use recurring method.
    if (event->isRecurring()) {
        return isAvailable(std::dynamic_pointer_cast<RecurringEvent>(event));
    }
    // Return availability for an interval.
    return isAvailable(event->interval());
}

bool Calendar::isAvailable(const DateInterval &interval) const {
    if (!_events.size()) {
        return true;
    }
    auto it = std::upper_bound(_events.begin(), _events.end(),
                               interval, std::less<Event_ptr>());
    // Is there a large enough spot?
    if (it == _events.begin()) {
        return !interval.intersects((*it)->interval());
    }
    
    if ((it != _events.begin() && interval.intersects((*--it)->interval()))
         ||
        (++it != _events.end() && interval.intersects((*it)->interval()))) {
        return false;
    }
    // No intersection found, event is available to be placed.
    return true;
}

void Calendar::addEvent(Event_ptr event) {
    // Check if there is available space
    if (!isAvailable(event)) {
        throw EventCreationException("No available spot for this event.");
    }
    // Update data sources.
    if (event->isRecurring()) {
        auto event_ = std::dynamic_pointer_cast<RecurringEvent>(event);
        auto instances = event_->instantiate();
        _events.insert(instances.begin(), instances.end());
        _recurringEvents.insert(event);
    } else {
        auto interval = ceilInterval(event->interval());
        _events.insert(event);
        _loader->updateEventsForUID(_uid, interval, {event});
    }
}

void Calendar::removeEvent(Event_ptr event) {
    if (event->isRecurring()) {
        throw EventCreationException("No support for recurring events yet.");
    } else {
        removeSingleEvent(event);
    }
}

DateInterval Calendar::maxDateInterval() noexcept {
    return {
        0,
        DateFormatter::dateFromFormat(2500, 1, 1)
    };
}
