//===- EventView.hpp - Display Event ----------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventView.hpp
//===----------------------------------------------------------------------===//

#ifndef EventView_hpp
#define EventView_hpp

#include "UI/UI.hpp"
#include "core/Date.hpp"
#include "Event.hpp"
#include "RecurringEvent.hpp"
#include <ncurses.h>
#include <vector>
#include <memory>

namespace planner {
    
    class EventView: public UI::View {
    protected:
        Event_ptr _event;
        
    public:
        EventView(WINDOW *win, Event_ptr event);
        
        const Event_ptr event() const noexcept;
        Event_ptr event() noexcept;
        void setEvent(Event_ptr event) noexcept;
        
        virtual void draw() override;
        
    }; /* class EventView */
    
    typedef std::shared_ptr<EventView> EventView_ptr;
    typedef std::unique_ptr<EventView> EventView_uptr;
    typedef std::weak_ptr<EventView> EventView_wptr;
    
    
} /* end namespace planner */

#endif /* EventView_hpp */
