//===- EvenListViewController.hpp - List Events -----------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventListViewController.hpp
//===----------------------------------------------------------------------===//

#ifndef EventListViewController_hpp
#define EventListViewController_hpp

#include "UI/UI.hpp"
#include "EventView.hpp"
#include "Event.hpp"
#include <ncurses.h>
#include <vector>
#include <memory>

namespace planner {
    
    class EventListViewControllerDelegate;
    
    class EventListViewController: public UI::ViewController {
    protected:
        size_t                 _eventIdx;
        std::vector<Event_ptr> _events;
        EventView_ptr          _eventView;
        UI::Label_ptr          _label;
        EventListViewControllerDelegate *_delegate;
        
        void updateLabel();
        void nextEvent();
        void prevEvent();
        
    public:
        EventListViewController(WINDOW *win);
        EventListViewController(WINDOW *win, std::vector<Event_ptr> events);
        
        virtual void init() override;
        virtual void deinit() override;
        
        virtual void viewDidLoad() override;
        virtual void viewDidAppear() override;
        
        virtual void setEvents(std::vector<Event_ptr> events);
        
        EventListViewControllerDelegate *delegate() noexcept;
        void setDelegate(EventListViewControllerDelegate *delegate) noexcept;
        
        
        // MARK: Event handling
        virtual void keyPressedEscapeKey() override;
        virtual void keyPressedArrowKeyRight() override;
        virtual void keyPressedArrowKeyLeft() override;
        
    }; /* class EventListViewController */
    
    typedef std::shared_ptr<EventListViewController> EventListViewController_ptr;
    typedef std::unique_ptr<EventListViewController> EventListViewController_uptr;
    typedef std::weak_ptr<EventListViewController> EventListViewController_wptr;
    
    class EventListViewControllerDelegate {
    public:
        virtual void eventListViewControllerShouldDismiss(EventListViewController &) = 0;
        
    }; /* class EventListViewControllerDelegate */
    
} /* end namespace planner */

#endif /* EventListViewController_hpp */
