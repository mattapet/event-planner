//===- EvenListViewController.cpp -----------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventListViewController.cpp
//===----------------------------------------------------------------------===//

#include "EventListViewController.hpp"


using namespace planner;
    
// MARK: Constrction

EventListViewController::EventListViewController(WINDOW *win)
: UI::ViewController(win), _eventIdx(0)
{}

EventListViewController::
EventListViewController(WINDOW *win,std::vector<Event_ptr> events)
: UI::ViewController(win), _eventIdx(0), _events(events)
{}

// MARK: Protected implementation

void EventListViewController::updateLabel() {
    _label->setText(std::to_string(_eventIdx + 1) + "/" +
                    std::to_string(_events.size()));
}

void EventListViewController::nextEvent() {
    if (_events.size() && _eventIdx < _events.size() - 1) {
        _eventView->setEvent(_events[++_eventIdx]);
        updateLabel();
    }
}

void EventListViewController::prevEvent() {
    if (_eventIdx) {
        _eventView->setEvent(_events[--_eventIdx]);
        updateLabel();
    }
}

// MARK: Public implemnetation

void EventListViewController::init() {
    UI::ViewController::init();
    _label = std::make_shared<UI::Label>(_win);
    _eventView = std::make_shared<EventView>(_win, nullptr);
    _label->initWithBounds({
        _bounds.origin.x,
        _bounds.origin.y + _bounds.size.height - 1,
        _bounds.size.width,
        1
    });
    _eventView->initWithBounds({
        _bounds.origin.x + 2,
        _bounds.origin.y,
        _bounds.size.width - 2,
        _bounds.size.height - 1
    });
}

void EventListViewController::deinit() {
    _label->deinit();
    _eventView->deinit();
    UI::ViewController::deinit();
}

void EventListViewController::viewDidLoad() {
    UI::ViewController::viewDidLoad();
    _view->addSubview(_label);
    _view->addSubview(_eventView);
    if (_events.size()) {
        _eventView->setEvent(_events.front());
        updateLabel();
    } else {
        _label->setText("0/0");
    }
}

void EventListViewController::viewDidAppear() {
    UI::ViewController::viewDidAppear();
    reload();
}

void EventListViewController::setEvents(std::vector<Event_ptr> events) {
    _events   = events;
    _eventIdx = 0;
}

EventListViewControllerDelegate *
EventListViewController::delegate() noexcept {
    return _delegate;
}

void EventListViewController::
setDelegate(EventListViewControllerDelegate *delegate) noexcept
{
    _delegate = delegate;
}

// MARK: Handling events.

void EventListViewController::keyPressedEscapeKey() {
    if (_delegate) {
        _delegate->eventListViewControllerShouldDismiss(*this);
    }
}

void EventListViewController::keyPressedArrowKeyRight() {
    nextEvent();
}

void EventListViewController::keyPressedArrowKeyLeft() {
    prevEvent();
}
