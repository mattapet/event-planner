//===- RecurringEvent.cpp -------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file RecurringEvent.hpp
//===----------------------------------------------------------------------===//

#include "RecurringEvent.hpp"

#include "core/DateFormatter.hpp"
#include <algorithm>

using namespace planner;
    
RecurringEvent::
RecurringInstanceEvent::RecurringInstanceEvent(const RecurringEvent &parent,
                                               const DateInterval &interval)
: Event(parent._uid, parent._summary, interval,
        parent._privacy, parent._dateStamp),
  _parent(parent)
{}

bool RecurringEvent::RecurringInstanceEvent::isRecurring() const {
    return true;
}


// MARK: Constructors

RecurringEvent::RecurringEvent(Recurrence recurrence,
                               const std::string &uid,
                               const std::string &summary,
                               const DateInterval &interval,
                               PrivacyOption privacy,
                               Date dateStamp)
: Event(uid, summary, interval, privacy, dateStamp),
  _recurrence(recurrence)
{}

RecurringEvent::RecurringEvent(json jsonObject)
: Event(jsonObject), _recurrence(jsonObject["recurrence"]) {
    try {
        _uid = jsonObject["uid"];
        _summary = jsonObject["summary"];
        _interval = {
            DateFormatter::fromISOString(jsonObject["date_start"]),
            DateFormatter::fromISOString(jsonObject["date_end"])
        };
        _dateStamp = DateFormatter::fromISOString(jsonObject["datestamp"]);
    } catch (const nlohmann::detail::out_of_range &err) {
        throw InvalidEventParameterException("A parameter of a json object is invalid");
    }
}

// MARK: Public Implementation

DateInterval RecurringEvent::interval() const {
    return intervalForDate(Date::now());
}

/// Accessor of the event's interval property.
DateInterval RecurringEvent::intervalForDate(Date date) const {
    if (date < _interval.dateStart()) {
        return _interval;
    }
    auto interval = _interval;
    for (size_t count = 0;; count++) {
        if (_recurrence.endDate < interval.dateStart()) {
            return interval.add((int)-_recurrence.frequency, _recurrence.unit);
        } else if (interval.dateStart() >= date) {
            return interval;
        } else  if (_recurrence.count <= count) {
            return interval;
        }
        
        interval = interval.add((int)_recurrence.frequency, _recurrence.unit);
    }
}

bool RecurringEvent::occursOnDate(Date date) const {
    auto currdate  = date.startOf(Date::TimeUnit::Day);
    auto startDate = _interval.dateStart().startOf(Date::TimeUnit::Day);
    
    // unwrapping optional
    if (auto dateEnd = _recurrence.endDate) {
        if (dateEnd.value() < currdate) {
            return false;
        }
    }
    if ((currdate - startDate) / _recurrence.unit % _recurrence.frequency) {
        return false;
    } else if (!_recurrence.onDays.size()) {
        return true;
    }
    
    switch (_recurrence.unit) {
        case Date::TimeUnit::Day:
            return true;
        case Date::TimeUnit::Week:
            return _recurrence.onDays.find(DateFormatter::weekDay(currdate))
                != _recurrence.onDays.end();
        case Date::TimeUnit::Month:
        case Date::TimeUnit::Year:
            return _recurrence.onDays.find(DateFormatter::day(currdate))
                != _recurrence.onDays.end();
        default:
            throw InvalidRecurrenceUnitException();
    }
}

bool RecurringEvent::isRecurring() const {
    return true;
}

std::set<RecurringEvent::RecurringInstanceEvent_ptr>
RecurringEvent::instantiate() const {
    auto instances = std::set<RecurringInstanceEvent_ptr>();
    auto interval = _interval;
    auto maxDate  = _recurrence.endDate.hasValue() ?
          (Date)_recurrence.endDate.value()
        : (Date)DateFormatter::dateFromFormat(2500, 1, 1);
    size_t count = 0;
    
    for (; interval.dateStart() < maxDate
         && (!_recurrence.count.hasValue() || count < _recurrence.count);)
    {
        instances.insert(
            std::make_shared<RecurringInstanceEvent>(*this, interval)
        );
        interval = interval.add((int)_recurrence.frequency, _recurrence.unit);
        count += 1;
    }

    return instances;
}

std::string RecurringEvent::toICalString() const {
    std::stringstream ss;
    ss << "BEGIN:VEVENT"
    << "\nUID:"     << _uid
    << "\nDTSTAMP:" << _formatter.format(_dateStamp)
    << "\nDTSTART:" << _formatter.format(_interval.dateStart())
    << "\nDTEND:"   << _formatter.format(_interval.dateEnd())
    << "\nRRULE:"   << _recurrence.toICalString()
    << "\nSUMMARY:" << _summary
    << "\nEND:VEVENT\n";
    return ss.str();
}

json RecurringEvent::toJSON() const {
    return {
        {"uid", _uid},
        {"datestamp", _formatter.format(_dateStamp)},
        {"date_start", _formatter.format(_interval.dateStart())},
        {"date_end", _formatter.format(_interval.dateEnd())},
        {"recurrence", _recurrence.toJSON()},
        {"summary", _summary}
    };
}
