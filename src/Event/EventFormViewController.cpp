//===- EvenFormViewController.cpp - List Events ---------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventFormViewController.cpp
//===----------------------------------------------------------------------===//

#include "EventFormViewController.hpp"
#include "core/Recurrence.hpp"
#include <algorithm>

using namespace planner;
    
// MARK: Construction

EventFormViewController::EventFormViewController(WINDOW *win)
: UI::ViewController(win)
{}

EventFormViewController::EventFormViewController(WINDOW *win, Date date)
: UI::ViewController(win), _defaultDate(date)
{}

// MARK: Protected implementaion

void EventFormViewController::setupLabels() {
    _labels = {
        std::make_shared<UI::Label>(_win, "Date from: "),
        std::make_shared<UI::Label>(_win, "Date to: "),
        std::make_shared<UI::Label>(_win, "Sumamry: "),
        std::make_shared<UI::Label>(_win, "Privacy: "),
        std::make_shared<UI::Label>(_win, "Recurrence frequency: "),
        std::make_shared<UI::Label>(_win, "Recurrence end date: "),
        std::make_shared<UI::Label>(_win, "Recurrence unit: "),
        std::make_shared<UI::Label>(_win, ""),
        std::make_shared<UI::Label>(_win, "")
    };
}

void EventFormViewController::setupForms() {
    _fields = {
        _dateFromField,
        _dateToField,
        _summaryField,
        _privacyField,
        _recurrenceFrequencyField,
        _recurrenceEndDateField,
        _recurrenceUnitSelect,
        _confirmButton,
        _cancelButton
    };
    _dateFromField->setDelegate(this);
    _dateToField->setDelegate(this);
    _summaryField->setDelegate(this);
    _privacyField->setDelegate(this);
    _recurrenceFrequencyField->setDelegate(this);
    _recurrenceEndDateField->setDelegate(this);
    _recurrenceUnitSelect->setDelegate(this);
    _confirmButton->setDelegate(this);
    _cancelButton->setDelegate(this);
}

json EventFormViewController::singleEventData() const {
    Date from = DateFormatter::dateFromString(_dateFromField->text(), "%Y/%m/%d %H:%M");
    Date to = DateFormatter::dateFromString(_dateToField->text(), "%Y/%m/%d %H:%M");
    auto summary = _summaryField->text();
    auto privacy = _privacyField->option();
    return {
        {"date_start", from.toJSON()},
        {"date_end", to.toJSON()},
        {"privacy", privacy},
        {"summary", summary}
    };
}

json EventFormViewController::recurrentEventData() const {
    json data_ = singleEventData();
    auto frequency = std::stoi(_recurrenceFrequencyField->text());
    auto unit = _recurrenceUnitSelect->option();
    std::transform(unit.begin(), unit.end(), unit.begin(), ::toupper);
    data_["recurrence"] = {
        {"frequency", frequency},
        {"unit", unit},
    };
    if (_recurrenceEndDateField->text().size()) {
        Date endDate = DateFormatter::dateFromString(_recurrenceEndDateField->text(), "%Y/%m/%d %H:%M");
        data_["recurrence"]["end_date"] = endDate.toJSON();
    }
    return data_;
}


// MARK: Public implementation

void EventFormViewController::init() {
    UI::ViewController::init();
    _dateFromField = std::make_shared<UI::TextField>(_win, true);
    _dateToField = std::make_shared<UI::TextField>(_win, true);
    _summaryField = std::make_shared<UI::TextField>(_win, true);
    _privacyField = std::make_shared<UI::Select>(_win, std::vector<std::string>{
        "Private", "Shared", "Public"
    });
    _recurrenceFrequencyField = std::make_shared<UI::TextField>(_win, true);
    _recurrenceEndDateField = std::make_shared<UI::TextField>(_win, true);
    _recurrenceUnitSelect = std::make_shared<UI::Select>(_win, std::vector<std::string>{
        "None", "Day", "Week", "Month", "Year"
    });
    _confirmButton = std::make_shared<UI::Button>(_win, "Confirm");
    _cancelButton = std::make_shared<UI::Button>(_win, "Cancel");
    
    setupLabels();
    setupForms();
    
    for (size_t i = 0; i < _fields.size(); i++) {
        _labels[i]->initWithBounds({
            _bounds.origin.x + 1,
            _bounds.origin.y + 1 + (int)i,
            _bounds.size.width - 2,
            1
        });
        _fields[i]->initWithBounds({
            _bounds.origin.x + 2 + (int)_labels[i]->text().size(),
            _bounds.origin.y + 1 + (int)i,
            _bounds.size.width - 2 - (int)_labels[i]->text().size() - 1,
            1
        });
    }
}

void EventFormViewController::deinit() {
    for (size_t i = 0; i < _fields.size(); i++) {
        _labels[i]->deinit();
        _fields[i]->deinit();
    }
    UI::ViewController::deinit();
}

void EventFormViewController::viewDidLoad() {
    UI::ViewController::viewDidLoad();
    _view->setFramed(true);
    for (size_t i = 0; i < _fields.size(); i++) {
        _view->addSubview(_labels[i]);
        _view->addSubview(_fields[i]);
    }
    if (_defaultDate) {
        _dateFromField->setText(
            DateFormatter::stringFromDate((*_defaultDate)
            .add(8, Date::TimeUnit::Hour), "%Y/%m/%d %H:%M")
        );
        _dateToField->setText(
            DateFormatter::stringFromDate((*_defaultDate)
            .add(9, Date::TimeUnit::Hour), "%Y/%m/%d %H:%M")
        );
    }
    _fields[0]->becomeFirstResponder();
}

void EventFormViewController::viewDidAppear() {
    UI::ViewController::viewDidAppear();
    reload();
}


EventFormViewControllerDelegate *EventFormViewController::delegate() {
    return _delegate;
}

void EventFormViewController::
setDelegate(EventFormViewControllerDelegate *delegate)
{
    _delegate = delegate;
}

json EventFormViewController::data() const {
    if (_recurrenceUnitSelect->option() != "None"
        && _recurrenceFrequencyField->text().size()) {
        return recurrentEventData();
    } else {
        return singleEventData();
    }
}

void EventFormViewController::
nextResponderShouldBecomeFirstResponder(UI::Responder &responder)
{
    for (size_t i = 0; i < _fields.size(); i++) {
        if ((UI::Responder *)(_fields[i].get()) == &responder) {
            _fields[(i + 1) % _fields.size()]->becomeFirstResponder();
            if (i >= _fields.size() - 2) {
                dynamic_cast<UI::Button &>(responder).reload();
            }
            _fields[(i + 1) % _fields.size()]->reload();
            break;
        }
    }
}

void EventFormViewController::
prevResponderShouldBecomeFirstResponder(UI::Responder &responder)
{
    for (size_t i = 0; i < _fields.size(); i++) {
        if ((UI::Responder *)(_fields[i].get()) == &responder) {
            i = i ? i - 1 : _fields.size() - 1;
            _fields[i]->becomeFirstResponder();
            if (i >= _fields.size() - 3 && i < _fields.size() - 1) {
                dynamic_cast<UI::Button &>(responder).reload();
            }
            _fields[i]->reload();
            break;
        }
    }
}

void EventFormViewController::buttonWasSelected(UI::Button &button) {
    try {
        if (_confirmButton.get() == &button) {
            if (_delegate) {
                _delegate->eventFormViewControllerShouldCreate(*this);
            }
        } else {
            if (_delegate) {
                _delegate->eventFormViewControllerShouldCancel(*this);
            }
        }
    } catch (...) {}
}
