//===- EvenFormViewController.hpp - Event Form -----------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventFormViewController.hpp
//===----------------------------------------------------------------------===//

#ifndef EventFormViewController_hpp
#define EventFormViewController_hpp

#include "UI/UI.hpp"
#include "core/Date.hpp"
#include "core/Optional.hpp"
#include <ncurses.h>
#include <string>
#include <vector>
#include <memory>

namespace planner {
    
    // For the json convenience
    using json = nlohmann::json;
    
    class EventFormViewControllerDelegate;
    
    class EventFormViewController
    : public UI::ViewController,
             UI::TextFieldDelegate,
             UI::SelectDelegate,
             UI::ButtonDelegate
    {
    protected:
        Optional<Date>             _defaultDate;
        std::vector<UI::Label_ptr> _labels;
        std::vector<UI::View_ptr>  _fields;
        
        UI::TextField_ptr _dateFromField;
        UI::TextField_ptr _dateToField;
        UI::TextField_ptr _summaryField;
        UI::Select_ptr    _privacyField;
        UI::TextField_ptr _recurrenceFrequencyField;
        UI::TextField_ptr _recurrenceEndDateField;
        UI::Select_ptr    _recurrenceUnitSelect;
        
        UI::Button_ptr    _confirmButton;
        UI::Button_ptr    _cancelButton;
        
        EventFormViewControllerDelegate *_delegate;
        
        void setupLabels();
        void setupForms();
        
        json singleEventData() const;
        json recurrentEventData() const;
        
    public:
        EventFormViewController(WINDOW *win);
        EventFormViewController(WINDOW *win, Date);
        
        virtual void init() override;
        virtual void deinit() override;

        virtual void viewDidLoad() override;
        virtual void viewDidAppear() override;

        EventFormViewControllerDelegate *delegate();
        void setDelegate(EventFormViewControllerDelegate *delegate);
        
        virtual json data() const;
        
        virtual void nextResponderShouldBecomeFirstResponder(UI::Responder &responder) override;
        virtual void prevResponderShouldBecomeFirstResponder(UI::Responder &responder) override;
        
        virtual void buttonWasSelected(UI::Button &button) override;
        
    }; /* class EventFormViewController */
    
    class EventFormViewControllerDelegate {
    public:
        virtual void eventFormViewControllerShouldCreate(EventFormViewController &) = 0;
        virtual void eventFormViewControllerShouldCancel(EventFormViewController &) = 0;
        
    }; /* class EventFormViewControllerDelegate */
    
    typedef std::shared_ptr<EventFormViewController> EventFormViewController_ptr;
    typedef std::unique_ptr<EventFormViewController> EventFormViewController_uptr;
    typedef std::weak_ptr<EventFormViewController> EventFormViewController_wptr;
    
} /* end namespace planner */

#endif /* EventFormViewController_hpp */
