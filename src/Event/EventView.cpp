//===- EventView.cpp ------------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventView.cpp
//===----------------------------------------------------------------------===//

#include "EventView.hpp"

using namespace planner;
    
EventView::EventView(WINDOW *win, Event_ptr event)
: UI::View(win), _event(event)
{
    _framed = true;
}

const Event_ptr EventView::event() const noexcept {
    return _event;
}

Event_ptr EventView::event() noexcept {
    return _event;
}

void EventView::setEvent(Event_ptr event) noexcept {
    _event = event;
    reload();
}

void EventView::draw() {
    if (!_event) {
        mvwprintw(_win, _bounds.origin.y + 1, _bounds.origin.x,
                 "No events on given date.");
        return;
    }
    mvwprintw(
    _win, _bounds.origin.y + 1, _bounds.origin.x,
    "From: %s", DateFormatter::stringFromDate(_event->interval().dateStart(),
                                              "%Y/%m/%d %H:%M").c_str());
    mvwprintw(
    _win, _bounds.origin.y + 2, _bounds.origin.x,
    "To: %s", DateFormatter::stringFromDate(_event->interval().dateEnd(),
                                            "%Y/%m/%d %H:%M").c_str());
    mvwprintw(
    _win, _bounds.origin.y + 3, _bounds.origin.x,
    "UID: %s", _event->uid().c_str());
    mvwprintw(
      _win, _bounds.origin.y + 4, _bounds.origin.x,
      "Type: %s", _event->privacyAsString().c_str());
    mvwprintw(
    _win, _bounds.origin.y + 5, _bounds.origin.x,
    "Summary: %s", _event->summary().c_str());
}
