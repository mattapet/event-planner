//===- Event.cpp - Calendar Event models ----------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Event.cpp
///
/// \brief This file contains implementation of a Event class, and \c std::hash
///  and \c std::less function specialization for Event class instances.
//===----------------------------------------------------------------------===//

#include "Event.hpp"

#include <iostream>
#include <sstream>
#include <exception>

using namespace planner;
    
InvalidEventParameterException::
InvalidEventParameterException(const char *desc)
: std::runtime_error(desc) {}

// MARK: Constructors

Event::Event(const std::string &uid,
             const std::string &summary,
             const DateInterval &interval,
             PrivacyOption       privacy,
             Date dateStamp)
: _formatter({kISO_FORMAT_STRING}),
  _dateStamp(dateStamp),
  _interval(interval),
  _uid(uid),
  _summary(summary),
  _privacy(privacy)
{}

Event::Event(std::string &&uid,
      std::string &&summary,
      DateInterval &&interval,
      PrivacyOption &&privacy,
      Date &&dateStamp)
: _formatter({kISO_FORMAT_STRING}),
  _dateStamp(std::move(dateStamp)),
  _interval(std::move(interval)),
  _uid(std::move(uid)),
  _summary(std::move(summary)),
  _privacy(std::move(privacy))
{}

Event::Event(json jsonObject): _interval({0, 0}) {
    try {
        _uid = jsonObject["uid"];
        _summary = jsonObject["summary"];
        _interval = {
            DateFormatter::fromISOString(jsonObject["date_start"]),
            DateFormatter::fromISOString(jsonObject["date_end"])
        };
        _dateStamp = DateFormatter::fromISOString(jsonObject["datestamp"]);
    } catch (const nlohmann::detail::out_of_range &err) {
        throw InvalidEventParameterException("A parameter of a json object is invalid");
    }
    
    try {
        std::string privacy = jsonObject.at("privacy");
        if (privacy == "Private") {
            _privacy = PrivacyOption::Private;
        } else if (privacy == "Shared") {
            _privacy = PrivacyOption::Shared;
        } else if (privacy == "Public") {
            _privacy = PrivacyOption::Public;
        } else {
            throw std::invalid_argument("Invalid privacy option.");
        }
    } catch (const nlohmann::detail::out_of_range &err) {
        _privacy = PrivacyOption::Private;
    }
}

// MARK: Accessor methods

PrivacyOption Event::privacy() const {
    return _privacy;
}

std::string Event::privacyAsString() const {
    switch (_privacy) {
        case PrivacyOption::Private: return "Private";
        case PrivacyOption::Shared: return "Shared";
        case PrivacyOption::Public: return "Public";
    }
}

std::string Event::uid() const {
    return _uid;
}


std::string Event::summary() const {
    return _summary;
}

DateInterval Event::interval() const {
    return _interval;
}

DateInterval Event::intervalForDate(Date date) const {
    return _interval;
}

bool Event::occursOnDate(Date date) const {
    return date == _interval.dateStart();
}

std::size_t Event::hash() const {
    return std::hash<DateInterval>()(_interval)
    ^ std::hash<Date>()(_dateStamp)
    ^ std::hash<std::string>()(_uid)
    ^ std::hash<std::string>()(_summary);
}

std::string Event::toICalString() const {
    std::stringstream ss;
    ss << "BEGIN:VEVENT"
    << "\nUID:"     << _uid
    << "\nCLASS:"   << privacyAsString()
    << "\nDTSTAMP:" << _formatter.format(_dateStamp)
    << "\nDTSTART:" << _formatter.format(_interval.dateStart())
    << "\nDTEND:"   << _formatter.format(_interval.dateEnd())
    << "\nSUMMARY:" << _summary
    << "\nEND:VEVENT\n";
    return ss.str();
}

json Event::toJSON() const {
    return {
        {"uid", _uid},
        {"summary", _summary},
        {"privacy", privacyAsString()},
        {"datestamp", _formatter.format(_dateStamp)},
        {"date_start", _formatter.format(_interval.dateStart())},
        {"date_end", _formatter.format(_interval.dateEnd())}
    };
}

bool Event::isRecurring() const {
    return false;
}

// MARK: Comparison methods

bool Event::isEqual(const Event &other) const {
    if (this == &other) {
        return true;
    } else {
        return isRecurring() == other.isRecurring()
        && _uid     == other._uid
        && _summary == other._summary;
    }
}

// MARK: Operator overloading

bool Event::operator<(const Event &other) const {
    return _interval < other._interval;
}
bool Event::operator<=(const Event &other) const {
    return _interval <= other._interval;
}
bool Event::operator>(const Event &other) const {
    return _interval > other._interval;
}
bool Event::operator>=(const Event &other) const {
    return _interval >= other._interval;
}
bool Event::operator<(const DateInterval &other) const {
    return _interval < other;
}
bool Event::operator<=(const DateInterval &other) const {
    return _interval <= other;
}
bool Event::operator>(const DateInterval &other) const {
    return _interval > other;
}
bool Event::operator>=(const DateInterval &other) const {
    return _interval >= other;
}

bool Event::operator==(const Event &other) const {
    return isEqual(other);
}
bool Event::operator!=(const Event &other) const {
    return !isEqual(other);
}

namespace std {
    size_t hash<Event>::
    operator()(const Event &event) {
        return event.hash();
    }

    bool less<Event_ptr>::
    operator()(const Event_ptr &lhs,
               const Event_ptr &rhs) const {
        return *lhs < *rhs;
    }

    bool less<Event_ptr>::
    operator()(const Event_ptr &lhs,
               const DateInterval &rhs) const {
        return *lhs < rhs;
    }

    bool less<Event_ptr>::
    operator()(const DateInterval &lhs,
               const Event_ptr &rhs) const {
        return lhs < rhs->interval();
    }

    bool less<DateInterval>::
    operator()(const Event_ptr &lhs,
               const DateInterval &rhs) const {
        return *lhs < rhs;
    }

    bool less<DateInterval>::
    operator()(const DateInterval &lhs,
               const Event_ptr &rhs) const {
        return lhs < rhs->interval();
    }
}


namespace planner {
    std::ostream &operator<<(std::ostream &os, const Event &event) {
        return os << event.toICalString();
    }
    void to_json(json &j, const Event &event) noexcept {
        j = event.toJSON();
    }

    void from_json(const json &j, Event &event) {
        event = Event(j);
    }
}

