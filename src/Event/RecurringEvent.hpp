//===- RecurringEvent.hpp - Recurring Calendar Event ------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file RecurringEvent.hpp
///
/// \brief This file contains declaration of RecurringEvent class.
//===----------------------------------------------------------------------===//

#ifndef RecurringEvent_hpp
#define RecurringEvent_hpp

#include "Event.hpp"
#include "core/Date.hpp"
#include "core/Recurrence.hpp"
#include "core/Optional.hpp"
#include "core/Recurrence.hpp"

#include <set>
#include <string>
#include <memory>

namespace planner {
    
    class RecurringEvent: public Event {
    protected:
        Recurrence _recurrence;
        
    public:
        /// Instantiated RecurringEvent as a specific event.
        class RecurringInstanceEvent: public Event {
        protected:
            /// Parent of the instantiated Event
            const RecurringEvent &_parent;
            
        public:
            /// Default constructor.
            RecurringInstanceEvent(const RecurringEvent &parent,
                                   const DateInterval   &interval);
            
            /// \brief Returns a bool value identifying recurring events.
            /// \return \c true, if an event is recurring, \c false otherwise.
            virtual bool isRecurring() const override;
            
        }; /* class RecurringInstanceEvent */
        
        typedef std::shared_ptr<RecurringInstanceEvent> RecurringInstanceEvent_ptr;
        typedef std::weak_ptr<RecurringInstanceEvent> RecurringInstanceEvent_wptr;
        typedef std::unique_ptr<RecurringInstanceEvent> RecurringInstanceEvent_uptr;
        
        
        /// Default constructor.
        RecurringEvent(Recurrence recurrence,
                       const std::string &uid,
                       const std::string &summary,
                       const DateInterval &interval,
                       PrivacyOption       privacy = PrivacyOption::Private,
                       Date dateStamp = Date::now());
        
        /// Convenience constructor from a JSON object.
        RecurringEvent(json jsonObject);
        
        /// Accessor of the event's interval property.
        virtual DateInterval interval() const override;
        
        /// Accessor of the event's interval property.
        virtual DateInterval intervalForDate(Date date) const override;
        
        /// Returns a boolean value identifying whether the recurring event
        /// does or does not occurs on the given date.
        /// \return \c true if events occurs on given date, \c false otherwise.
        virtual bool occursOnDate(Date date) const override;
        
        /// \brief Returns a bool value identifying recurring events.
        /// \return \c true, if an event is recurring, \c false otherwise.
        virtual bool isRecurring() const override;
        
        virtual std::set<RecurringInstanceEvent_ptr> instantiate() const;
        
        /// \brief A method, that should parse an event to appropriate
        ///  iCal format representation.
        /// \return A string iCal format representation of an event.
        virtual std::string toICalString() const override;
        
        /// \brief A method, that parses an event ot appropriate JSON object
        ///  representation.
        /// \return A JSON object representation of an event.
        virtual json toJSON() const override;
        
    }; /* class RecurringEvent */
    
    typedef std::shared_ptr<RecurringEvent> RecurringEvent_ptr;
    typedef std::weak_ptr<RecurringEvent> RecurringEvent_wptr;
    typedef std::unique_ptr<RecurringEvent> RecurringEvent_uptr;
    
} /* end namespace planner */

#endif /* RecurringEvent_hpp */
