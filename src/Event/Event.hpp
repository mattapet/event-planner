//===- Event.hpp - Calendar Event model -------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Event.hpp
///
/// \brief This file contains declaration of a Event class, from which should
///  subclass all classes, that wish to be used within Calendar objects.
//===----------------------------------------------------------------------===//

#ifndef Event_hpp
#define Event_hpp

#include "core/protocols/ICalable.hpp"
#include "core/protocols/Hashable.hpp"
#include "core/protocols/JSONable.hpp"
#include "core/Date.hpp"
#include "core/Optional.hpp"
#include "core/json.hpp"

#include <ctime>
#include <iostream>
#include <sstream>
#include <string>
#include <memory>
#include <exception>

namespace planner {
    // For the json convenience
    using json = nlohmann::json;
    
    class InvalidEventParameterException: std::runtime_error {
    public:
        InvalidEventParameterException(const char *desc);
    };
    
    enum PrivacyOption {
        Private,
        Public,
        Shared
    };
    
    /// \brief An \b base class for all events.
    ///
    /// \description Event is an abstract base class for all event classes,
    ///  that wish to be used within Calendar objects and/or ICalFormatter
    ///  objects.
    class Event
    : public protocol::ICalable,
      public protocol::Hashable,
      public protocol::JSONable
    {
    protected:
        /// The formatter, that shall be used to format the Date to ISO format.
        DateFormatter _formatter;
        
        /// DateStamp of the event
        Date          _dateStamp;
        /// The interval of the event.
        DateInterval  _interval;
        /// The UID of an event.
        std::string   _uid;
        /// The description of an event.
        std::string   _summary;
        /// Privacy option of event.
        PrivacyOption _privacy;
        
    public:
        /// Designated constructor.
        Event(const std::string &uid,
              const std::string &summary,
              const DateInterval &interval,
              PrivacyOption option = PrivacyOption::Private,
              Date dateStamp = Date::now());
        
        /// Designated movable constructor.
        Event(std::string &&uid,
              std::string &&summary,
              DateInterval &&interval,
              PrivacyOption &&option,
              Date &&dateStamp);
        
        /// Convenience constructor from a JSON object.
        Event(json jsonObject);
        
        /// Defatul destructor.
        virtual ~Event() = default;
        
        // MARK: Accessor methods
        
        /// Returns privacy option.
        virtual PrivacyOption privacy() const;
        
        /// Returns privacy option as string (e.g. for exporting).
        virtual std::string privacyAsString() const;
        
        /// Returns UID of an event.
        virtual std::string uid() const;
        
        /// Returns summary of an event.
        virtual std::string summary() const;
        
        /// Accessor of the event's interval property.
        virtual DateInterval interval() const;
        
        /// Accessor of the event's interval property.
        virtual DateInterval intervalForDate(Date date) const;
        
        /// Returns a boolean value identifying whether the recurring event
        /// does or does not occurs on the given date.
        /// \return \c true if events occurs on given date, \c false otherwise.
        virtual bool occursOnDate(Date date) const;
        
        /// \brief Returns a bool value identifying recurring events.
        /// \return \c true, if an event is recurring, \c false otherwise.
        virtual bool isRecurring() const;
        
        /// \brief A method, that should parse an event to appropriate
        ///  iCal format representation.
        /// \return A string iCal format representation of an event.
        virtual std::string toICalString() const override;
        
        /// \brief A method, that parses an event ot appropriate JSON object
        ///  representation.
        /// \return A JSON object representation of an event.
        virtual json toJSON() const override;
        
        /// \brief An abstract method, that should hash an event.
        /// \return A hash calculated based on an event object.
        virtual std::size_t hash() const override;
        
        // MARK: Comparison methods
        
        /// \brief Compares an event with another one to determin the equality.
        /// \return \c true if events equal, \c false otherwise.
        virtual bool isEqual(const Event &other) const;
        
        // MARK: Operator overloading
        
        bool operator<(const Event &other) const;
        bool operator<=(const Event &other) const;
        bool operator>(const Event &other) const;
        bool operator>=(const Event &other) const;
        bool operator<(const DateInterval &other) const;
        bool operator<=(const DateInterval &other) const;
        bool operator>(const DateInterval &other) const;
        bool operator>=(const DateInterval &other) const;
        
        bool operator==(const Event &other) const;
        bool operator!=(const Event &other) const;

        friend std::ostream &operator<<(std::ostream &os, const Event &event);
    }; /* end class Event */
    
    typedef std::shared_ptr<Event> Event_ptr;
    typedef std::weak_ptr<Event> Event_wptr;
    typedef std::unique_ptr<Event> Event_uptr;
    
    void to_json(json &j, const Event &event) noexcept;
    
    void from_json(const json &j, Event &event);
    
} /* end namespace planner */


namespace std {
    template <>
    struct hash<planner::Event> {
        size_t operator()(const planner::Event &event);
    };
    
    /// std::map and std::set specialization of std::less function
    template<>
    struct less<planner::Event_ptr> {
        bool operator()(const planner::Event_ptr &lhs,
                        const planner::Event_ptr &rhs) const;
        
        bool operator()(const planner::Event_ptr &lhs,
                        const planner::DateInterval &rhs) const;
        
        bool operator()(const planner::DateInterval &lhs,
                        const planner::Event_ptr &rhs) const;
    };
    
    template<>
    struct less<planner::DateInterval> {
        bool operator()(const planner::Event_ptr &lhs,
                        const planner::DateInterval &rhs) const;
        
        bool operator()(const planner::DateInterval &lhs,
                        const planner::Event_ptr &rhs) const;
    };
}

#endif /* Event_hpp */
