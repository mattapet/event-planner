//===- Ref.hpp  - Reference counting ----------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file Ref.hpp
///
/// \brief This file contains declaration and definition of the RefCounted core
///  class, that is used as a base class for all objects that which to be
///  reference conted during their lifetimes.
///
/// \description This header file contains the \c RefCounted class and
///  \c RefStore structure, that implement reference counting on classes derived
///  from the \RefCounted class.
///
//===----------------------------------------------------------------------===//
#ifndef Ref_hpp
#define Ref_hpp

#include <cassert>
#include <utility>

namespace planner {
    
    /// \brief A struct, which stores and menages reference count
    ///  of an object. This \b final struct should never be derived from.
    ///  It is used by the \c RefCounted class instances, to store,
    ///  share and maintain accurate reference count.
    struct RefStore final {
    private:
        /// Reference count.
        mutable size_t ref_count;
        
    public:
        /// The contructor initializes the `RefStore` instance
        /// to zero reference count.
        RefStore(): ref_count(0) {}
        
        /// \brief Increments reference count of an object.
        /// \return The updated reference count.
        inline size_t retain()  const { return ++ref_count; }
        
        /// \brief Decrements reference count of an object.
        /// \return The updated reference count.
        inline size_t release() const { return --ref_count; }
        
        /// \brief Retrieves the current reference count of the object.
        /// \return The current reference count.
        inline size_t count()   const { return ref_count; }
    };
    
    
    /// \brief A generic abstract base class for objects, that wish to be
    ///  reference counted during their lifetimes.
    ///
    /// \description By subclassing, derived classes obtain ability to be
    ///  referenced counted. The class uses \c RefStore objects, to manage
    ///  proper reference count, among the instances. The reference count
    ///  is manges by calling \c retain and \c release methods on the objects.
    ///  The class increments the reference count upon each construction,
    ///  assignement and decrements upon each descruction. If you wish to
    ///  override any of the constructors, assignement operator and/or
    ///  destructor, you should call the superclass version of the method
    ///  before and/or after the reimplementation of the overriden method.
    ///
    ///  All derived classes are required to manage all allocations and
    ///  deallocations in the \c init and \c deinit methods, by overridin them.
    ///  When overriding \c init and \c deinit method, you always \b must call
    ///  \b superclass version of the overriden method.
    ///
    ///  \c RefCounted object is mainly used for constant and/or shared objects.
    ///  If you wish to add the copy-on-write behaviour to your derived class,
    ///  simply call the \c mutating method on top of implementation of each
    ///  of the methods, which can change the internal state of the object.
    ///  The \c mutating method resolves checks the number of reference of the
    ///  object and copies the data is needed, while keeping all reference
    ///  counts valid. The only other obligation of the derived class, is to
    ///  implement the optional abstract method \c copy, which should copy all
    ///  necessary data of the object.
    ///
    /// \see https://en.wikipedia.org/wiki/Reference_counting
    class RefCounted {
        /// A reference count manager.
        RefStore *store;
        
    public:
        // MARK: Constructors
        
        explicit RefCounted(): store(nullptr) {}
        
        RefCounted(const RefCounted &other): store(other.store) {
            if (this != &other) { retain(); }
        }
        
        RefCounted(RefCounted &&other): store(other.store) {
            if (this != &other) { other.store = nullptr; }
        }
        
        virtual ~RefCounted() {}
        
        // MARK: Lifecycle methods
        
        /// \brief Called after construction to properly initialize the object.
        ///
        /// \description The \c init method is called, when the new object
        ///  is allocated with reference count of zero. When the \c init method
        ///  is called, the object should initialize all properties and do
        ///  all necessary allocations.
        ///  When overriding the \c init method, the dervied class \b should
        ///  \b always call the superclass version of init \b before performing
        ///  any of it's initialization.
        virtual void init() { store = new RefStore(); }
        
        /// \brief Called upon destruction to property deinitialize the object.
        ///
        /// \description The \c deinit method is called, when the object's
        ///  reference count comes down to zero and it needs to be desctructed.
        ///  \c deinit method properly performes all necessary deallocations
        ///   and prepares the object, for the desctruction itself.
        ///   When overriding the \c deinit method, the derived class \b should
        ///   \b always call the superclass version if deinit \b after
        ///   performing all of it's deinitializations.
        virtual void deinit() { delete store; }
        
        /// \brief Called upon mutating method call, to properly copy
        ///   the resources manages by the object.
        ///
        /// \description The \c copy method is called, when a \c mutating
        ///  method is called on an object, that has more than one reference
        ///  count. \c copy is an optional abstract method, that needs to be
        ///  implemented, if the derived class whishes to imeplement
        ///  the copy-on-write behaviour.
        virtual void copy() {}
        
        
        // MARK: Assignement operator overload
        
        /// Assignement operator checks, the the current's object store
        /// is not the same, as the other's one and if they differ, releases
        /// the object with current store, copies and retains the new one.
        virtual RefCounted &operator=(const RefCounted &other) {
            if (store != other.store) {
                release();
                store = other.store;
                retain();
            }
            return *this;
        }
        
        // MARK: Compare operators
        
        /// \brief Compares the reference count stores.
        /// \return \c true, if the stores are equal, \c false otherwise.
        virtual bool operator==(const RefCounted &other) const {
            return this == &other || store == other.store;
        }
        
        /// \brief Compares the reference count stores.
        /// \return \c true, if the stores don't equal, \c false otherwise.
        virtual bool operator!=(const RefCounted &other) const {
            return this != &other || store != other.store;
        }
        
        // MARK: Reference count accessor
        
        /// \brief Retrieves current reference count to the object.
        /// \return The current reference count to the object.
        inline size_t count() const {
            return store->count();
        }
        
        // MARK: Reference count manipulators
        
        /// \brief Increments the reference count of the object.
        ///
        /// \description The \c retain is a \b final method, that is called
        ///  to increment the reference count to the object. If the \c retain
        ///  method is called on an object, that does not have any references
        ///  to it yet, the \c init method is called.
        inline void retain() {
            if (!store) {
                init();
            }
            store->retain();
        }
        
        /// \brief Decrements the reference count of the object.
        ///
        /// \description The \c release is a \b final method, taht is called
        ///  to decrement the reference count to the object. If the \c release
        ///  method is called on an object and reference count hits zero,
        ///  the \c deinit method is called.
        inline void release() {
            assert(store->count() != 0 && "Reference count is already zero.");
            if (!store->release()) {
                deinit();
            }
        }
        
        // MARK: Mutating method specifier
        
        /// \brief Specifies a mutating method in a copy-on-write object
        ///  and resolves the copying of the object.
        ///
        /// \description The \c mutating is a \b final method, that should
        ///  be called on the \b top of implementation of \b every \b mutating
        ///  \b method in copy-on-write objects. \c mutating method checks
        ///  the reference count of the object and performes the \c copy method
        ///  if needed.
        inline void mutating() {
            if (count() > 1) {
                copy();
                release();
                store = new RefStore();
                retain();
            }
        }
    };
    
} /* namespace planner */

#endif /* Ref_hpp */
