//===- Recurrence.hpp - Evetn recurrence ------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Recurrence.hpp
///
/// \brief This file contains declaration of the \c Recurrence class, and
///  associate exceptions used to encapsulating event recurrence.
//===----------------------------------------------------------------------===//

#ifndef Recurrence_hpp
#define Recurrence_hpp

#include "protocols/ICalable.hpp"
#include "protocols/JSONable.hpp"
#include "Optional.hpp"
#include "Date.hpp"
#include "json.hpp"
#include <string>
#include <set>
#include <exception>

namespace planner {
    // For the json convenience
    using json = nlohmann::json;
    
    /// Exception, that is raised, when an invalid recurrence unit is used
    /// to create a recurrence.
    class InvalidRecurrenceUnitException: public std::invalid_argument {
    public:
        InvalidRecurrenceUnitException();
    };
    
    /// Exception, that is raised, when the the a \c onDays parameter does not
    /// conform to the date unit that was selected.
    class InvalidNumberOfDaysException: public std::invalid_argument {
    public:
        InvalidNumberOfDaysException();
    };
    
    /// An immutable struct encapsualting a recurrence.
    struct Recurrence: public protocol::ICalable, protocol::JSONable {
        /// Unit of recurrence.
        /// \note Only day or longer unit of time.
        const Date::TimeUnit      unit;
        /// Frequency of recurrence.
        /// Should be a non-zero value determining how many time units to occur.
        const size_t              frequency;
        /// Specifies, on which days it should occur.
        /// If it's recurring daily, no value.
        std::set<size_t>          onDays;
        /// Specified the end date of the recurrence.
        Optional<Date>            endDate;
        /// Maximal number of recurrences.
        Optional<size_t>          count;
        
        /// Deafult recurrence constructor.
        /// \param unit_ Specifies unit of recurrence.
        /// \param frequency_ Specifies the frequency in which to occur.
        /// \param onDays_ The number of days, on which should an event occur.
        /// \throw InvalidRecurrenceUnit
        Recurrence(Date::TimeUnit unit_,
                   size_t frequency_,
                   std::set<size_t> onDays_ = {});
        
        /// Convenience recurrence constructor.
        /// \param unit_ Specifies unit of recurrence.
        /// \param frequency_ Specifies the frequency in which to occur.
        /// \param endDate_ The end date of the recurrence.
        /// \param onDays_ The number of days, on which should an event occur.
        /// \throw InvalidRecurrenceUnit
        Recurrence(Date::TimeUnit unit_,
                   size_t frequency_,
                   Date endDate_,
                   std::set<size_t> onDays_ = {});
        
        /// Convenience recurrence constructor.
        /// \param unit_ Specifies unit of recurrence.
        /// \param frequency_ Specifies the frequency in which to occur.
        /// \param count_ The maximum number of times, the recurrence should
        ///               occur.
        /// \param onDays_ The number of days, on which should an event occur.
        /// \throw InvalidRecurrenceUnit
        Recurrence(Date::TimeUnit unit_,
                   size_t frequency_,
                   size_t count_,
                   std::set<size_t> onDays_ = {});
        
        /// Convenience recurrence constructor creating recurrence from JSON.
        /// \param jsonObject A JSON object from which ot create Recurrence.
        /// \throw InvalidRecurrenceUnit
        Recurrence(json jsonObject);
        
        /// Designated destructor.
        virtual ~Recurrence() = default;
        
        virtual std::string toICalString() const override;
        virtual json toJSON() const override;
        
    }; /* struct Recurrence */
    
    /// JSON library compatability function.
    /// Converts \c Recurrence to appropriate JSON format.
    void to_json(json &j, const Recurrence &recurrence) noexcept;
    
    /// JSON library compatability function.
    /// Converts appropriate JSON format to \c Recurrence.
    Recurrence from_json(const json &j);
    
} /* end namespace planner */

#endif /* Recurrence_hpp */
