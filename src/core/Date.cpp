//===- Date.cpp - Date & Datetime -----------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file Date.cpp
///
/// \brief This file contains implementations of the EventPlanner
///  core Date and Datetime objects and structures.
///
//===----------------------------------------------------------------------===//

#include "Date.hpp"
#include <ctime>

using namespace planner;

// MAKR: struct Date

// MARK: Creation and initialization

Date::Date() noexcept
: _seconds(std::time(nullptr))
{}

Date::Date(time_t seconds) noexcept
: _seconds(seconds)
{}

Date::Date(int year, int month, int day) throw()
: _seconds(DateFormatter::dateFromFormat(year, month, day))
{}

// MARK: Private imeplementation

Date Date::addMonths(int amound) const noexcept {
    auto t = std::gmtime(&_seconds);
    t->tm_mon   = (t->tm_mon + amound) % 12;
    t->tm_year += (t->tm_mon + amound) / 12;
    if (t->tm_mday == 29
        && t->tm_mon == 1
        && !DateFormatter::isLeapYear(t->tm_year))
    {
        t->tm_mon  = 2;
        t->tm_mday = 1;
    }
    return timegm(t);
}

Date Date::addYears(int amound) const noexcept {
    auto t = std::gmtime(&_seconds);
    t->tm_year += amound;
    if (t->tm_mday == 29
        && t->tm_mon == 1
        && !DateFormatter::isLeapYear(t->tm_year))
    {
        t->tm_mon  = 2;
        t->tm_mday = 1;
    }
    return timegm(t);
}

// MARK: Public imeplementation

json Date::toJSON() const {
    return DateFormatter::toISOString(_seconds);
}

void Date::assign(const json &j) {
    _seconds = DateFormatter::fromISOString(j);
}

std::size_t Date::hash() const noexcept {
    return std::hash<time_t>()(*this);
}

Date Date::add(int amound, TimeUnit value) const noexcept {
    switch (value) {
        case Second: case Minute: case Hour: case Day: case Week:
            return Date(amound * value + _seconds);
        case Month:
            return addMonths(amound);
        case Year:
            return addYears(amound);
    }
}

Date Date::startOf(TimeUnit value) const noexcept {
    switch (value) {
        case Second:
            return (*this);
        case Minute:
            return (*this)
            - Date(DateFormatter::second(*this));
        case Hour:
            return (*this)
            - (Date)(Date(DateFormatter::minute(*this))
                     * Date::TimeUnit::Minute)
            - Date(DateFormatter::second(*this));
        case Day:
            return (*this)
            - (Date)(Date(DateFormatter::hour(*this))
                     * Date::TimeUnit::Hour)
            - (Date)(Date(DateFormatter::minute(*this))
                     * Date::TimeUnit::Minute)
            - Date(DateFormatter::second(*this));
        case Week:
            return (*this)
            - (Date)(DateFormatter::weekDay(*this)
                     * Date::TimeUnit::Day)
            - (Date)(Date(DateFormatter::hour(*this))
                     * Date::TimeUnit::Hour)
            - (Date)(Date(DateFormatter::minute(*this))
                     * Date::TimeUnit::Minute)
            - Date(DateFormatter::second(*this));
        case Month:
            return {
                DateFormatter::year(*this),
                DateFormatter::month(*this),
                1
            };
        case Year:
            return { DateFormatter::year(*this), 1, 1 };
    }
}

// MARK: Operataor overloading.

Date Date::operator+(const Date &other) const noexcept {
    return _seconds + other._seconds;
}

Date Date::operator-(const Date &other) const noexcept {
    return _seconds - other._seconds;
}

Date Date::operator+(TimeUnit value) const noexcept {
    return Date(*this).add(1, value);
}

Date Date::operator-(TimeUnit value) const noexcept {
    return Date(*this).add(-1, value);
}

Date &Date::operator+=(const Date &other) noexcept {
    _seconds += other._seconds;
    return *this;
}

Date &Date::operator-=(const Date &other) noexcept {
    _seconds -= other._seconds;
    return *this;
}

Date &Date::operator+=(TimeUnit value) noexcept {
    _seconds = add(1, value);
    return *this;
}

Date &Date::operator-=(TimeUnit value) noexcept {
    _seconds = add(-1, value);
    return *this;
}


Date::operator time_t() const noexcept {
    return _seconds;
}

Date &Date::operator=(time_t other) noexcept {
    _seconds = other;
    return *this;
}

Date &Date::operator=(const Date &other) noexcept {
    if (this != &other) {
        _seconds = other._seconds;
    }
    return *this;
}

bool Date::operator<(const Date &other) const noexcept {
    return _seconds < other._seconds;
}
bool Date::operator<=(const Date &other) const noexcept {
    return _seconds <= other._seconds;
}
bool Date::operator>(const Date &other) const noexcept {
    return _seconds > other._seconds;
}
bool Date::operator>=(const Date &other) const noexcept {
    return _seconds >= other._seconds;
}
bool Date::operator==(const Date &other) const noexcept {
    return _seconds == other._seconds;
}
bool Date::operator!=(const Date &other) const noexcept {
    return _seconds != other._seconds;
}

// MARK: Static methods

Date Date::now() noexcept {
    return std::time(nullptr);
}

/* struct Date */


// MARK: class DateInterval

DateInterval::Iterator::
Iterator(const Date &date, Date::TimeUnit step) noexcept
: date(date), step(step)
{}

// MARK: Iterator overloading
const Date &DateInterval::Iterator::operator*() const noexcept {
    return date;
}
DateInterval::Iterator &DateInterval::Iterator::operator++() noexcept {
    date += step;
    return *this;
}
bool DateInterval::Iterator::operator==(const Iterator &it) const noexcept {
    return date == it.date;
}
bool DateInterval::Iterator::operator!=(const Iterator &it) const noexcept {
    return date < it.date;
}

// MARK:: Public implementation

DateInterval::DateInterval(const Date &dateStart,
                           const Date &dateEnd,
                           Date::TimeUnit step) noexcept
: _dateStart(dateStart), _dateEnd(dateEnd), _step(step)
{}

DateInterval::~DateInterval() noexcept
{}

DateInterval::Iterator DateInterval::begin() const noexcept {
    return { _dateStart, _step };
}

DateInterval::Iterator DateInterval::end() const noexcept {
    return { _dateEnd, _step };
}

DateInterval DateInterval::add(int amound, Date::TimeUnit value) const noexcept {
    return {
        _dateStart.add(amound, value),
        _dateEnd.add(amound, value)
    };
}


// MARK: Accessor methods.

const Date &DateInterval::dateStart() const noexcept { return _dateStart; }

const Date &DateInterval::dateEnd()   const noexcept { return _dateEnd; }

Date::TimeUnit DateInterval::step()   const noexcept { return _step; }

json DateInterval::toJSON() const {
    return {
        {"date_start", _dateStart},
        {"date_end", _dateEnd}
    };
}

void DateInterval::assign(const json &j) {
    _dateStart = j.at("date_start").get<Date>();
    _dateEnd = j.at("date_end").get<Date>();
}
    
std::size_t DateInterval::hash() const noexcept {
    return _dateStart.hash() ^ _dateEnd.hash() ^ _step;
}

// MARK: Compare methods
    
bool DateInterval::intersects(const DateInterval &other) const noexcept {
    return
    (_dateStart <= other._dateStart && _dateEnd >= other._dateStart)
    || (_dateEnd   >= other._dateEnd   && _dateStart <= other._dateEnd)
    || (_dateStart >= other._dateStart && _dateEnd   <= other._dateEnd);
}

bool DateInterval::contains(const Date &date) const noexcept {
    return _dateStart <= date && date <= _dateEnd;
}
    
bool DateInterval::contains(const DateInterval &other) const noexcept {
    return _dateStart <= other._dateStart && _dateEnd >= other._dateEnd;
}

// MARK: Mutating methods.

void DateInterval::setStep(Date::TimeUnit step) noexcept { _step = step; }


// MARK: Operator overloading.

DateInterval DateInterval::operator+(Date::TimeUnit value) const noexcept {
    return {
        _dateStart + value,
        _dateEnd + value
    };
}

DateInterval DateInterval::operator-(Date::TimeUnit value) const noexcept {
    return {
        _dateStart - value,
        _dateEnd - value
    };
}

DateInterval &DateInterval::operator+=(Date::TimeUnit value) noexcept {
    _dateStart += value;
    _dateEnd += value;
    return *this;
}

DateInterval &DateInterval::operator-=(Date::TimeUnit value) noexcept {
    _dateStart -= value;
    _dateEnd -= value;
    return *this;
}

bool DateInterval::operator==(const DateInterval &other) const noexcept {
    return _dateStart   == other._dateStart
    && _dateEnd == other._dateEnd
    && _step    == other._step;
}

bool DateInterval::operator!=(const DateInterval &other) const noexcept {
    return _dateStart   != other._dateStart
    || _dateEnd != other._dateEnd
    || _step    != other._step;
}

bool DateInterval::operator<(const DateInterval &other) const noexcept {
    return _dateStart < other._dateStart;
}
bool DateInterval::operator>(const DateInterval &other) const noexcept {
    return _dateStart > other._dateStart;
}
bool DateInterval::operator<=(const DateInterval &other) const noexcept {
    return _dateStart <= other._dateStart;
}
bool DateInterval::operator>=(const DateInterval &other) const noexcept {
    return _dateStart >= other._dateStart;
}
    
// Mark: static methods

DateInterval DateInterval::sinceDate(const Date &date) noexcept {
    return DateInterval(date, Date::now());
}

DateInterval DateInterval::sinceJan1970() noexcept {
    return DateInterval(0, Date::now());
}

DateInterval DateInterval::wholeMonthInterval(const Date &date) noexcept {
    return {
        date.startOf(Date::TimeUnit::Month),
        date.add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month)
    };
}
    
DateInterval DateInterval::
wholeMonthInterval(const DateInterval &interval) noexcept
{
    return {
        interval._dateStart.startOf(Date::TimeUnit::Month),
        interval._dateEnd.add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month)
    };
}

/* class DateInterval */

    
    
namespace planner {
    void to_json(json &j, const Date &date) noexcept {
        j = date.toJSON();
    }
    
    void from_json(const json &j, Date &date) {
        date.assign(j);
    }
    
    
    std::ostream &operator<<(std::ostream &os,
                             const DateInterval &interval) noexcept
    {
        return os << "DateInterval ( "
        << interval._dateStart << ", "
        << interval._dateEnd << " )";
    }
    
    void to_json(json &j, const DateInterval &interval) noexcept {
        j = interval.toJSON();
    }
    
    void from_json(const json &j, DateInterval &interval) {
        interval.assign(j);
    }
}

namespace std {
    size_t hash<Date>::
    operator()(const planner::Date &date) {
        return date.hash();
    }
    
    size_t hash<DateInterval>::
    operator()(const planner::DateInterval &interval) {
        return interval.hash();
    }
    
} /* end namespace std */


