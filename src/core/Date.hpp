//===- Date.hpp - Date & Datetime -------------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file Date.hpp
///
/// \brief This file contains declaration and definition of the EventPlanner
///  core Date and Datetime objects and structures.
///
/// \description This header file contains the \c Date structure and
///  \c DateInterval class, that provide interface for working with dates and
///  date times. This header also includes the \c DateInvalidFormatException,
///  which specified, that the given date is invalid (e.g. Jan with 32+ days,
///  13th or higher month, Feb 29th on not leaping year, ...).
///
//===----------------------------------------------------------------------===//

#ifndef Date_hpp
#define Date_hpp

#include "DateFormatter.hpp"
#include "protocols/Hashable.hpp"
#include "protocols/JSONable.hpp"

#include <ctime>
#include <iostream>
#include <locale>
#include <string>
#include <iomanip>

namespace planner {
    // For the json convenience
    using json = nlohmann::json;
    
    /// \brief A struct, that encapsulates a single point in time. The Date
    ///  structs are \b immutable and the time information, which it stores
    ///  does not conform to any calendar system, nor any time zone.
    ///
    /// \description This struct provides an interface for comparing dates,
    ///  calculating time inbetween the two dates. It can parse the date
    ///  from multiple formats and also output it. The \c Date structs are used
    ///  by \c Calendar objects to perform any calendar arithmetics.
    ///
    ///  Internally what \c Date does, is that after parsing date, it converts
    ///  it into days and seconds since 00:00:00 UTC Jan 1, 1970. Then these
    ///  values are used in all arithmetic operations. The classic date format
    ///  is calculated back each time, any otuput format method is called upon
    ///  a \c Date struct.
    ///
    /// \see https://en.wikipedia.org/wiki/Determination_of_the_day_of_the_week
    struct Date: public protocol::Hashable,
                 public protocol::JSONable
    {
        static const int64_t kSECONDS_PER_DAY = 86400;
        
        /// Specified the time value.
        enum TimeUnit {
            /// Number of seconds in a second.
            Second = 1,
            /// Number of seconds in a minute.
            Minute = 1 * 60,
            /// Number of seconds in an hour.
            Hour   = 1 * 60 * 60,
            /// Number of seconds in a day.
            Day    = 1 * 60 * 60 * 24,
            /// Number of seconds in a week (7 days).
            Week   = 1 * 60 * 60 * 24 * 7,
            /// Number of seconds in 31 days.
            Month  = 1 * 60 * 60 * 24 * 31,
            /// Number of seconds in 366 days.
            Year   = 1 * 60 * 60 * 24 * 366
        };
        
    private:
        /// Number of seconds from the 00:00:00 UTC Jan 1, 1970
        time_t _seconds;
        
        /// \brief Creates a new date with provided number of months
        ///  ahead of the current date. This method also resolves the
        ///  resulting date being Feb 29th on not leaping year by changing
        ///  the date to March 1st.
        ///
        /// \param amound Number of months, to put a new date ahead of the
        ///  current date.
        /// \return A \c Date struct encapsulating a date, that is \c amound
        ///  number of months ahed of the current date.
        /// \throw DateInvalidFormatException
        Date addMonths(int amound) const noexcept;
        
        /// \brief Creates a new date with provided number of years
        ///  ahead of the current date.This method also resolves the
        ///  resulting date being Feb 29th on not leaping year by changing
        ///  the date to March 1st.
        ///
        /// \param amound Number of years, to put a new date ahead of the
        ///  current date.
        /// \return A \c Date struct encapsulating a date, that is \c amound
        ///  number of years ahed of the current date.
        /// \throw DateInvalidFormatException
        Date addYears(int amound) const noexcept;
        
    public:
        // MARK: Creation and initialization

        /// Initializes date to 00:00:00 UTC Jan 1, 1970
        Date() noexcept;
        
        /// Initilalizes date with given number of seconds
        /// form the 00:00:00 UTC Jan 1, 1970.
        Date(time_t seconds) noexcept;
        
        /// Initializes date with given year, month and day.
        Date(int year, int month, int day) throw();
        
        // MARK: Accessor
        
        /// Returns the number of passed seconds from the 00:00:00 Jan 1, 1970
        /// until the encapsulated date.
        inline time_t seconds() const noexcept { return _seconds; }
        
        /// Returns a ISO date format of a date.
        /// \return JSON format of a date.
        virtual json toJSON() const override;
        
        /// Assigns contents of json to self.
        virtual void assign(const json &j) override;
        
        /// \brief Creates a hash of a struct.
        /// \return A hash of the struct.
        virtual std::size_t hash() const noexcept override;
        
        // MARK: Date calculations.
        
        /// \brief Encapsulates a point in time exactly provided number of
        ///  time values ahead of the current one. This method also resolves the
        ///  resulting date being Feb 29th on not leaping year by changing
        ///  the date to March 1st.
        ///
        ///  \param amound The amound of the time value to put the new point
        ///   in time ahead of the current ont.
        ///  \param value The time value of which to put the new point in time
        ///   ahead of the current one.
        ///  \return Encapsulated new point in time.
        ///  \throw DateInvalidFormatException
        Date add(int amound, TimeUnit value) const noexcept;

        /// \breif Returns a \c Date, that starts on the provided unit date
        ///  of the date.
        ///
        /// \param value The unit of time, which start should be encapsulated.
        /// \return Encapsulated new point in time.
        Date startOf(TimeUnit value) const noexcept;
        
        // MARK: Operataor overloading.
        
        /// \brief Adds two dates.
        /// \return A sum of the dates.
        Date operator+(const Date &other) const noexcept;
        
        /// \brief Subtracts two dates.
        /// \return A subtraction of the dates.
        Date operator-(const Date &other) const noexcept;
        
        /// \brief Adds a time unit to the date.
        /// \return A date, with added time unit.
        Date operator+(TimeUnit value) const noexcept;
        
        /// \brief Subtracts a time unit to the date.
        /// \return A date, with subtracted time unit.
        Date operator-(TimeUnit value) const noexcept;
        
        /// \brief Increments a date by a date.
        /// \return A date, with added date.
        Date &operator+=(const Date &other) noexcept;
        
        /// \brief Decrements a date by a date.
        /// \return A date, with subtracted date.
        Date &operator-=(const Date &other) noexcept;
        
        /// \brief Increments a date by a time unit.
        /// \return A date, with added time unit.
        Date &operator+=(TimeUnit value) noexcept;
        
        /// \brief Decrements a date by a time unit.
        /// \return A date, with subtracted time unit.
        Date &operator-=(TimeUnit value) noexcept;
        
        /// Date structs implicity convert to \c time_t values,
        /// representing the number of seconds since 00:00:00 Jan 1, 1970 until
        /// the encapsulated moment.
        operator time_t() const noexcept;
        
        Date &operator=(time_t other) noexcept;
        Date &operator=(const Date &other) noexcept;
        bool operator<(const Date &other) const noexcept;
        bool operator<=(const Date &other) const noexcept;
        bool operator>(const Date &other) const noexcept;
        bool operator>=(const Date &other) const noexcept;
        bool operator==(const Date &other) const noexcept;
        bool operator!=(const Date &other) const noexcept;
        
        friend std::ostream &
        operator<<(std::ostream &os, const Date &date) noexcept
        {
            return os << "Date ( "
            << DateFormatter::stringFromDate(date, "%Y-%m-%d %H:%M:%S")
            << " )";
        }
        
        // MARK: Static methods
        
        /// \brief Returns an integer value with number of seconds since
        ///  00:00:00 UTC Jan 1, 1970
        /// \return Number of seconds since 00:00:00 UTC Jan 1, 1970 until now.
        static Date now() noexcept;
        
    }; /* struct Date */
    
    
    /// JSON library compatability function.
    /// Converts \c Date to appropriate JSON format.
    void to_json(json &j, const Date &date) noexcept;
    
    /// JSON library compatability function.
    /// Converts appropriate JSON format to \c Date.
    void from_json(const json &j, Date &date);
    
    /// \brief A class that that encapsualtes a time interval between two
    ///  single points in time.
    ///
    /// \description The encapsulated date interval. This class implements
    ///  C++11 \c for \c range standart, which means, you can easily iterate
    ///  over the date interval. The step by which the interval changes, is
    ///  provided upon construction of a date interval instance and/or can be
    ///  set during the life of an instance with \c setStep method.
    class DateInterval: public protocol::Hashable, protocol::JSONable {
    protected:
        /// The start date of the interval.
        Date _dateStart;
        /// The end day of the interval.
        Date _dateEnd;
        /// The step of the interval.
        Date::TimeUnit _step;
        
    public:
        
        /// \brief A struct, that implements the standart iterator behaviour
        ///  over the \c DateInterval objects.
        struct Iterator {
            /// The date of the current step.
            Date date;
            /// The step of the iteration.
            Date::TimeUnit step;
            
            /// \brief Constructs a \c DateInterval iterator.
            /// \param date The start date of the iteration.
            /// \param step The step of the iteration.
            Iterator(const Date &date, Date::TimeUnit step) noexcept;
            
            // MARK: Operator overloading.
            const Date &operator*() const noexcept;
            Iterator &operator++() noexcept;
            bool operator==(const Iterator &it) const noexcept;
            bool operator!=(const Iterator &it) const noexcept;
            
        }; /* DateIntervalIterator */
        
        /// \brief Creates a date interval with provided date start and end.
        ///  The step of the interval is defauled to \c Date::TimeValue::Day.
        /// \throw DateInvalidFormatException
        DateInterval(const Date &dateStart,
                     const Date &dateEnd,
                     Date::TimeUnit step = Date::TimeUnit::Day) noexcept;
        
        /// \brief Destructor for the \c DateInterval objects.
        virtual ~DateInterval() noexcept;
        
        /// \brief The C++ standart method for retrieving the begin iterator.
        /// \return The \c DateIntervalIterator to the start of the date
        ///  interval.
        virtual Iterator begin() const noexcept;
        
        /// \brief The C++ standart method for retrieving the end iterator.
        /// \return The \c DateIntervalIterator to the end of the date interval.
        virtual Iterator end() const noexcept;
        
        /// \brief Shifts the date interval for the amound of the time value.
        ///
        /// \param amound The amound of the time value to shift the interval.
        /// \param value The time value, which to shift the interval.
        DateInterval add(int amound, Date::TimeUnit value) const noexcept;
        
        
        // MARK: Accessor methods.
        /// \return The start of the interval.
        virtual const Date &dateStart() const noexcept;
        /// \return The end of the interval.
        virtual const Date &dateEnd()   const noexcept;
        /// \return The step of the interval.
        virtual Date::TimeUnit step()   const noexcept;
        
        /// Returns a JSON representation of a date interval.
        /// \return A JSON representation of a date interval.
        virtual json toJSON() const override;
        
        /// Assigns contents of json to self.
        virtual void assign(const json &j) override;
        
        /// \brief Creates a hash of an object.
        /// \return A hash of an object.
        virtual std::size_t hash() const noexcept override;
        
        // MARK: Compare methods
        
        /// \brief Checks, wheather the given \c DateInterval intersects
        ///  with the other one.
        /// \param other The other \c DateInterval, that should be compared
        ///  to the current object.
        /// \return \c true if the intervals intesect, \c false otherwise.
        virtual bool intersects(const DateInterval &other) const noexcept;
        
        /// \brief Checks, wheather the \c DateInterval contains the
        ///  given date.
        /// \param other The \c Date, that should be comapred to the
        ///  current object.
        /// \return \c true if the date is fully contained by the dateinterval,
        ///  \c false otherwise.
        virtual bool contains(const Date &other) const noexcept;
        
        /// \brief Checks, wheather the \c DateInterval is contains the other
        ///  one.
        /// \param other THe other \c DateInterval, that should be comapred
        ///  to the current object.
        /// \return \c true if the other interval is fully contained by the
        ///  current one, \c false otherwise.
        virtual bool contains(const DateInterval &other) const noexcept;
        
        // MARK: Mutating methods.
        
        /// \brief Sets the values of the interval.
        virtual void setStep(Date::TimeUnit step) noexcept;
        
        
        // MARK: Operator overloading.
        
        /// \brief Adds a date unit to the date interval.
        /// \return A new date interval with added date uint.
        virtual DateInterval operator+(Date::TimeUnit value) const noexcept;
        
        /// \brief Subtracts a date unit to the date interval.
        /// \return A new date interval with subtracted date uint.
        virtual DateInterval operator-(Date::TimeUnit value) const noexcept;
        
        /// \brief Increments the date interval by the date unit.
        /// \return Incremented date interval.
        virtual DateInterval &operator+=(Date::TimeUnit value) noexcept;
        
        /// \brief Decrements the date interval by the date unit.
        /// \return Decremeneted date interval.
        virtual DateInterval &operator-=(Date::TimeUnit value) noexcept;
        
        /// \brief Compares two date intervals.
        /// \return \c true if the date interval equal, \c false otherwise.
        virtual bool operator==(const DateInterval &other) const noexcept;
        
        /// \brief Compares two date intervals.
        /// \return \c true if the date interval don't equal, \c false otherwise.
        virtual bool operator!=(const DateInterval &other) const noexcept;

        virtual bool operator<(const DateInterval &other) const noexcept;
        virtual bool operator>(const DateInterval &other) const noexcept;
        virtual bool operator<=(const DateInterval &other) const noexcept;
        virtual bool operator>=(const DateInterval &other) const noexcept;
        
        // Mark: static methods
        
        /// \brief Retrieves a date interval since the provided date to now.
        /// \param date The date, when the interval should be started.
        /// \return A new date interval instance.
        static DateInterval sinceDate(const Date &date) noexcept;
        
        /// \brief Retrieves a date interval since the 00:00:00 UTC Jan 1, 1970.
        /// \return A new date interval instance.
        static DateInterval sinceJan1970() noexcept;
        
        /// \brief Creates a new date interval, which contains the whole
        ///  given date interval, while starting and ending on the 1st
        ///  of the month.
        /// \return A date interval, containing the given date interval.
        static DateInterval wholeMonthInterval(const Date &date) noexcept;
        
        /// \brief Creates a new date interval, which contains the whole
        ///  given date interval, while starting and ending on the 1st
        ///  of the month.
        /// \return A date interval, containing the given date interval.
        static DateInterval
        wholeMonthInterval(const DateInterval &interval) noexcept;
        
        friend std::ostream &operator<<(std::ostream &os,
                                        const DateInterval &interval) noexcept;
        
    }; /* class DateInterval */
    
    /// JSON library compatability function.
    /// Converts \c DateIterval to appropriate JSON format.
    void to_json(json &j, const DateInterval &interval) noexcept;
    
    /// JSON library compatability function.
    /// Converts appropriate JSON format to \c DateInterval.
    void from_json(const json &j, DateInterval &interval);
    
} /* end namespace planner */


namespace std {
    
    /// std::hash temaplte specialization for planner::Date strucs.
    template <>
    struct hash<planner::Date> {
        size_t operator()(const planner::Date &date);
    };
    
    /// std::hash temaplte specialization for planner::DateInterval objects.
    template <>
    struct hash<planner::DateInterval> {
        size_t operator()(const planner::DateInterval &interval);
    };
} /* end namespace std */

#endif /* Date_hpp */
