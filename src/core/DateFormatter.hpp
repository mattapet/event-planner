//===- DateFormatter.hpp - Datetime formatting ------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file DateFormatter.hpp
///
/// \brief This file contains declaration and definition of the DateFormatter
///  class, that is used to create a Date structs and/or transform them to
///  to a human readable string form.
///
/// \description This header file contains the \c DateFormatter class, that
///  is used for parsing Date structs to human readable strings of choice and/or
///  the the strings back to hte Date structs themselves.
///
//===----------------------------------------------------------------------===//

#ifndef DateFormatter_hpp
#define DateFormatter_hpp

#include <ctime>
#include <iomanip>
#include <iostream>
#include <string>
#include <functional>

namespace planner {
    const std::string kISO_FORMAT_STRING = "%Y%m%dT%H%M%SZ";
    
    /// \brief Exception associated with \c Date structs, which is thrown
    ///  when an invalid date is provided to the struct's either static,
    ///  member method and/or constructor.
    ///
    /// \description This exception which specifies, that the given date is
    ///  invalid (e.g. Jan with 32+ days, 13th or higher month, Feb 29th on
    ///  not leaping year, ...).
    /// \inherits std::invalid_argumets
    class DateInvalidFormatException: public std::invalid_argument {
    public:
        explicit DateInvalidFormatException() noexcept;
    };
    
    /// \brief Contains static utility methods, that are used upon the \c Date
    ///  structs and/or other time representing types, to converts the values
    ///  into human-readable date strings.
    ///
    /// \description The \c DateFormatter class by itself contains \b only
    ///  static methods, therefore there is no reason to create any instances
    ///  of the class.
    class DateFormatter {
    protected:
        /// Formatting string that specifies the date format.
        std::string _formatString;
        
    public:
        /// Constructor taking \c formatString as a initialization argument.
        DateFormatter(const std::string &formatString = kISO_FORMAT_STRING) noexcept;
        
        /// Movable constructor taking \c formatString as a initialization
        /// argument.
        DateFormatter(std::string &&formatString) noexcept;
        
        /// Virtual descructor
        virtual ~DateFormatter() noexcept;
        
        // MARK: Accessors
        
        /// Returns the formatting string of the formtatter.
        virtual const std::string &formatString() const noexcept;
        
        /// Returns the formatting string of the formtatter.
        virtual std::string formatString() noexcept;
        
        /// Sets the formatting string of the formatter.
        virtual void setFormatString(const std::string &formatString) noexcept;
        
        /// Formats the given date string into a date representation.
        virtual time_t format(const std::string &dateString) const;
        
        /// Formats the given date string into a human readable date
        /// representation according to current format string.
        virtual std::string format(time_t date) const noexcept;
        
        // MARK: Static methods
        
        /// \brief Calculates the second part of the encapsulated date.
        /// \return The second of a minute.
        static int second(time_t seconds) noexcept;
        
        /// \brief Calculates the minute part of the encapsulated date.
        /// \return The minute of an hour.
        static int minute(time_t seconds) noexcept;
        
        /// \brief Calculates the day part of the encapsulated date.
        /// \return The hour of a day.
        static int hour(time_t seconds) noexcept;
        
        /// \brief Calculates the number of the day in week of the encapsulated
        ///  date. The week day numbers start with 0 and goes to 6 with
        ///  the first day of the week beeing Monday.
        /// \return The day number.
        static int weekDay(time_t seconds) noexcept;
        
        /// \brief Calculates the day part of the encapsulated date.
        /// \return The day number.
        static int day(time_t seconds) noexcept;
        
        /// \brief Calculates the month part of the encapsulated date.
        /// \return The month number.
        static int month(time_t seconds) noexcept;
        
        /// \brief Calculates the year part of the encapsulated date.
        /// \return The year number.
        static int year(time_t seconds) noexcept;
        
        /// \brief Validates the date.
        ///
        /// \param year The year of the date.
        /// \param month The month of the date.
        /// \param day The day of the date.
        /// \return \c true if the date is valid, \c false otherwise.
        static bool isValid(uint32_t year, uint8_t month, uint8_t day) noexcept;
        
        /// \brief Returns a boolean value identifying the given year
        ///  being leap.
        /// \return \c true if the year is leap, \c false otherwise.
        static bool isLeapYear(uint32_t year) noexcept;
        
        // MARK: Formatting methods
        
        /// \brief Creates a ISO string from the encapsulated date.
        /// \return Date in the ISO format string.
        static std::string toISOString(time_t seconds) noexcept;
        
        /// \brief Transform the number of seconds, since the epoch to the
        ///  human readable form using the provided format string.
        /// \param seconds The number of seconds since 00:00:00 Jan 1, 1970
        ///  representing a single point in time.
        /// \param formatString The format string describring the the resulting
        ///  date string.
        /// \return The human readable date string.
        static std::string
        stringFromDate(time_t seconds, const std::string &formatString) noexcept;
        
        /// \brief Parses a date string according to the given format string.
        ///
        /// \description Interally, the DateFormatter uses \c ctime functions
        ///  to handle all date operations, therefore the format string should
        ///  conform to the standart C++ ctime ones.
        /// \param str The date string.
        /// \param dateFormat The date format string. Determins how to parse
        ///  the given date string.
        /// \return Number of seconds since 00:00:00 Jan 1, 1970 until the date.
        /// \throw DateInvalidFormatException
        /// \see http://en.cppreference.com/w/cpp/chrono/c/strftime
        static time_t dateFromString(const std::string &str,
                                     const std::string &dateFormat);
        
        /// \brief Parses a year-month-day formated date string into number of
        ///  seconds since 00:00:00 Jan 1, 1970 until the date.
        /// \param year A year.
        /// \param month A month.
        /// \param day A day.
        /// \return Number of seconds since 00:00:00 Jan 1, 1970 until the date.
        /// \throw DateInvalidFormatException
        static time_t dateFromFormat(int year, int month, int day);
        
        /// \brief Parses a ISO formated date string into number of seconds
        ///  since 00:00:00 Jan 1, 1970 until the date.
        /// \param str The date string.
        /// \return Number of seconds since 00:00:00 Jan 1, 1970 until the date.
        /// \throw DateInvalidFormatException
        static time_t fromISOString(const std::string &str);
        
    }; /* class DateFormatter */
    
} /* end namespace planner */

#endif /* DateFormatter_hpp */
