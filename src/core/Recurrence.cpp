//===- Recurrence.cpp -----------------------------------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file Recurrence.cpp
///
/// \brief This file contains implementaion of the \c Recurrence class, and
///  associate exceptions used to encapsulating event recurrence.
//===----------------------------------------------------------------------===//

#include "Recurrence.hpp"
#include <unordered_map>
#include <sstream>

using namespace planner;

void validateOnDays(Date::TimeUnit unit, const std::set<size_t> &onDays) {
    size_t daysTreshold = 0;
    switch (unit) {
        case Date::TimeUnit::Day:
            daysTreshold = 0;
            break;
        case Date::TimeUnit::Week:
            daysTreshold = 7;
            break;
        case Date::TimeUnit::Month:
            daysTreshold = 31;
            break;
        case Date::TimeUnit::Year:
            daysTreshold = 366;
            break;
        default:
            throw InvalidRecurrenceUnitException();
    }
    
    for (auto onDay : onDays) {
        if ((daysTreshold && !onDay) || onDay > daysTreshold) {
            throw InvalidNumberOfDaysException();
        }
    }
}

InvalidRecurrenceUnitException::InvalidRecurrenceUnitException()
: std::invalid_argument("Recurrence can repeat only on day or any longer date unit.")
{}

InvalidNumberOfDaysException::InvalidNumberOfDaysException()
: std::invalid_argument("There was an invalid number of days in seleced date unit.")
{}

// MARK: Constructors
Recurrence::Recurrence(Date::TimeUnit unit_,
                       size_t frequency_,
                       std::set<size_t> onDays_)
: unit(unit_), frequency(frequency_), onDays(onDays_) {
    validateOnDays(unit, onDays);
}

Recurrence::Recurrence(Date::TimeUnit unit_,
                       size_t frequency_,
                       Date endDate_,
                       std::set<size_t> onDays_)
: unit(unit_), frequency(frequency_), onDays(onDays_), endDate(endDate_) {
    validateOnDays(unit, onDays);
}

Recurrence::Recurrence(Date::TimeUnit unit_,
                       size_t frequency_,
                       size_t count_,
                       std::set<size_t> onDays_)
: unit(unit_), frequency(frequency_), onDays(onDays_), count(count_) {
    validateOnDays(unit, onDays);
}

Recurrence::Recurrence(json jsonObject)
: unit(std::unordered_map<std::string, Date::TimeUnit>({
    {"DAY", Date::TimeUnit::Day}, {"WEEK", Date::TimeUnit::Week},
    {"MONTH", Date::TimeUnit::Month}, {"YEAR", Date::TimeUnit::Year}
})[jsonObject["unit"]]),
frequency(jsonObject["frequency"])

{
    for (auto onDay : jsonObject["on_days"]) {
        onDays.insert((size_t)onDay);
    }
    try {
        endDate = DateFormatter::fromISOString(jsonObject.at("end_date"));
    } catch(const nlohmann::detail::out_of_range &err) {}
    try {
        count = (size_t)jsonObject.at("count");
    } catch(const nlohmann::detail::out_of_range &err) {}
    validateOnDays(unit, onDays);
}

// MARK: Public Implementation

std::string Recurrence::toICalString() const {
    std::stringstream ss;
    ss << "FREQ=";
    switch (unit) {
        case Date::TimeUnit::Day:
            ss << "DAILY";
            break;
        case Date::TimeUnit::Week:
            ss << "WEEKLY";
            if (onDays.size()) {
                const char *days[] = {
                    "MO", "TU", "WE", "TH", "FR", "SA", "SU"
                };
                ss << ";BYDAY=" << days[*onDays.begin()];
                for (auto onDay : onDays) {
                    if (onDay != *onDays.begin()) {
                        ss << "," << days[onDay];
                    }
                }
            }
            break;
        case Date::TimeUnit::Month:
            ss << "MONTHY";
            if (onDays.size()) {
                ss << ";BYMONTHDAY=" << *onDays.begin();
                for (auto onDay : onDays) {
                    if (onDay != *onDays.begin()) {
                        ss << "," << onDay;
                    }
                }
            }
            break;
        case Date::TimeUnit::Year:
            ss << "YEARLY";
            if (onDays.size()) {
                ss << ";BYYEARDAY=" << *onDays.begin();
                for (auto onDay : onDays) {
                    if (onDay != *onDays.begin()) {
                        ss << "," << onDay;
                    }
                }
            }
            break;
        default:break;
    }
    if (frequency > 2) {
        ss << ";INTERVAL=" << frequency;
    }
    if (auto endDate_ = endDate) {
        ss << ";UNTIL=" << DateFormatter::toISOString(endDate_.value());
    }
    if (auto count_ = count) {
        ss << ";COUNT=" << DateFormatter::toISOString(count_.value());
    }
    return ss.str();
}

json Recurrence::toJSON() const {
    std::string unitName = "";
    std::vector<size_t> onDays_;
    for (auto onDay : onDays) {
        onDays_.push_back(onDay);
    }
    switch (unit) {
        case Date::TimeUnit::Day:
            unitName = "DAY";
            break;
        case Date::TimeUnit::Week:
            unitName = "WEEK";
            break;
        case Date::TimeUnit::Month:
            unitName = "MONTH";
            break;
        case Date::TimeUnit::Year:
            unitName = "YEAR";
            break;
        default:break;
    }
    json out = {
        {"unit", unitName},
        {"frequency", frequency},
        {"on_days", onDays_}
    };
    if (auto endDate_ = endDate) {
        out["end_date"] = DateFormatter::toISOString(endDate.value());
    }
    if (auto count_ = count) {
        out["count"] = count.value();
    }
    return out;
}

namespace planner {
    void to_json(json &j, const Recurrence &recurrence) noexcept {
        j = recurrence.toJSON();
    }
    
    Recurrence from_json(const json &j) {
        return Recurrence(j);
    }
}

