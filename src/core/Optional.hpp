//===- Optional.hpp  - Optional type template -------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file Optional.hpp
///
/// \brief This file contains declaration and definition of a Optional template
///  class that can be used for creating optional variables.
///
/// \description This header file contains the \c Optional template class and
///  \c BadOptionalAccess exception, that implement the `optional variables`
///   behaviour similar to the one the Swfit programming language uses.
///
/// \see https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html
///
//===----------------------------------------------------------------------===//

#ifndef Optional_hpp
#define Optional_hpp

#include <exception>

namespace planner {
    
    /// \brief An exception, that is thrown, when an empty optional is
    ///  being unwrapped.
    class BadOptionalAccess: public std::runtime_error {
    public:
        BadOptionalAccess()
        : std::runtime_error("Unexcepted empty value found while unwrapping an optional.")
        {}
    };
    
    /// \brief A class, that wrapps an \c Element type to create an \c Element
    ///  \c optional.
    ///
    /// \description Optional class creates an optional version of a provided
    ///  type. An options can, but does not have to have a value set. To get
    ///  a value wrapped in the optional, it needs to be 'unwrapped'.
    template <typename Element>
    class Optional {
        Element _value;
        bool    _empty;
        
    public:
        /// Creates an empty optional.
        explicit Optional() noexcept: _empty(true)
        {}
        
        /// Creates an optional with a value.
        Optional(const Element &value) noexcept
        : _value(value), _empty(false)
        {}
        
        /// \brief Unwrapps a value.
        /// \return An unwrapped value.
        /// \throw BadOptionalAccess
        Element &value() throw() {
            if (_empty) {
                throw BadOptionalAccess();
            }
            return _value;
        }
        
        /// \brief Unwrapps a value.
        /// \return An unwrapped value.
        /// \throw BadOptionalAccess
        const Element &value() const throw() {
            if (_empty) {
                throw BadOptionalAccess();
            }
            return _value;
        }
        
        /// \brief Returns a boolean value indicating wheather an optional
        ///  contains a value.
        /// \return \c true if an optional contains a value, \c false otherwise.
        bool hasValue() const noexcept { return !_empty; }
        
        /// \brief Implicitly converts into a boolean value indicating wheather
        /// an optional contains a value.
        /// \return \c true if an optional contains a value, \c false otherwise.
        operator bool() const noexcept { return !_empty; }
        
        /// \brief Unwrapps a value.
        /// \return An unwrapped value.
        /// \throw BadOptionalAccess
        Element &operator*() throw() {
            return value();
        }
        
        /// \brief Unwrapps a value.
        /// \return An unwrapped value.
        /// \throw BadOptionalAccess
        Element &operator->() throw() {
            return value();
        }
        
        /// \brief Convenience operator overloading.
        /// \return Result of comparison, when the optional has value,
        ///  \c false otherwise
        bool operator>(const Element &other) const {
            return !_empty && _value > other;
        }
        
        /// \brief Convenience operator overloading.
        /// \return Result of comparison, when the optional has value,
        ///  \c false otherwise
        bool operator<(const Element &other) const {
            return !_empty && _value < other;
        }
        
        /// \brief Convenience operator overloading.
        /// \return Result of comparison, when the optional has value,
        ///  \c false otherwise
        bool operator>=(const Element &other) const {
            return !_empty && _value >= other;
        }
        
        /// \brief Convenience operator overloading.
        /// \return Result of comparison, when the optional has value,
        ///  \c false otherwise
        bool operator<=(const Element &other) const {
            return !_empty && _value <= other;
        }
        
        /// \brief Assigns a value to an optional.
        /// \return Optional with assigned value.
        Optional<Element> &operator=(const Element &other) noexcept {
            _value = other;
            _empty = false;
            return *this;
        }
        
    }; /* class Optional */

} /* end namespace planner */

#endif /* Optional_hpp */
