//===- DateFormatter.hpp - Datetime formatting ----------------------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file DateFormatter.cpp
///
/// \brief This file contains implementaion of \c DateFormatter class.
///
//===----------------------------------------------------------------------===//

#include "DateFormatter.hpp"
#include <sstream>

using namespace planner;
    
// MARK: DateInvalidationFormatException
DateInvalidFormatException::DateInvalidFormatException() noexcept
: std::invalid_argument("Provided date format is invalid.")
{}

// MARK: DateFormatter Contructor

DateFormatter::DateFormatter(const std::string &formatString) noexcept
: _formatString(formatString)
{}


DateFormatter::DateFormatter(std::string &&formatString) noexcept
: _formatString(std::move(formatString))
{}

// MARK: Decsructor

DateFormatter::~DateFormatter() noexcept
{}

// MARK: Accessors

std::string DateFormatter::formatString() noexcept {
    return _formatString;
}

const std::string &DateFormatter::formatString() const noexcept {
    return _formatString;
}

void DateFormatter::setFormatString(const std::string &formatString) noexcept {
    _formatString = formatString;
}

time_t DateFormatter::format(const std::string &dateString) const {
    return dateFromString(dateString, _formatString);
}

std::string DateFormatter::format(time_t date) const noexcept {
    return stringFromDate(date, _formatString);
}

// MARK: Static methods

int DateFormatter::second(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    return (uint8_t)time.tm_sec;
}

int DateFormatter::minute(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    return (uint8_t)time.tm_min;
}

int DateFormatter::hour(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    return (uint8_t)time.tm_hour;
}

int DateFormatter::weekDay(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    return (uint8_t)time.tm_wday;
}

int DateFormatter::day(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    return time.tm_mday;
}

int DateFormatter::month(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    // Jan - 0th month.
    return time.tm_mon + 1;
}

int DateFormatter::year(time_t seconds) noexcept {
    auto time = *std::gmtime(&seconds);
    // years in format `1900 + year`
    // e.g. 2017 is represented as 117
    return time.tm_year + 1900;
}

bool DateFormatter::
isValid(uint32_t year, uint8_t month, uint8_t day) noexcept
{
    const static int monthDays[12] = {
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };
    uint8_t leap = isLeapYear(year);
    return year > 1999 && month < 13 && (
        (month == 2 && day <= monthDays[month - 1] + leap)
     || (month != 2 && day <= monthDays[month - 1])
     );
}

bool DateFormatter::isLeapYear(uint32_t year) noexcept {
    return (!!(year % 4000) && !(year % 400))
    || (!!(year % 100)  && !(year % 4));
}

// MARK: Formatting methods

std::string DateFormatter::toISOString(time_t seconds) noexcept {
    char buffer[255];
    auto time = *std::gmtime(&seconds);
    strftime(buffer, 255, kISO_FORMAT_STRING.c_str(), &time);
    return buffer;
}

std::string DateFormatter::
stringFromDate(time_t seconds, const std::string &formatString) noexcept
{
    char buffer[255];
    auto time = *std::gmtime(&seconds);
    // Not using put_time, because of gcc 4.9
    strftime(buffer, 255, formatString.c_str(), &time);
    return buffer;
}

time_t DateFormatter::dateFromString(const std::string &str,
                                     const std::string &dateFormat) {
    std::tm time = {0};
    // Not using get_time, because of gcc 4.9
    if (!strptime(str.c_str(), dateFormat.c_str(), &time)) {
        throw new DateInvalidFormatException();
    }
    return timegm(&time);
}

time_t DateFormatter::dateFromFormat(int year, int month, int day) {
    std::stringstream ss;
    ss << year
    << (month < 10 ? "0" : "") << month
    << (day < 10 ? "0" : "") << day;
    return dateFromString(ss.str(), "%Y%m%d");
}

time_t DateFormatter::fromISOString(const std::string &str) {
    return dateFromString(str, kISO_FORMAT_STRING);
}
