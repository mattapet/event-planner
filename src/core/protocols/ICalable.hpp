//===- ICalable.hpp - iCal formatting protocol ------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file ICalable.hpp
///
/// \brief This file contains declaration of the \c ICalable protocol, which
///  derived objects are able to be properly transformed into iCal format.
///
/// \see https://en.wikipedia.org/wiki/Protocol_(object-oriented_programming)
///
//===----------------------------------------------------------------------===//

#ifndef ICalable_hpp
#define ICalable_hpp

#include <string>

namespace protocol {
    
    /// \brief The objects, that conform to \c ICalable protocol, ale abe be
    ///  exported into iCal format files.
    ///
    /// \description The ICalable protocol requires the derived objects to
    ///  implement only a single method \c toICalString. This method is purely
    ///  abstract, therefore the derived classes, that do not implement the
    ///  method, will also be abstract.
    class ICalable {
    public:
        /// Default constructor.
                 ICalable() = default;
        /// Default virtual destructor.
        virtual ~ICalable() = default;
        
        /// Method, that should transform an object into iCal format and return
        /// it as a string.
        /// \return A string representation of an iCal format of the object.
        virtual std::string toICalString() const = 0;
        
    }; /* class ICalable */
    
} /* end namespace planner */

#endif /* ICalable_hpp */
