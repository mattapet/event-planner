//===- JSONable.hpp - JSON formatting protocol ------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file JSONable.hpp
//===----------------------------------------------------------------------===//

#ifndef JSONable_hpp
#define JSONable_hpp

#include "core/json.hpp"

namespace protocol {
    
    // For the json convenience
    using json = nlohmann::json;
    
    class JSONable {
    public:
                 JSONable() = default;
        virtual ~JSONable() = default;
        
        virtual json toJSON() const = 0;
        virtual void assign(const json &j) {}
    };
}

#endif /* JSONable_hpp */
