//===- Hashable.hpp - Hashable protocol -------------------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
///
/// \file Hashable.hpp
///
/// \brief This file contains declaration of the \c Hashable protocol, which
///  derived objects are able to be hashed by a \c std::hash<> funciton.
///
/// \see https://en.wikipedia.org/wiki/Protocol_(object-oriented_programming)
/// \see https://en.wikipedia.org/wiki/Hash_function
///
//===----------------------------------------------------------------------===//

#ifndef Hashable_hpp
#define Hashable_hpp

#include <functional>

namespace protocol {
    
    /// \brief Objects conforming to the hashable protocol are able to be hashed
    ///  by the \c std::hash<> function.
    ///
    /// \description The Hashable protocol requires to implemnet only a single
    ///  method. The method is \c hash, that returns a hash of the object.
    class Hashable {
    public:
        /// Default constructor.
                 Hashable() = default;
        /// Default destructor.
        virtual ~Hashable() = default;
        
        /// Creates a hash of the objects and returns it as a single value.
        /// \return The hash of the object.
        virtual std::size_t hash() const = 0;
        
    }; /* class Hashable */
    
} /* end namespace planner */

namespace std {
    
    /// std::hash temaplte specialization for all planner::Hashable
    /// structs/classes.
    template <>
    struct hash<protocol::Hashable> {
        size_t operator()(const protocol::Hashable &item) {
            return item.hash();
        }
    };
    
} /* end namespace std */

#endif /* Hashable_hpp */
