//===- FileEventLoader.cpp - FileEventLoader implementation ---------------===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventLoader.cpp
///
/// \brief This file contains implementation of a FileEventLoader class and
///  all associated exceptions.
//===----------------------------------------------------------------------===//

#include "FileEventLoader.hpp"
#include "Event/RecurringEvent.hpp"
#include <iostream>
#include <fstream>
#include <vector>

using namespace planner;

// MARK: Constructors

FileEventLoader::FileEventLoader(const std::string &srcDir) noexcept
: _srcDir(srcDir) {
    _formatter.setFormatString("%Y-%m-%d");
}

// MARK: Protected Implementation

void FileEventLoader::loadSingleEvents(DateInterval &interval) {
    loadSingleEventsForUID("", interval);
}

void FileEventLoader::loadSingleEventsForUID(const std::string &uid,
                                             DateInterval &interval) {
    interval.setStep(Date::TimeUnit::Month);
    for (auto date : interval) {
        std::ifstream is(fileNameForDate(date, uid));
        json events;
        try {
            is >> events;
            for (auto event : events) {
                _events.insert(std::make_shared<Event>(event));
            }
        } catch (const nlohmann::detail::parse_error &err) {}
    }
}

void FileEventLoader::loadRecurringEvents(DateInterval &interval) {}

void FileEventLoader::loadRecurringEventsForUID(const std::string &uid) {
    std::ifstream is(fileNameForRecur(uid));
    json events;
    _events.clear();
    try {
        is >> events;
        for (auto event : events) {
            _events.insert(std::make_shared<RecurringEvent>(event));
        }
    } catch (const nlohmann::detail::parse_error &err) {}
}

// MARK: Public Implementation

void FileEventLoader::setDirectory(const std::string &dir) noexcept {
    _srcDir = dir;
}

void FileEventLoader::loadAll(const std::string &uid) {
    auto interval = DateInterval(
         0, // Start of the time, the Jan 1st 1970
         DateFormatter::dateFromFormat(2500, 1, 1), // Max date
         Date::TimeUnit::Month
    );
    _events.clear();
    loadSingleEventsForUID(uid, interval);
}

void FileEventLoader::loadDateInterval(const DateInterval &interval) {
    loadDateIntervalForUID("", interval);
}

void FileEventLoader::loadDateIntervalForUID(const std::string &uid,
                                             const DateInterval &interval) {
    auto interval_ = DateInterval({
        interval.dateStart().startOf(Date::TimeUnit::Month),
        interval.dateEnd().add(1, Date::TimeUnit::Month)
        .startOf(Date::TimeUnit::Month)
    });
    _events.clear();
    loadSingleEventsForUID(uid, interval_);
}

void FileEventLoader::saveEvents(const std::set<Event_ptr> &events) {
    saveEventsForUID("", events);
}

void FileEventLoader::saveEventsForUID(const std::string &uid,
                                       const std::set<Event_ptr> &events) {
    std::ofstream os;
    std::vector<json> events_;
    auto interval = DateInterval(0,0);
    
    for (auto event : events) {
        if (event->isRecurring()) {
            continue;
        }
        if (event->interval().dateStart() > interval.dateEnd()) {
            interval = {
                event->interval().dateStart().startOf(Date::TimeUnit::Month),
                event->interval().dateStart().add(1, Date::TimeUnit::Month)
                .startOf(Date::TimeUnit::Month),
                Date::TimeUnit::Month
            };
            if (!events_.empty()) {
                os << events_;
            }
            os.open(fileNameForDate(interval.dateStart(), uid),
                    std::ios_base::out);
            events_.clear();
        }
        events_.push_back(event->toJSON());
    }
    os << events_;
}

void FileEventLoader::updateEventsForUID(const std::string &uid,
                                         const DateInterval &interval,
                                         const std::set<Event_ptr> &events) {
    loadDateIntervalForUID(uid, interval);
    _events.insert(events.begin(), events.end());
    saveEventsForUID(uid, _events);
}

void FileEventLoader::
saveRecurringEventsForUID(const std::string &uid,
                          const std::set<Event_ptr> &events)
{
    std::ofstream os(fileNameForRecur(uid));
    std::vector<json> events_;
    for (auto event : events) {
        events_.push_back(event->toJSON());
    }
    os << events_;
    
}

std::string FileEventLoader::fileNameForDate(const Date &date) {
    Date date_ = date.startOf(Date::TimeUnit::Month);
    return _srcDir + "event_data-" + _formatter.format(date_) + ".json";
}

std::string FileEventLoader::fileNameForDate(const Date &date,
                                             const std::string &uid) {
    Date date_ = date.startOf(Date::TimeUnit::Month);
    return _srcDir + uid + "-event_data-" + _formatter.format(date_) + ".json";
}

std::string FileEventLoader::fileNameForRecur(const std::string &uid) {
    return _srcDir + uid + "-event_data-recur.json";
}
