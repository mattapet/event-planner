//===- FileEventLoader.hpp - File Loader of Event objects -------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file FileEventLoader.hpp
///
/// \brief This file contains declaration of a FileEventLoader class, this is a
///  concrete class of the \c EventLoader using JSON files as a store for data.
///
/// \description For parsing JSON is used a small open sourced library called
///  'JSON for Modern C++'. The whole repository is stored in the \c lib/
///  directory, however only a single file contains all source code of the
///  module.
///
/// \see https://en.wikipedia.org/wiki/JSON
/// \see https://github.com/nlohmann/json
//===----------------------------------------------------------------------===//


#ifndef FileEventLoader_hpp
#define FileEventLoader_hpp

#include "EventLoader.hpp"
#include "core/DateFormatter.hpp"
#include "core/json.hpp"
#include <string>
#include <exception>

namespace planner {
    // For the json convenience
    using json = nlohmann::json;
    
    /// \brief A class, that implement the default behaviour of the
    ///  \c EventLoader base class.
    ///
    /// \description This class uses JSON files to store all necessary
    ///  information abount events.
    class FileEventLoader: public EventLoader {
    protected:
        /// The source root directory.
        std::string   _srcDir;
        /// The date formatter. Default to use format 'YYYY-MM-DD'
        DateFormatter _formatter;
        
        virtual void loadSingleEvents(DateInterval &interval);
        virtual void loadSingleEventsForUID(const std::string &uid,
                                            DateInterval &interval);
        virtual void loadRecurringEvents(DateInterval &interval);
        
    public:
        /// Deafult constructor
        /// \param srcDir The source directory of the loader.
        ///  The default values is the currect directory \c "./".
        FileEventLoader(const std::string &srcDir = "./") noexcept;
        
        /// Sets the source directory to given value.
        /// \param dir The source directory to use.
        void setDirectory(const std::string &dir) noexcept;
        
        /// Loads all events for the provided UID begining now.
        virtual void loadAll(const std::string &uid) override;
        
        /// Loads recurring events for the given UID.
        /// \param uid The UID of a user, whose recurring events
        ///  should be loaded.
        virtual void loadRecurringEventsForUID(const std::string &uid) override;
        /// Loads events for the given date interval.
        /// \param interval The date interval to load.
        virtual void loadDateInterval(const DateInterval &interval) override;
        /// Loads events for the given date interval.
        /// \param uid The UID of the user to load.
        /// \param interval The date interval to load.
        virtual void loadDateIntervalForUID(const std::string &uid,
                                            const DateInterval &interval) override;
        
        /// Saves provided events for the given UID.
        /// \param events The events to store.
        virtual void saveEvents(const std::set<Event_ptr> &events) override;
        /// Saves provided events for the given UID.
        /// \param uid The UID uder which to store the events.
        /// \param events The events to store.
        virtual void saveEventsForUID(const std::string &uid,
                                      const std::set<Event_ptr> &events) override;
        /// Saves provided events for the given UID.
        virtual void updateEventsForUID(const std::string &uid,
                                        const DateInterval &interval,
                                        const std::set<Event_ptr> &events) override;
        /// Saves provided recurring events for the given UID.
        /// \param uid The UID of the user.
        /// \param events The events which should be stored.
        virtual void saveRecurringEventsForUID(const std::string &uid,
                                               const std::set<Event_ptr> &events) override;
        
        /// Creates a filename of the source for the given date.
        std::string fileNameForDate(const Date &date);
        /// Creates a filename of the source for the given date and UID.
        std::string fileNameForDate(const Date &date, const std::string &uid);
        /// Creates a filename of the course for the given recurring UID.
        std::string fileNameForRecur(const std::string &uid);
        /// Creates a filename of the recurring source for the given date.
        std::string recurringFileNameForDate(const Date &date);
        
    }; /* class FileEventLoader */
    
    /* Convenience typedefs */
    typedef std::shared_ptr<FileEventLoader> FileEventLoader_ptr;
    typedef std::weak_ptr<FileEventLoader> FileEventLoader_wptr;
    typedef std::unique_ptr<FileEventLoader> FileEventLoader_uptr;
    
} /* end namespace planner */

#endif /* FileEventLoader_hpp */
