//===- EventLoader.hpp - Loader of Event objects ----------------*- C++ -*-===//
//
//                          The EventPlanner
// This file is part of the semestral project for the PA2 class at the CTU in
// Prague. The software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND.
//
//===----------------------------------------------------------------------===//
/// \file EventLoader.hpp
///
/// \brief This file contains declaration of a EventLoader class, from which
///  which serves as a default interface declaration for any event provider.
///
/// \description The \c EventLoader is used in the \c Calendar class,
/// for loading and storing events.
//===----------------------------------------------------------------------===//

#ifndef EventLoader_hpp
#define EventLoader_hpp

#include "core/Date.hpp"
#include "Event/Event.hpp"

#include <string>
#include <vector>
#include <set>
#include <memory>

namespace planner {
    
    /// A base abstract class, which derived class instances should be
    /// used by \c Calendar instances to load and save events.
    class EventLoader {
    protected:
        /// Set of loaded events.
        std::set<Event_ptr> _events;
        
    public:        
        /// Returns the currently loaded events.
        inline std::set<Event_ptr> &events() {
            return _events;
        }
        /// Returns the currently loaded events.
        inline std::set<Event_ptr>  events() const {
            return _events;
        }
        
        /// Loads all events for the provided UID begining now.
        virtual void loadAll(const std::string &uid) = 0;
        
        /// Loads recurring events.
        virtual void loadRecurringEventsForUID(const std::string &uid) = 0;
        /// Loads events for the given date interval.
        virtual void loadDateInterval(const DateInterval &interval) = 0;
        /// Loads events for the given date interval and UID.
        virtual void loadDateIntervalForUID(const std::string &uid,
                                            const DateInterval &interval) = 0;
        /// Saves provided events for the given UID.
        virtual void saveEvents(const std::set<Event_ptr> &events) = 0;
        /// Saves provided events for the given UID.
        virtual void saveEventsForUID(const std::string &uid,
                                      const std::set<Event_ptr> &events) = 0;
        /// Saves provided events for the given UID.
        virtual void updateEventsForUID(const std::string &uid,
                                        const DateInterval &interval,
                                        const std::set<Event_ptr> &events) = 0;
        /// Saves recurring events.
        virtual void saveRecurringEventsForUID(const std::string &uid,
                                               const std::set<Event_ptr> &events) = 0;
    }; /* class EventLoader */
    
    typedef std::shared_ptr<EventLoader> EventLoader_ptr;
    typedef std::weak_ptr<EventLoader> EventLoader_wptr;
    typedef std::unique_ptr<EventLoader> EventLoader_uptr;
    
} /* end namespace planner */

#endif /* EventLoader_hpp */
